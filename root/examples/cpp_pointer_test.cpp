#include <iostream>
using namespace std;

/**
 * @brief Demonstrates how a function accepts the address of a variable (i.e.
 *  the variable is passed to the function by REFERENCE).
 * 
 * @param g The integer we're going to modify.
 */
void af_1(int& g) {
    g++;
    std::cout << "g inside af_1: " << g << std::endl;
}

/**
 * @brief Demonstrates how a function accepts the value of a variable (i.e.
 *  the variable is passed to the function by VALUE).
 * 
 * @param g The integer we're going to modify.
 */
void af_2(int g) {
    g++;
    std::cout << "g inside af_2: " << g << std::endl;
}

int main() {
    int g = 123;
    std::cout << "Original g: " << g << std::endl;
    af_1(g);
    std::cout << "g after running af_1: " << g << std::endl;
    af_2(g);
    std::cout << "g after running af_2: " << g << std::endl;
    return 0;
}