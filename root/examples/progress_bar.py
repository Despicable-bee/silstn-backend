from alive_progress import alive_bar
import time

if __name__ == '__main__':
    with alive_bar(1000) as bar:
        for _ in range(2000):
            time.sleep(.0001)
            bar()   # type: ignore