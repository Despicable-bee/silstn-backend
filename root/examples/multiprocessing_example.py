from multiprocessing import Process, Queue
import os

def f(q: Queue):
    """ Some funct """
    messageFromParent = q.get()
    print("To: {} From: {}".format(os.getpid(), os.getppid()))
    print("Message from {}: {}".format(os.getppid(), messageFromParent))

if __name__ == '__main__':
    q = Queue()
    p = Process(target=f, args=(q,))
    p.start()
    q.put(["Burger", 23, None])
    p.join()
