import pprint
import json
from typing import List
import sys
import math

from alive_progress import alive_bar
from root.base_classes.base_doc import BaseDocType
from root.helpers.chunk_bounds_checker.chunk_bounds_checker import BoundingBoxHelper
from root.helpers.google_maps_traffic_helper.google_maps_traffic_helper import GoogleMapsTrafficHelperTransactionStatus

# Local libs
from root.helpers.google_maps_traffic_helper.google_maps_traffic_helper import \
        ChunkGetter
from root.helpers.google_maps_traffic_helper.google_maps_traffic_helper import \
        GoogleMapsTrafficHelper

from root.helpers.common_helper.common_firestore_helper import \
        FirestoreTimeHelper

from root.helpers.google_maps_traffic_helper.google_maps_traffic_helper import \
        IntersectionsTransactions

# Base Classes
from root.base_classes.chunk_doc import ChunkBoundingBox
from root.base_classes.chunk_doc import ChunkDoc
from root.base_classes.chunk_doc import Chunk
from root.base_classes.chunk_doc import ChunkType

from root.base_classes.intersections_doc import IntersectionDocManifestInstance, IntersectionsDoc, IntersectionsDocManifest
from root.base_classes.intersections_doc import Intersection
from root.base_classes.intersections_doc import Arm
from root.base_classes.intersections_doc import Route
from root.base_classes.intersections_doc import Step

from root.base_classes.common_classes import Coordinates

def calculate_distance(lat1: float, lng1: float, lat2: float, lng2: float):
    """ Calculates the distance between two geographic coordinates. 
    
    ARGS:
    - lat1: The latitude of the first coordinate
    - lng1: The longitude of the first coordinate
    - lat2: The latitude of the second coordinate
    - lng2: The longitude of the second coordinate

    RETURNS:
    - The distance between the two coordinates
    """
    R = 6371e3
    phi1 = lat1 * math.pi / 180
    phi2 = lat2 * math.pi / 180
    deltaPhi = (lat2 - lat1) * math.pi / 180
    deltaLambda = (lng2 - lng1) * math.pi / 180; 
    
    a = math.sin(deltaPhi / 2)**2 + math.cos(phi1) * math.cos(phi2) * \
            math.sin(deltaLambda/2)**2
    
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))

    d = R * c # in metres
    return d

def assign_intersections_to_chunks():
    """ Assigns various intersections to chunks that are in view. """
    # Open the routes file
    intersectionsFile = open('root/examples/data/routesMatchedToIntersections.json', 
            'r')
    intersectionsFileDict = json.loads(intersectionsFile.read())
    intersectionsFile.close()

    # Load each route into
    
    pp = pprint.PrettyPrinter()
    listOfIntersections: List[Intersection] = []
    counter = 1
    totalLength = len(intersectionsFileDict)
    for intersection in intersectionsFileDict:
        try:
            # Get all the arms
            listOfArms: List[Arm] = []
            for arm in intersection['arms']:
                # Get all the steps
                listOfSteps: List[Step] = []
                if arm == None:
                    continue
                for step in arm['route']['steps']:
                    # Get all the coordinates
                    listOfCoords: List[Coordinates] = []
                    for coord in step['coords']:
                        listOfCoords.append(Coordinates(
                                latitude=coord['lat'],
                                longitude=coord['lng']))
                    listOfSteps.append(Step(coords=listOfCoords,
                            distanceValue=step['distanceValue'],
                            durationValue=step['durationValue']))
                listOfArms.append(Arm(
                    nextTsc=arm['nextTsc'],
                    route=Route(steps=listOfSteps),
                    streetName=arm['streetName']))

            listOfIntersections.append(Intersection(
                    arms=listOfArms,
                    origin=Coordinates(
                            latitude=intersection['coordinates']['lat'],
                            longitude=intersection['coordinates']['lng']),
                    tsc=intersection['tsc']))
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            print("Line num: {}".format(exc_tb.tb_lineno)) # type: ignore
            print(e)
            pp.pprint(intersection)
            input("Intersection: {} / {}".format(counter, totalLength))
            counter += 1
            sys.exit()

    counter = 1
    totalLength = len(listOfIntersections)
    allIntersections = {}
    allStreetNames = {}
    for intersection in listOfIntersections:
        # Check if all the intersections are unique
        if intersection.get_tsc() in allIntersections:
            allIntersections[intersection.get_tsc()] += 1
        else:
            allIntersections[intersection.get_tsc()] = 1

    # Check if all the arms actually lead somewhere
    for intersection in listOfIntersections:
        for arm in intersection.get_arms():
            if arm.get_next_tsc() not in allIntersections:
                print("Intersection doesn't exist: {}".format(arm.get_next_tsc()))

    # Load all the near chunks
    cg = ChunkGetter()
    bbh = BoundingBoxHelper()

    selectedChunkType = ChunkType.CLOSE_CHUNK
    allChunksDocs = cg.get_all_chunks(selectedChunkType)

    if allChunksDocs == False:
        raise Exception("Error getting chunks docs")

    for chunkDoc in allChunksDocs:
        for chunk in chunkDoc.get_chunks():
            if len(chunk.get_intersections_ids_list()) > 0:
                print("Chunk doc Id: {}".format(chunkDoc.get_owner_id()))
                print("Chunk Id: {}".format(chunk.get_chunk_id()))

    barSize = len(listOfIntersections)
    with alive_bar(barSize) as bar:
        for intersection in listOfIntersections:
            # pp.pprint(intersection.to_dict())
            intersectionLats = []
            intersectionLngs = []
            for arm in intersection.get_arms():
                for step in arm.get_route().get_steps():
                    for coord in step.get_coords():
                        # Check if point exists in a particular close / near chunk
                        routePointBounds = ChunkBoundingBox(
                                topRightLat=coord.get_latitude(),
                                topRightLng=coord.get_longitude(),
                                bottomLeftLat=coord.get_latitude(),
                                bottomLeftLng=coord.get_longitude())

                        for chunkDoc in allChunksDocs:
                            for chunk in chunkDoc.get_chunks():
                                if bbh.check_bounding_box_in_view(
                                        viewportBounds=routePointBounds, 
                                        chunkBoundingBox=chunk.get_bounding_box()):
                                    listOfIntersectionIds = chunk\
                                            .get_intersections_ids_list()

                                    if str(intersection.get_tsc()) not in \
                                            listOfIntersectionIds:
                                        listOfIntersectionIds.append(
                                                str(intersection.get_tsc()))
                                        chunk.set_intersection_ids_list(
                                                listOfIntersectionIds)
            bar()
                    # intersectionLats.append(coord.get_latitude())
                    # intersectionLngs.append(coord.get_longitude())
        
    # Now let's check stuff
    barSize = len(allChunksDocs)
    with alive_bar(barSize) as bar:
        for chunkDoc in allChunksDocs:
            result = cg.write_chunk_to_firestore(chunkDoc=chunkDoc, 
                    chunkType=selectedChunkType)
            if result != GoogleMapsTrafficHelperTransactionStatus.TRANSACTION_SUCCESS:
                print("Type: {}".format(type(result)))
                print("Result: {}".format(result))
                raise Exception("Error trying to write files to firestore: {}".format(chunkDoc.get_owner_id()))
            bar()
    
        # Get the max/min lats/lngs and create a bounding box with them. 
        # boundingBox = ChunkBoundingBox(topRightLat=max(intersectionLats),
        #         topRightLng=max(intersectionLngs),
        #         bottomLeftLat=min(intersectionLats),
        #         bottomLeftLng=min(intersectionLngs))
        
        # print(intersection.get_tsc())
        # pp.pprint(boundingBox.to_dict())
        

        # # Compute the length and width of the bounding box
        # v1 = boundingBox.get_top_right_vert()
        # v2 = boundingBox.get_bottom_left_vert()
        # width = calculate_distance(lat1=v1.get_lat(), lng1=v1.get_lng(), 
        #         lat2=v1.get_lat(), lng2=v2.get_lng())
        # height = calculate_distance(lat1=v1.get_lat(), lng1=v1.get_lng(), 
        #         lat2=v2.get_lat(), lng2=v1.get_lng())
        # print("Area: {}km^2".format(round(width*height / 1000, 3)))
        # print("Width/Height Ratio: {}".format(round(width/height,3)))
        
        # # Remember
        # # Total Earth Surface Area is 510.1 x 10^6 km^2
        # # Thus, each FAR chunk is 99628.91 km^2
        # # STANDARD chunk is 6626.81 km^2
        # # NEAR chunk is 389.175 km^2
        # # CLOSE chunk is 24.32 km^2
        
        # input()

def copy_over_intersection_data():
    """ Copies the intersection data to firestore """
    # Open the routes file
    intersectionsFile = open('root/examples/data/routesMatchedToIntersections.json', 
            'r')
    intersectionsFileDict = json.loads(intersectionsFile.read())
    intersectionsFile.close()

    # Load each route into
    
    pp = pprint.PrettyPrinter()
    listOfIntersections: List[Intersection] = []
    counter = 1
    totalLength = len(intersectionsFileDict)
    for intersection in intersectionsFileDict:
        try:
            # Get all the arms
            listOfArms: List[Arm] = []
            for arm in intersection['arms']:
                # Get all the steps
                listOfSteps: List[Step] = []
                if arm == None:
                    continue
                for step in arm['route']['steps']:
                    # Get all the coordinates
                    listOfCoords: List[Coordinates] = []
                    for coord in step['coords']:
                        listOfCoords.append(Coordinates(
                                latitude=coord['lat'],
                                longitude=coord['lng']))
                    listOfSteps.append(Step(coords=listOfCoords,
                            distanceValue=step['distanceValue'],
                            durationValue=step['durationValue']))
                listOfArms.append(Arm(
                    nextTsc=arm['nextTsc'],
                    route=Route(steps=listOfSteps),
                    streetName=arm['streetName']))

            listOfIntersections.append(Intersection(
                    arms=listOfArms,
                    origin=Coordinates(
                            latitude=intersection['coordinates']['lat'],
                            longitude=intersection['coordinates']['lng']),
                    tsc=intersection['tsc']))
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            print("Line num: {}".format(exc_tb.tb_lineno)) # type: ignore
            print(e)
            pp.pprint(intersection)
            input("Intersection: {} / {}".format(counter, totalLength))
            counter += 1
            sys.exit()
    
    # We have all the intersections loaded, lets save them to a document
    fth = FirestoreTimeHelper()
    currentTime = fth.get_unix_epoch_timestamp_ms()
    
    listOfIntersectionDocs: List[IntersectionsDoc] = []
    
    currentIntersectionDoc = IntersectionsDoc(intersections=[],
            createTime=currentTime,
            docType=BaseDocType.INTERSECTIONS_DOC,
            lastEditted=currentTime,
            major=0,
            minor=3,
            patch=0,
            ownerId='int0')

    counter = 0

    intersectionsDocManifest = IntersectionsDocManifest(createTime=currentTime,
            docType=BaseDocType.INTERSECTIONS_DOC_MANIFEST,
            instancesList=[],
            lastEditted=currentTime,
            major=0,
            minor=3,
            patch=0,
            ownerId='intersections_doc_manifest')

    for intersection in listOfIntersections:
        if not currentIntersectionDoc.add_intersection(
                intersection=intersection):
            # Current intersection doc is full, append it to the list and 
            # create a new one
            counter += 1

            # Create a manifest instance that points to the new intersections 
            # doc.
            tscsList: List[int] = []
            for intersection in currentIntersectionDoc.get_intersections():
                tscsList.append(intersection.get_tsc())

            newManifInstance = IntersectionDocManifestInstance(
                    currentCapacity=currentIntersectionDoc\
                            .get_current_capacity(),
                    docId=currentIntersectionDoc.get_owner_id(),
                    listOfTscs=tscsList)


            manifInstancesList = intersectionsDocManifest.get_instances_list()
            manifInstancesList.append(newManifInstance)

            # Append the intersection doc to a list for later writing
            listOfIntersectionDocs.append(currentIntersectionDoc)
            currentIntersectionDoc = IntersectionsDoc(intersections=[],
                    createTime=currentTime,
                    docType=BaseDocType.INTERSECTIONS_DOC,
                    lastEditted=currentTime,
                    major=0,
                    minor=3,
                    patch=0,
                    ownerId='int{}'.format(counter))

    it = IntersectionsTransactions()
    
    # Write the intersection manifest doc to firestore
    it.write_intersection_doc_manifest(manifToWrite=intersectionsDocManifest)
    
    # Write the intersection to firestore
    for intersectionDoc in listOfIntersectionDocs:
        result = it.write_intersections_doc(
                intersectionsDocToWrite=intersectionDoc)

        if result != GoogleMapsTrafficHelperTransactionStatus\
                .TRANSACTION_SUCCESS:
            raise Exception("Error writing intersections doc to firestore")

def test_get_intersection_doc():
    it = IntersectionsTransactions()
    for doc in it.get_intersection_docs():
        print("Document Id: {}".format(doc.get_owner_id()))
        print("Number of intersections: {}".format(len(doc.get_intersections())))

if __name__ == '__main__':
    copy_over_intersection_data()
    
    

        