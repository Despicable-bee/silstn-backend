import pprint
from libcst import Modulo
from matplotlib import pyplot

def brute_force_multiples_finder(number: int):
    """ Oh boy, reminds me of prime numbers :P """
    listOfMultiples = []
    for i in range(1, number):
        if (number / i).is_integer() and (i % 4 == 0):
            listOfMultiples.append(i)
    return listOfMultiples

def determine_num_multiples_of(targetNumber: int, multiple: int):
    counter = 0
    currentNumber = targetNumber
    while True:
        currentNumber = currentNumber / multiple
        if currentNumber.is_integer():
            counter += 1
        else:
            break
    return counter

if __name__ == '__main__':
    targetNumber = 40075 * 4
    result = brute_force_multiples_finder(targetNumber)
    for r in result:
        outcome = determine_num_multiples_of(targetNumber, 4)
        print("{} : {}".format(r, outcome))