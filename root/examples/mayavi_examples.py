"""
    DESCRIPTION: Some basic examples of how the `mayavi` library works for
            3D visualisation of data. 
            Main purpose of this at the time of writing is to test the
            icosahedron chunk system.

    NOTES:  mayavi's mlab.animate() decorator doesn't seem to work for
            mesh objects. Hence, I recommend using something like
            `moviepy` to create a video, and mayavi to render each frame.
"""

# Standard libs

# Third party libs
from cmath import sqrt
from numpy import pi
from numpy import sin
from numpy import cos
from numpy import mgrid
import numpy as np

from mayavi import mlab

import moviepy.editor as mpy

# Local libs

# Base classes

def harmonic_surface_demo():
    dphi = pi/250.0
    dtheta = pi/250.0

    [phi, theta] = mgrid[0 : pi + dphi*1.5 : dphi, 
                         0 : 2*pi + dtheta*1.5 : dtheta]

    m0 = 4
    m1 = 3
    m2 = 2
    m3 = 3
    m4 = 6
    m5 = 2
    m6 = 6
    m7 = 4
    r = sin(m0*phi)**m1 + \
        cos(m2*phi)**m3 + \
        sin(m4*theta)**m5 + \
        cos(m6*theta)**m7
    
    x = r*sin(phi)*cos(theta)
    y = r*cos(phi)
    z = r*sin(phi)*sin(theta)

    s = mlab.mesh(x, y, z)
    mlab.show()

def icosahedron_demo():
    """ Draws an icosahedron using the triangle mesh function. """
    t = ((1 + sqrt(5)) / 2).real

    multiplier = (1 / (sqrt(1+t**2))).real

    print(t)

    print(multiplier)

    # Compute the various permutations of the icosahedron
    shift = -0.2*pi
    print(np.array((t, 1, 0)))
    v0 = multiplier * np.array((t, 1, 0))
    print(v0)
    v1 = multiplier * np.array((-t, 1, 0))
    v2 = multiplier * np.array((t, -1, 0))
    v3 = multiplier * np.array((-t, -1, 0))
    v4 = multiplier * np.array((1, 0, t))
    v5 = multiplier * np.array((1, 0, -t))
    v6 = multiplier * np.array((-1, 0, t))
    v7 = multiplier * np.array((-1, 0, -t))
    v8 = multiplier * np.array((0, t, 1))
    v9 = multiplier * np.array((0, -t, 1))
    v10 = multiplier * np.array((0, t, -1))
    v11 = multiplier * np.array((0, -t, -1))

    temp = [v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11]


    # Compute the triangles given the vertices (triangles are simply the 
    # indices of the vertices in the temp list)
    T0 = (0, 8, 4)
    T1 = (0, 5, 10)
    T2 = (2, 4, 9)
    T3 = (2, 11, 5)
    T4 = (1, 6, 8)
    T5 = (1, 10, 7)
    T6 = (3, 9, 6)
    T7 = (3, 7, 11)
    T8 = (0, 10, 8)
    T9 = (1, 8, 10)
    T10 = (2, 9, 11)
    T11 = (3, 9, 11)
    T12 = (4, 2, 0)
    T13 = (5, 0, 2)
    T14 = (6, 1, 3)
    T15 = (7, 3, 1)
    T16 = (8, 6, 4)
    T17 = (9, 4, 6)
    T18 = (10, 5, 7)
    T19 = (11, 7, 5)

    triangles = [T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, 
            T14, T15, T16, T17, T18, T19]
    
    x = []
    y = []
    z = []

    for vert in temp:
        x.append(vert[0])
        y.append(vert[1])
        z.append(vert[2])

    x = np.array([x]).T
    y = np.array([y]).T
    z = np.array([z]).T

    zeros = np.zeros((12,1))
    ones = np.ones((12,1))

    print(x.shape)
    print(y.shape)
    print(z.shape)

    # x = np.append(zeros, x, axis=1)
    # y = np.append(ones, y, axis=1)
    # z = np.append(-1*ones, z, axis=1)

    print(x)
    print(y)
    print(z)
    print(triangles)

    mlab.triangular_mesh(x , y , z, triangles, color=(0,0,0), representation='wireframe')
    mlab.show()

def make_triangular_mesh_cube():
    """ Makes a triangular mesh cube """
    v1 = (1,-1, 0)
    v2 = (1, 1, 0)
    v3 = (-1,1, 0)
    v4 = (-1,-1,0)

    temp = [v1, v2, v3, v4]

    T0 = (0,3,1)
    T1 = (2,1,3)

    x = []
    y = []
    z = []

    for t in temp:
        x.append(t[0])
        y.append(t[1])
        z.append(t[2])

    x = np.array([x]).T
    y = np.array([y]).T
    z = np.array([z]).T

    triangles = [T0, T1]

    zeros = np.zeros((4,1))
    ones = np.ones((4,1))

    # x = np.append(ones, x, axis=1)
    # y = np.append(zeros, y, axis=1)
    # z = np.append(zeros, z, axis=1)

    mlab.triangular_mesh(x , y , z, triangles, color=(0,0,0), 
            representation='wireframe')
    mlab.show()

def ball_and_icosahedron():
    """ Here we combine the icosahedron with the sphere mesh. """
    t = ((1 + sqrt(5)) / 2).real

    multiplier = 1 / t

    # Compute the various permutations of the icosahedron
    v0 = multiplier * np.array((t, 1, 0))
    v1 = multiplier * np.array((-t, 1, 0))
    v2 = multiplier * np.array((t, -1, 0))
    v3 = multiplier * np.array((-t, -1, 0))
    v4 = multiplier * np.array((1, 0, t))
    v5 = multiplier * np.array((1, 0, -t))
    v6 = multiplier * np.array((-1, 0, t))
    v7 = multiplier * np.array((-1, 0, -t))
    v8 = multiplier * np.array((0, t, 1))
    v9 = multiplier * np.array((0, -t, 1))
    v10 = multiplier * np.array((0, t, -1))
    v11 = multiplier * np.array((0, -t, -1))

    temp = [v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11]


    # Compute the triangles given the vertices (triangles are simply the 
    # indices of the vertices in the temp list)
    T0 = (0, 8, 4)
    T1 = (0, 5, 10)
    T2 = (2, 4, 9)
    T3 = (2, 11, 5)
    T4 = (1, 6, 8)
    T5 = (1, 10, 7)
    T6 = (3, 9, 6)
    T7 = (3, 7, 11)
    T8 = (0, 10, 8)
    T9 = (1, 8, 10)
    T10 = (2, 9, 11)
    T11 = (3, 9, 11)
    T12 = (4, 2, 0)
    T13 = (5, 0, 2)
    T14 = (6, 1, 3)
    T15 = (7, 3, 1)
    T16 = (8, 6, 4)
    T17 = (9, 4, 6)
    T18 = (10, 5, 7)
    T19 = (11, 7, 5)

    triangles = [T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, 
            T14, T15, T16, T17, T18, T19]
    
    x = []
    y = []
    z = []

    for vert in temp:
        x.append(vert[0])
        y.append(vert[1])
        z.append(vert[2])

    x = np.array([x]).T
    y = np.array([y]).T
    z = np.array([z]).T

    # And now for the sphere
    # Step size for the points
    dphi = pi/50.0
    dtheta = pi/25.0

    # Radius of the sphere
    r = 1

    [phi, theta] = mgrid[-pi: pi + dphi*1.0: dphi, 0: pi + dtheta*1.0: dtheta]

    xSphere = r*sin(phi)*cos(theta)
    ySphere = r*sin(phi)*sin(theta)
    zSphere = r*cos(phi)

    mlab.mesh(xSphere, ySphere, zSphere, representation='surface', opacity=1.0)
    mlab.triangular_mesh(x , y , z, triangles, color=(1,0,0), representation='wireframe')
    mlab.show()
    pass

def test_triangular_mesh():
    """An example of a cone, ie a non-regular mesh defined by its
        triangles.
    """
    n = 8
    t = np.linspace(-np.pi, np.pi, n)
    z = np.exp(1j * t)
    x = z.real.copy()
    y = z.imag.copy()
    z = np.zeros_like(x)

    print(x)
    print(y)
    print(z)

    triangles = [(0, i, i + 1) for i in range(1, n)]

    print(triangles)

    x = np.r_[0, x]
    y = np.r_[0, y]
    z = np.r_[1, z]
    t = np.r_[0, t]

    mlab.triangular_mesh(x, y, z, triangles, scalars=t)

def sphere_mesh_demo():
    """ Draws a sphere that spins. 
    
    """
    # Step size for the points
    dphi = pi/50.0
    dtheta = pi/25.0

    # Radius of the sphere
    r = 10.0

    [phi, theta] = mgrid[-pi: pi + dphi*1.0: dphi, 0: pi + dtheta*1.0: dtheta]

    x = r*sin(phi)*cos(theta)
    y = r*sin(phi)*sin(theta)
    z = r*cos(phi)

    # Figure to save to
    fig_myv = mlab.figure(size=(600,600), bgcolor=(1,1,1))
    
    # Movie will last for 10 seconds
    duration = 2
    
    def make_frame(t):
        mlab.clf()
        multiplier = 5;
        x = r*sin(phi)*cos(theta + dtheta*t*multiplier)
        y = r*sin(phi)*sin(theta + dtheta*t*multiplier)
        z = r*cos(phi)
        mlab.mesh(x,y,z, figure=fig_myv, color=(0,0,0), representation='fancymesh')

        f = mlab.gcf()
        f.scene._lift() # type: ignore
        return mlab.screenshot()

    animation = mpy.VideoClip(make_frame, duration=duration)
    
    # Save the animation as `test.gif`, with a framerate of 24 fps
    animation.write_gif("test.gif", 24)

    # s = mlab.mesh(x, y, z, color=(0,0,0), representation='fancymesh')
    # mlab.show()

if __name__ == '__main__':
    ball_and_icosahedron()