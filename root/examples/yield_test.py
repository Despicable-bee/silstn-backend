def func1():
    x = 0
    while x < 5:
        yield x
        x += 1

def func2():
    for i in func1():
        yield i


if __name__ == '__main__':
    for i in func2():
        print(i)
    pass