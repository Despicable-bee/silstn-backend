# Standard libs
from typing import List
import random
import sys

# Third party libs
import numpy as np
from numpy import pi
from numpy import sin

import scipy.signal as siglib

import matplotlib.pyplot as plt



# Local libs

def test_fabricate_data():
    # Number of sample points
    N = 600     

    # Sample space (i.e. Fs = 800)
    T = 1.0 / 800.0

    x = np.linspace(0, N*T, N)
    y = sin(100 * 2.0 * pi * x)

    yf = np.fft.fft(y)
    xf = np.linspace(0.0, 1.0/(2.0*T), N // 2)

    plt.plot(xf, 2.0/N * np.abs(yf[:N//2]))
    plt.show()

def test_fabricate_noisy_data():
    # Number of sample points
    N = 600     

    # Sample space (i.e. Fs = 800)
    T = 1.0 / 800.0

    x = np.linspace(0, N*T, N)

    # Create some noise
    mu = 0
    sigma = 0.1
    noise = np.random.normal(mu, sigma, N)

    y = sin(50 * 2.0 * pi * x) + noise

    plt.plot(x, y)
    plt.show()

    yf = np.fft.fft(y)
    xf = np.linspace(0.0, 1.0/(2.0*T), N // 2)

    plt.plot(xf, 2.0/N * np.abs(yf[:N//2]))
    plt.show()

def exponential_moving_average(inputList: List[float], smoothing: float, 
        days: int):
    """ Computes the exponential moving average of a given list

    ARGS:
    - inputList: The input list we want to get the EMA of
    - smoothing: How `smooth` we want the EMA to be (subjective)
    - days: The number of days to use when computing the average (i.e. 5-day, 
        10-day, etc)

    RETURNS:
    - The EMA of the list
    """
    outputList: List[float] = []
    emaYesterday = inputList[0]
    for value in inputList:
        emaToday = (value * (smoothing / (1 + days))) + \
                (emaYesterday * (1 - (smoothing/(1 + days))))
        outputList.append(emaToday)
        emaYesterday = emaToday
    
    return outputList

def test_isolate_core_frequencies():
    """ Tests whether we can isolate specific frequencies in a signal """
     # Number of sample points
    N = 1000     

    # Sample space (i.e. Fs = 300)
    T = 1.0 / 5000.0

    x = np.linspace(0, N*T, N)
    y = sin(1 * 2.0 * pi * x)
    for i in range(0,50):
        freq = random.randint(1, 30)
        y += sin(freq * 2.0 * pi * x)

    ymin = min(y)
    y = y + abs(ymin)

    plt.plot(x, y)
    plt.show()

    yf = np.fft.fft(y)
    xf = np.linspace(0.0, 1.0/(2.0*T), N // 2)

    plt.plot(xf, 2.0/N * np.abs(yf[:N//2]))
    plt.show()

    # Randomly shift samples back and forth certain indexes
    counter = 0
    length = len(yf)
    for sample in yf:
        offset = round(np.random.normal(0, 1, 1)[0])
        noise = np.random.normal(0, 0.5, 1)[0]
        noisePower = 1 + np.abs(np.random.normal(0, 1, 1)[0])
        if (counter + offset) < 0 or counter + offset >= length:
            continue
        yf[counter + offset] = noisePower * (sample + noise)
        yf[counter] = 0
        counter += 1

    plt.plot(xf, 2.0/N * np.abs(yf[:N//2]))
    plt.show()

    yb = np.fft.ifft(yf)
    ybMin = min(yb)
    yb += abs(ybMin)
    
    plt.plot(x, y, label="original")
    plt.plot(x, yb, label="altered")
    plt.show()

def sinc_interpolation(y: list, M: int, r: float):
    # Upsample our original data
    counter = 0
    indexer = 0
    upsampled = []
    for i in range(0,len(y) * M):
        if counter == M:
            upsampled.append(y[indexer])
            indexer += 1
            counter = 0
        else:
            upsampled.append(0)
            counter += 1
    
    # Do sinc interpolation
    outputThing = np.zeros(len(y)*M)
    counter = 0
    try:
        for value in y:
            # Create a sinc wave that is M samples long
            n = np.linspace(-M*r, M*r, (M*2)+1)
            sincWave = value*np.sinc(n)
            # plt.plot(sincWave)
            # plt.show()
            for i in range(0, (M*2) + 1):
                outputThing[counter + i] += sincWave[i]
            counter += (M - 1)
    except Exception as e:
        print(counter)
        print(e)
        sys.exit()

    return outputThing

def test_sinc_wave_interpolation():
    # Number of samples
    N = 288
    # Sample spacing (T = 1/Fs)
    T = 1/0.0033

    # Samples
    x = np.linspace(0, N * T, N)

    # 5 Hz
    y = sin(4 * 2.0 * pi * x)
    print(len(y))
    plt.plot(x,y)
    plt.show()
    
    # Interpolate
    M = 20
    xM = np.linspace(0, N * T, N*M)
    plt.plot(xM, sinc_interpolation(y, M, 0.05), label="interpolated")
    plt.plot(x,y, label="original")
    plt.legend()
    plt.show()
    

if __name__ == '__main__':
    test_sinc_wave_interpolation()