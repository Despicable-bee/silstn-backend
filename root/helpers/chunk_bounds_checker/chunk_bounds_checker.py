# Standard Libs
from typing import List
from typing import Tuple
from typing import Dict
from typing import Optional

from enum import Enum
from enum import auto

import sys
import json

import logging

# Third Party Libs
from google.cloud import firestore

# Local Libs

from root.helpers.common_helper.common_firestore_helper import \
        FirestoreErrorHelper

from root.helpers.chunk_bounds_checker.intersection_doc_getters import \
        IntersectionsManifestObjectFactory
from root.helpers.chunk_bounds_checker.intersection_doc_getters import \
        IntersectionDocGetter

# Base Classes
from root.base_classes.chunk_doc import ChunkBoundingBox
from root.base_classes.chunk_doc import ChunkType
from root.base_classes.chunk_doc import ChunkManifestDoc
from root.base_classes.chunk_doc import ChunkManifestIndex
from root.base_classes.chunk_doc import ChunkDoc
from root.base_classes.chunk_doc import ChunkManifestBoundsType
from root.base_classes.chunk_doc import Chunk
from root.base_classes.chunk_doc import ChunkMode

from root.base_classes.base_doc import BaseDocType
from root.base_classes.intersections_doc import Intersection
from root.base_classes.intersections_doc import IntersectionDocManifestInstance
from root.base_classes.intersections_doc import IntersectionsDoc
from root.base_classes.intersections_doc import IntersectionsDocManifest

from root.base_classes.common_classes import Coordinates

from root.base_classes.intersections_doc import Arm
from root.base_classes.intersections_doc import Route
from root.base_classes.intersections_doc import Step

from root.api.chunk_getter.generated import chunk_getter_pb2 as pb2

# * DEBUG ----------------------------------------------------------------------

__filename__ = 'chunk_bounds_checker.py'

# * ENUM -----------------------------------------------------------------------

class ViewportBoundsConfig(Enum):
    RHS = auto()
    LHS = auto()
    BOTH = auto()
    SPLIT = auto()

# * AUXILIARY HELPERS ----------------------------------------------------------

class BoundingBoxHelper(object):
    def __init__(self):
        pass

    def check_viewport_bounds_config(self, viewportBounds: ChunkBoundingBox):
        """ Determines the configuration of the viewport bounds. 
        
        The viewport can be one of 4 configurations:
        1. `LHS`: The entirety of the viewport bounds is in the LHS quadrant.
        2. `RHS`: The entirety of the viewport bounds is in the RHS quadrant.
        3. `Both`: The viewport bounds cross from -ve to +ve, but does not 
            cross the split line.
        4. `Split`: The viewport bounds cross the split line.
        """
        v1 = viewportBounds.get_top_right_vert()
        v2 = viewportBounds.get_bottom_left_vert()

        if v1.get_lng() < v2.get_lng():
            # Viewport is split
            return ViewportBoundsConfig.SPLIT
        else:
            # Viewport is not split
            if v1.get_lng() < 0 and v2.get_lng() < 0:
                return ViewportBoundsConfig.RHS
            elif v1.get_lng() >= 0 and v2.get_lng() >= 0:
                return ViewportBoundsConfig.LHS
            else:
                return ViewportBoundsConfig.BOTH

    def check_bounding_box_in_view(self, viewportBounds: ChunkBoundingBox,
            chunkBoundingBox: ChunkBoundingBox):
        """ Determines if the chunk is within the bounds of the viewport. 
        
        NOTES:
        - Optimise this please :)

        ARGS:
        - viewportBounds: Bounding box representing the viewport from the user.

        RETURNS:
        - If either of the viewport verticies are within the bounding box, then
            the method will return `True`. Otherwise will return `False`
        """
        v1 = chunkBoundingBox.get_top_right_vert()
        v2 = chunkBoundingBox.get_bottom_left_vert()
        p1 = viewportBounds.get_top_right_vert()
        p2 = viewportBounds.get_bottom_left_vert()

        v1lat = v1.get_lat()
        v1lng = v1.get_lng()
        v2lat = v2.get_lat()
        v2lng = v2.get_lng()

        p1lat = p1.get_lat()
        p1lng = p1.get_lng()
        p2lat = p2.get_lat()
        p2lng = p2.get_lng()

        # Check the outside chunk cases
        if (v1lng < p2lng) or (p1lat < v2lat) or (p1lng < v2lng) or (v1lat < p2lat):
            # logging.info("Chunk outside")
            return False

        # If it's not outside, then it must be inside :D
        return True

class IntersectionDocObjectFactory(object):
    def __init__(self):
        self.__classname__ = 'IntersectionDocObjectFactory'

    def to_obj_v_0_3_0(self, inputDict: dict):
        try:
            docVersion = inputDict['doc_version'].split(".")
            
            # Get all the intersections
            intersections = self.__intersections_to_obj(inputDict=inputDict)

            # Put together the intersection doc
            return IntersectionsDoc(createTime=inputDict['create_time'],
                    currentCapacity=inputDict['current_capacity'],
                    docType=BaseDocType(inputDict['doc_type']),
                    intersections=intersections,
                    lastEditted=inputDict['last_editted'],
                    major=int(docVersion[0]),
                    minor=int(docVersion[1]),
                    patch=int(docVersion[2]),
                    ownerId=inputDict['owner_id'])
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.class_error_handler(fileName=__filename__,
                    className=self.__classname__, 
                    methodName='to_obj_v_0_3_0', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            return None
    
    def __intersections_to_obj(self, inputDict: dict):
        manifestList: List[Intersection] = []
        for intersec in inputDict['intersections']:
            armsList: List[Arm] = []
            for arm in intersec['arms']:
                # Get all the arms
                stepsList: List[Step] = []
                for step in arm['route']['steps']:
                    # Get all the steps
                    coordsList: List[Coordinates] = []
                    for coords in step['coords']:
                        coordsList.append(Coordinates(latitude=coords['lat'], 
                                longitude=coords['lng']))
                    stepsList.append(Step(distanceValue=step['distance_value'],
                            durationValue=step['duration_value'],
                            coords=coordsList))
                armsList.append(Arm(nextTsc=arm['next_tsc'],
                        route=Route(steps=stepsList),
                        streetName=arm['street_name']))
            manifestList.append(Intersection(
                    origin=Coordinates(latitude=intersec['origin']['lat'],
                            longitude=intersec['origin']['lng']),
                    arms=armsList,
                    tsc=intersec['tsc']))
        return manifestList
    

class ChunkManifestObjectFactory(object):
    def __init__(self):
        self.__classname__ = "ChunkManifestObjectFactory"

    def to_obj_v_0_3_0(self, inputDict: dict):
        """ Converts the firestore dict to its corresponding object. 
        
        VERSION:
        - This method supports doc_version 0.3.0

        ARGS:
        - inputDict: The dictionary received from firestore

        RETURNS:
        - On success, the method returns a `ChunkManifestDoc`. Otherwise returns
            `None`. 
        """
        try:
            docVersion = inputDict['doc_version'].split('.')
            
            # Get all of the manifests
            manifestsList = self.__manifest_instances_to_obj(
                    inputDict=inputDict)

            # Put together the chunk document
            return ChunkManifestDoc(chunkManifests=manifestsList,
                    createTime=inputDict['create_time'],
                    docType=BaseDocType(inputDict['doc_type']),
                    lastEditted=inputDict['last_editted'],
                    major=int(docVersion[0]),
                    minor=int(docVersion[1]),
                    patch=int(docVersion[2]),
                    ownerId=inputDict['owner_id'])
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.class_error_handler(fileName=__filename__,
                    className=self.__classname__, 
                    methodName='to_obj_v_0_3_0', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            return None

    
    def __manifest_instances_to_obj(self, inputDict: dict):
        manifestsList: List[ChunkManifestIndex] = []
        for instance in inputDict['manifests']:
            manifestsList.append(ChunkManifestIndex(
                chunkGroupBoundingBox=ChunkBoundingBox(
                    topRightLat=instance['group_bounds']['top_right']['lat'],
                    topRightLng=instance['group_bounds']['top_right']['lng'],
                    bottomLeftLat=instance['group_bounds']['bottom_left']['lat'],
                    bottomLeftLng=instance['group_bounds']['bottom_left']['lng']),
                currentCapacity=instance['current_capacity'],
                docId=instance['doc_id'],
                numChunks=instance['num_chunks']))
        return manifestsList

class ChunkDocObjectFactory(object):
    def __init__(self):
        self.__classname__ = "ChunkDocObjectFactory"

    def to_obj_v_0_3_0(self, inputDict: dict):
        try:
            docVersion = inputDict['doc_version'].split('.')

            chunksList = self.__chunk_instances_to_obj(inputDict)

            return ChunkDoc(chunkType=ChunkType(inputDict['chunk_type']),
                    bounds=ChunkBoundingBox(
                            topRightLat=inputDict['bounds']['top_right']['lat'],
                            topRightLng=inputDict['bounds']['top_right']['lng'],
                            bottomLeftLat=inputDict['bounds']['bottom_left']['lat'],
                            bottomLeftLng=inputDict['bounds']['bottom_left']['lng']),
                    chunks=chunksList,
                    boundsType=ChunkManifestBoundsType(inputDict['bounds_type']),
                    createTime=inputDict['create_time'],
                    currentCapacity=inputDict['current_capacity'],
                    docType=BaseDocType(inputDict['doc_type']),
                    lastEditted=inputDict['last_editted'],
                    major=int(docVersion[0]),
                    minor=int(docVersion[1]),
                    patch=int(docVersion[2]),
                    ownerId=inputDict['owner_id'])

        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.class_error_handler(fileName=__filename__,
                    className=self.__classname__, 
                    methodName='to_obj_v_0_3_0', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            return None


    def __chunk_instances_to_obj(self, inputDict: dict):
        chunksList: List[Chunk] = []
        for chunk in inputDict['chunks']:
            chunksList.append(Chunk(
                lat1=chunk['vert_1']['lat'],
                lat2=chunk['vert_2']['lat'],
                lat3=chunk['vert_3']['lat'],
                lng1=chunk['vert_1']['lng'],
                lng2=chunk['vert_2']['lng'],
                lng3=chunk['vert_3']['lng'],
                intersectionsIdsList=chunk['intersection_ids'],
                chunkId=chunk['chunk_id']))
        return chunksList

# * MAIN HELPER ----------------------------------------------------------------

class ChunkBoundsChecker:
    def __init__(self):
        self.db = firestore.Client()

        self.transaction = self.db.transaction()

        self.chunksRef = self.db.collection(u'chunks') # type: ignore

        self.intersectionsRef = self.db.collection(u'intersections') # type: ignore

        self.__classname__ = 'ChunkBoundsChecker'
    
    # ? PUBLIC METHODS---------------------------------------------------------
     
    def get_chunks_in_view_stream(self, viewMode: ChunkType, 
            viewportBounds: ChunkBoundingBox,
            viewportBoundsConfig: ViewportBoundsConfig):
        """ Grabs the set of chunks that are in view of the camera. 
        
        Behaviour will change depending on the current mode of the camera.
        (i.e. if the user requests chunks within the viewport at FAR mode, then
                chunks will be sampled from the `far_chunks_manifest`)
        """
        try:
            manifestDoc = self.__get_chunk_manifest(viewMode)
            specialManifestDoc = self.__get_special_chunk_manifest(viewMode)
            intersectionsManifestDoc = self.__get_intersections_manifest_doc()

            # Container for storing the loaded intersection docs. Docs will be
            #   loaded on demand.
            loadedIntersectionDocs: Dict[str, Optional[IntersectionsDoc]] = {}
            
            # Container for specifying whether the intersection has been yielded
            # already or not
            yieldedIntersectionIds: Dict[str, bool] = {}

            if manifestDoc == None or specialManifestDoc == None or \
                    intersectionsManifestDoc == None:
                raise Exception("Error trying to get manifestDoc " + \
                        "or specialManifestDoc")

            # Initialise the container that will hold the loaded intersection 
            #   docs
            for instance in intersectionsManifestDoc.get_instances_list():
                loadedIntersectionDocs[instance.get_doc_id()] = None

            if viewportBoundsConfig == ViewportBoundsConfig.LHS or \
                    viewportBoundsConfig == ViewportBoundsConfig.RHS:
                for i in self.__check_chunks_in_view(
                        viewportBounds=viewportBounds,
                        manifestDoc=manifestDoc,
                        specialManifestDoc=specialManifestDoc,
                        viewMode=viewMode,
                        viewportConfig=viewportBoundsConfig,
                        intersectionDocManifest=intersectionsManifestDoc,
                        loadedIntersectionDocs=loadedIntersectionDocs,
                        yieldedIntersectionIds=yieldedIntersectionIds):
                    yield i

            else:
                if viewportBoundsConfig == ViewportBoundsConfig.BOTH:
                    # Split the viewport into LHS and RHS about zero degrees.
                    viewportLHS, viewportRHS = self.__split_viewport_down_0(
                            viewportBounds=viewportBounds)
                else:
                    # Split the viewport into LHS and RHS about 180 degrees.
                    viewportLHS, viewportRHS = self.__split_viewport_down_180(
                            viewportBounds=viewportBounds)
                
                # LHS
                for i in self.__check_chunks_in_view(
                        viewportBounds=viewportLHS,
                        manifestDoc=manifestDoc,
                        specialManifestDoc=specialManifestDoc,
                        viewMode=viewMode,
                        viewportConfig=ViewportBoundsConfig.LHS,
                        intersectionDocManifest=intersectionsManifestDoc,
                        loadedIntersectionDocs=loadedIntersectionDocs,
                        yieldedIntersectionIds=yieldedIntersectionIds):
                    yield i
                
                # RHS
                for i in self.__check_chunks_in_view(
                        viewportBounds=viewportRHS,
                        manifestDoc=manifestDoc,
                        specialManifestDoc=specialManifestDoc,
                        viewMode=viewMode,
                        viewportConfig=ViewportBoundsConfig.RHS,
                        intersectionDocManifest=intersectionsManifestDoc,
                        loadedIntersectionDocs=loadedIntersectionDocs,
                        yieldedIntersectionIds=yieldedIntersectionIds):
                    yield i

            return True
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.class_error_handler(fileName=__filename__,
                    className=self.__classname__, 
                    methodName='get_chunks_in_view_stream', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            return False
    
    

    # ? PRIVATE METHODS --------------------------------------------------------
    
    def __split_viewport_down_180(self, 
            viewportBounds: ChunkBoundingBox
            ) -> Tuple[ChunkBoundingBox, ChunkBoundingBox]:
        """ Splits the viewport in two, about the 180 degree line. 
        
        RETURNS:
        - On success, the method returns a `Tuple` containing the LHS and RHS
            viewport bounds respectively.
        """
        v1 = viewportBounds.get_top_right_vert()
        v2 = viewportBounds.get_bottom_left_vert()
        
        viewportLHS = ChunkBoundingBox(
            topRightLat=v1.get_lat(),
            topRightLng=180.0,
            bottomLeftLat=v2.get_lat(),
            bottomLeftLng=v2.get_lng())

        viewportRHS = ChunkBoundingBox(
            topRightLat=v1.get_lat(),
            topRightLng=v1.get_lng(),
            bottomLeftLat=v2.get_lat(),
            bottomLeftLng=-180.0)
        
        return (viewportLHS, viewportRHS)

    def __split_viewport_down_0(self, viewportBounds: ChunkBoundingBox):
        """ Splits the viewport in two, about the 0 degree line. 
        
        RETURNS:
        - On success, the method returns a `Tuple` containing the LHS and RHS
            viewport bounds respectively.
        """
        v1 = viewportBounds.get_top_right_vert()
        v2 = viewportBounds.get_bottom_left_vert()
        
        viewportLHS = ChunkBoundingBox(
            topRightLat=v1.get_lat(),
            topRightLng=0,
            bottomLeftLat=v2.get_lat(),
            bottomLeftLng=v2.get_lng())

        viewportRHS = ChunkBoundingBox(
            topRightLat=v1.get_lat(),
            topRightLng=v1.get_lng(),
            bottomLeftLat=v2.get_lat(),
            bottomLeftLng=0)
        
        return (viewportLHS, viewportRHS)

    def __check_chunks_in_view_helper(self,
            chunk: Chunk,
            loadedIntersectionDocs: Dict[str, Optional[IntersectionsDoc]],
            intersectionDocManifest: IntersectionsDocManifest,
            yieldedIntersectionIds: Dict[str, bool]):
        """ Helper method for checking chunks in view. 
        
        Determines if the method has previously yielded intersections.
        """
        try:
            # Go through each intersection and check if we've 
            #   yielded it in a previous chunk
            for intersectionId in \
                    chunk.get_intersections_ids_list():
                
                # Check have we previously yielded this intersection
                if intersectionId in yieldedIntersectionIds:
                    continue
                    
                # We haven't yielded this intersection id before,
                #   let's add it
                yieldedIntersectionIds[intersectionId] = True

                # Find where this intersection is in the manifest
                for instance in intersectionDocManifest.\
                        get_instances_list():
                    if int(intersectionId) in \
                            instance.get_list_of_tscs():
                        # Check if we have loaded this particular intersection
                        # before
                        if isinstance(
                                loadedIntersectionDocs[instance.get_doc_id()], 
                                IntersectionsDoc):
                            
                            currentIntersectionDoc = loadedIntersectionDocs[
                                    instance.get_doc_id()]
                        else:
                            # Intersection doc has not been loaded, load it
                            currentIntersectionDoc = self.\
                                    __get_intersection_doc(
                                            instance.get_doc_id())

                            # Check if we loaded the intersection doc correctly
                            if isinstance(currentIntersectionDoc, IntersectionsDoc):
                                loadedIntersectionDocs[instance.get_doc_id()] \
                                        = currentIntersectionDoc
                            else:
                                raise Exception("Error trying to load " + \
                                        "intersection doc: {}".format(
                                        instance.get_doc_id()))
                            
                            # Find the intersection in the document and return 
                            #   it
                            for intersec in \
                                    currentIntersectionDoc.get_intersections():
                                if intersec.get_tsc() == int(intersectionId):
                                    yield intersec
                                    # We don't need to iterate further, 
                                    #   thus break
                                    break
                        # We don't need to go through the other instances since
                        #   we've already found the correct one
                        break
                    
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.class_error_handler(fileName=__filename__,
                    className=self.__classname__, 
                    methodName='__check_chunks_in_view_helper', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            yield None

    def __check_chunks_in_view(self,
            viewportBounds: ChunkBoundingBox,
            viewMode: ChunkType,
            manifestDoc: ChunkManifestDoc,
            specialManifestDoc: ChunkManifestDoc,
            viewportConfig: ViewportBoundsConfig,
            loadedIntersectionDocs: Dict[str, Optional[IntersectionsDoc]],
            intersectionDocManifest: IntersectionsDocManifest,
            yieldedIntersectionIds: Dict[str, bool]):
        """ Goes through the given manifest docs, finds the chunks in view. 
        
        ARGS:

        RETURNS:
        - On success, the method yields a number of chunks, and returns True.
            Otherwise yields no chunks and returns False.
        """
        
        bbh = BoundingBoxHelper()
        idg = IntersectionDocGetter()

        # Determine which document has chunks in our view
        try:
            intersecManifestDoc = idg.get_intersections_manifest_doc()

            if not isinstance(intersecManifestDoc, IntersectionsDocManifest):
                raise Exception(
						"Error trying to get intersections manifest doc")

            for manifest in manifestDoc.get_chunk_manifests():
                if bbh.check_bounding_box_in_view(viewportBounds, 
                        manifest.get_bounding_box()):
                    chunkDoc = self.__get_chunk_doc(viewMode, 
                            manifest.get_doc_id())

                    if not isinstance(chunkDoc, ChunkDoc):
                        raise Exception("Error getting chunk from firestore {}"\
                                .format(manifest.get_doc_id()))

                    # Get the chunks that are in the viewport
                    for chunk in chunkDoc.get_chunks():
                        if bbh.check_bounding_box_in_view(viewportBounds, 
                                chunk.get_bounding_box()):
                            for intersec in self.__check_chunks_in_view_helper(
                                    chunk=chunk,
                                    intersectionDocManifest=intersectionDocManifest,
                                    loadedIntersectionDocs=loadedIntersectionDocs,
                                    yieldedIntersectionIds=yieldedIntersectionIds):
                                if isinstance(intersec, Intersection):
                                    forecastAvailable = 2
                                    if intersec.get_tsc() in \
                                            intersecManifestDoc.\
                                            get_prediction_tscs_as_list():
                                        forecastAvailable = 1
                                    # Yield the normal chunks
                                    yield pb2.ChunkGetter_Response(
                                        status=pb2.STREAM_OK,
                                        chunksJSON=json.dumps(
                                                intersec.to_dict()),
                                        errMsg='',
                                        forecastAvailable=forecastAvailable)
            
            # TODO
            if viewportConfig == ViewportBoundsConfig.LHS:
                # LHS
                for sManifest in specialManifestDoc.get_chunk_manifests():
                    chunkManifestBoundingBox = sManifest.get_bounding_box()
                    
                    v1 = chunkManifestBoundingBox.get_top_right_vert()
                    v1.set_lng(v1.get_lng() + 360.0)
                    chunkManifestBoundingBox.set_top_right_vert(v1)

                    if bbh.check_bounding_box_in_view(
                            viewportBounds=viewportBounds,
                            chunkBoundingBox=chunkManifestBoundingBox):
                        chunkDoc = self.__get_special_chunk_doc(viewMode,
                                sManifest.get_doc_id())
                        
                        if not isinstance(chunkDoc, ChunkDoc):
                            raise Exception("Error getting chunk from firestore {}"\
                                    .format(sManifest.get_doc_id()))

                        # Get the chunks that are in the viewport
                        for chunk in chunkDoc.get_chunks():
                            chunkBB = chunk.get_bounding_box()
                            if chunk.get_chunk_mode() == ChunkMode.SPLIT:
                                v1 = chunkBB.get_top_right_vert()
                                v1.set_lng(v1.get_lng() + 360.0)
                                chunkBB.set_top_right_vert(v1)

                            if bbh.check_bounding_box_in_view(
                                    viewportBounds=viewportBounds,
                                    chunkBoundingBox=chunkBB):
                                for intersection in self.__check_chunks_in_view_helper(
                                        chunk=chunk,
                                        intersectionDocManifest=intersectionDocManifest,
                                        loadedIntersectionDocs=loadedIntersectionDocs,
                                        yieldedIntersectionIds=yieldedIntersectionIds):
                                    if isinstance(intersection, Intersection):
                                        # Yield the normal chunks
                                        yield pb2.ChunkGetter_Response(
                                            status=pb2.STREAM_OK,
                                            chunksJSON=json.dumps(
                                                    intersection.to_dict()),
                                            errMsg='')
                                    else:
                                        # Something has gone wrong, return.
                                        return
                                

            elif viewportConfig == ViewportBoundsConfig.RHS:
                # RHS
                for sManifest in specialManifestDoc.get_chunk_manifests():
                    chunkManifestBoundingBox = sManifest.get_bounding_box()
                    v2 = chunkManifestBoundingBox.get_bottom_left_vert()
                    v2.set_lng(v2.get_lng() - 360.0)
                    chunkManifestBoundingBox.set_bottom_left_vert(v2)
                        

                    if bbh.check_bounding_box_in_view(
                            viewportBounds=viewportBounds,
                            chunkBoundingBox=chunkManifestBoundingBox):
                        chunkDoc = self.__get_special_chunk_doc(viewMode,
                                sManifest.get_doc_id())
                        
                        if not isinstance(chunkDoc, ChunkDoc):
                            raise Exception("Error getting chunk from firestore {}"\
                                    .format(sManifest.get_doc_id()))

                        # Get the chunks that are in the viewport
                        for chunk in chunkDoc.get_chunks():
                            chunkBB = chunk.get_bounding_box()
                            if chunk.get_chunk_mode() == ChunkMode.SPLIT:
                                v2 = chunkBB.get_bottom_left_vert()
                                v2.set_lng(v2.get_lng() - 360.0)
                                chunkBB.set_bottom_left_vert(v2)
                            
                            if bbh.check_bounding_box_in_view(
                                    viewportBounds=viewportBounds,
                                    chunkBoundingBox=chunkBB):
                                for intersection in self.__check_chunks_in_view_helper(
                                        chunk=chunk,
                                        intersectionDocManifest=intersectionDocManifest,
                                        loadedIntersectionDocs=loadedIntersectionDocs,
                                        yieldedIntersectionIds=yieldedIntersectionIds):
                                    if isinstance(intersection, Intersection):
                                        # Yield the normal chunks
                                        yield pb2.ChunkGetter_Response(
                                            status=pb2.STREAM_OK,
                                            chunksJSON=json.dumps(
                                                    intersection.to_dict()),
                                            errMsg='')
                                    else:
                                        # Something has gone wrong, return.
                                        return
                            

            yield pb2.ChunkGetter_Response(
                status=pb2.STREAM_ENDED,
                chunksJSON="",
                errMsg='')
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.class_error_handler(fileName=__filename__,
                    className=self.__classname__,
                    methodName='__check_chunks_in_view', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            raise e
        
    
    def __get_chunk_manifest(self, chunkType: ChunkType):
        """ Gets the manifest for a particular chunk type. """
        if chunkType == ChunkType.FAR_CHUNK:
            docSnapshot = self.chunksRef.document('far_chunk_manifest')\
                    .get()  # type: ignore
        elif chunkType == ChunkType.STANDARD_CHUNK:
            docSnapshot = self.chunksRef.document('standard_chunk_manifest')\
                    .get()  # type: ignore
        elif chunkType == ChunkType.NEAR_CHUNK:
            docSnapshot = self.chunksRef.document('near_chunk_manifest')\
                    .get()  # type: ignore
        else:
            docSnapshot = self.chunksRef.document('close_chunk_manifest')\
                    .get()  # type: ignore

        if not docSnapshot.exists:
                raise Exception("Type {} Chunk manifest doesn't exist".format(
                        chunkType.value))

        # Convert the manifest to an object
        cmof = ChunkManifestObjectFactory()
        return cmof.to_obj_v_0_3_0(docSnapshot.to_dict())  # type: ignore


    def __get_special_chunk_manifest(self, chunkType: ChunkType):
        """ Gets the manifest for a particular special chunk type. 
        
        The `special` part, refers to chunks that are either `on-the-line` or
            `split` along the abrupt 180 to -180 degree jump in longitude.
        """ 
        if chunkType == ChunkType.FAR_CHUNK:
            docSnapshot = self.chunksRef.document('special_far_chunk_manifest')\
                    .get()  # type: ignore
        elif chunkType == ChunkType.STANDARD_CHUNK:
            docSnapshot = self.chunksRef.document('special_standard_chunk_manifest')\
                    .get()  # type: ignore
        elif chunkType == ChunkType.NEAR_CHUNK:
            docSnapshot = self.chunksRef.document('special_near_chunk_manifest')\
                    .get()  # type: ignore
        else:
            docSnapshot = self.chunksRef.document('special_close_chunk_manifest')\
                    .get()  # type: ignore

        if not docSnapshot.exists:
                raise Exception("Type {} Special Chunk manifest doesn't exist".format(
                        chunkType.value))

        # Convert the manifest to an object
        cmof = ChunkManifestObjectFactory()
        return cmof.to_obj_v_0_3_0(docSnapshot.to_dict())  # type: ignore

    def __get_special_chunk_doc(self, chunkType: ChunkType, chunkDocId: str):
        """ Gets the special chunk doc. """
        if chunkType == ChunkType.FAR_CHUNK:
            docRef = self.chunksRef.document('special_far_chunk_manifest')
        elif chunkType == ChunkType.STANDARD_CHUNK:
            docRef = self.chunksRef.document('special_standard_chunk_manifest')
        elif chunkType == ChunkType.NEAR_CHUNK:
            docRef = self.chunksRef.document('special_near_chunk_manifest')
        else:
            docRef = self.chunksRef.document('special_close_chunk_manifest')
        
        # Get the specific chunk doc
        docSnapshot = docRef.collection('chunk_data').document(chunkDocId).get()

        if not docSnapshot.exists:
            raise Exception("Type {} Chunk doc {} doesn't exist".format(
                        chunkType.value, chunkDocId))

        cdof = ChunkDocObjectFactory()
        return cdof.to_obj_v_0_3_0(docSnapshot.to_dict())

    def __get_intersections_manifest_doc(self):
        """ Gets the intersection manifest doc. """
        docSnapshot = self.intersectionsRef.document(
                'intersections_doc_manifest').get() # type: ignore
        
        if not docSnapshot.exists:
            raise Exception("Error trying to read intersections manifest")
        
        imof = IntersectionsManifestObjectFactory()
        return imof.to_obj_v_0_3_0(docSnapshot.to_dict())   # type: ignore

    def __get_intersection_doc(self, docId: str):
        """ Gets a specified intersection doc. """
        docSnapshot = self.intersectionsRef.document(
                'intersections_doc_manifest').collection("intersection_docs")\
                .document(docId).get()
        
        if not docSnapshot.exists:
            raise Exception("Error trying to read intersection doc: {}"\
                    .format(docId))

        idof = IntersectionDocObjectFactory()
        return idof.to_obj_v_0_3_0(docSnapshot.to_dict())

    def __get_chunk_doc(self, chunkType: ChunkType, chunkDocId: str):
        """ Gets the chunk doc """
        if chunkType == ChunkType.FAR_CHUNK:
            docRef = self.chunksRef.document('far_chunk_manifest')
        elif chunkType == ChunkType.STANDARD_CHUNK:
            docRef = self.chunksRef.document('standard_chunk_manifest')
        elif chunkType == ChunkType.NEAR_CHUNK:
            docRef = self.chunksRef.document('near_chunk_manifest')
        else:
            docRef = self.chunksRef.document('close_chunk_manifest')

        # Get the specific chunk doc
        docSnapshot = docRef.collection('chunk_data').document(chunkDocId).get()

        if not docSnapshot.exists:
                raise Exception("Type {} Chunk doc {} doesn't exist".format(
                        chunkType.value, chunkDocId))

        cdof = ChunkDocObjectFactory()
        return cdof.to_obj_v_0_3_0(docSnapshot.to_dict())
# * TRANSACTIONS ---------------------------------------------------------------