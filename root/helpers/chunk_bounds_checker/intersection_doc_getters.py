# Standard libs
import sys
from typing import List

# Third party libs
from google.cloud import firestore

# Local libs
from root.helpers.common_helper.common_firestore_helper import \
        FirestoreErrorHelper

# Base classes 

from root.base_classes.intersections_doc import IntersectionsDocManifest
from root.base_classes.intersections_doc import IntersectionDocManifestInstance

from root.base_classes.base_doc import BaseDocType

# * DEBUG ----------------------------------------------------------------------

__filename__ = 'intersection_doc_getters.py'

# * ENUMS ----------------------------------------------------------------------

class IntersectionsManifestObjectFactory(object):
    def __init__(self):
        self.__classname__ = 'IntersectionsManifestObjectFactory'
    
    def to_obj_v_0_3_0(self, inputDict: dict):
        """ Converts the firestore dict to its corresponding object. 
        
        VERSION:
        - This method supports doc_version 0.3.0

        ARGS:
        - inputDcict: The dictionary recieved from firestore

        RETURNS: 
        - On success, the method returns a `IntersectionsDocManifest`. Otherwise
            returns `None`
        """
        try:
            docVersion = inputDict['doc_version'].split('.')

            # Get all of the manifests
            manifestsList = self.__manifest_instance_to_obj(
                    inputDict=inputDict['instances'])

            # Put together the intersections manifest doc
            return IntersectionsDocManifest(instancesList=manifestsList,
                    docType=BaseDocType(inputDict['doc_type']),
                    createTime=inputDict['create_time'],
                    lastEditted=inputDict['last_editted'],
                    ownerId=inputDict['owner_id'],
                    major=int(docVersion[0]),
                    minor=int(docVersion[1]),
                    patch=int(docVersion[2]),
                    predictionTscs=inputDict['prediction_tscs'])
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.class_error_handler(fileName=__filename__,
                    className=self.__classname__, 
                    methodName='to_obj_v_0_3_0', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            return None
        

    def __manifest_instance_to_obj(self, inputDict: dict):
        manifestsList: List[IntersectionDocManifestInstance] = []
        for instance in inputDict:
            newInstance = IntersectionDocManifestInstance(
                listOfTscs=instance['list_of_tscs'],
                currentCapacity=instance['current_capacity'],
                docId=instance['doc_id'])
            
            manifestsList.append(newInstance)
        
        return manifestsList

# * AUXILIARY HELPERS ----------------------------------------------------------

class IntersectionDocGetter:
    def __init__(self):
        self.db = firestore.Client()

        self.transaction = self.db.transaction()

        self.chunksRef = self.db.collection(u'chunks') # type: ignore

        self.intersectionsRef = self.db.collection(u'intersections') # type: ignore

        self.__classname__ = 'IntersectionDocGetter'

    def get_intersections_manifest_doc(self):
        """ Gets the intersection manifest doc. """
        docSnapshot = self.intersectionsRef.document(
                'intersections_doc_manifest').get() # type: ignore
        
        if not docSnapshot.exists:
            raise Exception("Error trying to read intersections manifest")
        
        imof = IntersectionsManifestObjectFactory()
        return imof.to_obj_v_0_3_0(docSnapshot.to_dict())   # type: ignore

# * TRANSACTIONS ---------------------------------------------------------------