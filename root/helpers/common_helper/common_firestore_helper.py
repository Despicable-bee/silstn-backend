# Standard libs
import time
import logging
from random import randint
import uuid
import sys
import string
import random

from typing import Optional

# * DEBUG ----------------------------------------------------------------------

__filename__ = 'common_firestore_helper.py'

# * ENUMS ----------------------------------------------------------------------

# * HELPERS --------------------------------------------------------------------

class FirestoreDocSizeHelper:
    def __init__(self):
        # Maximum size of a firestore document (in bytes)
        self.firestoreDocMaxBytes = 1048576 # 1 MiB

        # Size of a boolean stored in firestore
        self.booleanSizeBytes = 1

        # Size of a date and time number in firestore
        self.dateAndTimeSizeBytes = 8

        # Size of a floating point number in firestore
        self.floatSizeBytes = 8

        # Size of a geolocation point in firestore.
        self.geographicalPointSizeBytes = 16

        # Size of an integer in firestore.
        self.integer = 8
    
    def get_firestore_doc_max_size(self):
        """ Returns the max size of a firestore document in bytes
        """
        return self.firestoreDocMaxBytes

    def get_firestore_max_integer_size(self):
        """ Returns the max value of a signed 64 bit integer. """
        return 9223372036854775807
 

    def firestore_doc_size_calc(self, docAbsPath: str, docName: str,
            docDict: dict):
        """ Calculates the size of the firestore document in bytes.

        ARGS:
        - docAbsPath: The absolute path to the document from root (excluding
            name of document)
        - docName: Name of the document.
        - docDict: Dictionary containing the document name
        
        RETURNS:
        - On success, the method returns the number of bytes the firestore doc
                takes up. Otherwise returns `None`
        """
        # Compute the size of all the strings (num chars + 1 = bytes)
        # e.g. jeff = 4 + 1 = 5 bytes

        # Compute document name size 
        # (path to doc chars + document_id + 16 = bytes)
        # e.g. users/jeff/tasks/my_task_id = 6 + 5 + 6 + 11 + 16 = 44 bytes
        cumSubAbsPathLen = 0
        for word in docAbsPath.split('/'):
            cumSubAbsPathLen += len(word) + 1

        docNameSizeBytes = cumSubAbsPathLen + (len(docName) + 1) + 16

        # Compute the size of each field in the docDict
        cumulativeDocSizeSum = 0
        for fieldString in docDict:
            cumulativeDocSizeSum += self.__firestore_size_calc_handler(
                    fieldString, docDict[fieldString])
        
        return docNameSizeBytes + cumulativeDocSizeSum + 32
    
    def __firestore_size_calc_handler(self, fieldKey: str, fieldValue):
        if isinstance(fieldValue, int) or \
                isinstance(fieldValue, float):
            return self.__firestore_handle_int_float_size_calc(fieldKey)

        elif isinstance(fieldValue, str):
            return self.__firestore_handle_string_size_calc(fieldKey, 
                    fieldValue)

        elif isinstance(fieldValue, bool):
            return self.__firestore_handle_bool_size_calc(fieldKey)

        elif isinstance(fieldValue, list):
            return self.firestore_handle_list_size_calc(fieldKey, fieldValue)

        elif isinstance(fieldValue, dict):
            return self.firestore_handle_dict_size_calc(fieldKey, fieldValue)

        else:
            # ! UNHANDLED thing !
            print("Unfactored type")
            print(type(fieldValue))

    def __firestore_handle_bool_size_calc(self, fieldName: str):
        """ Booleans firestore size calc: 
        - (num chars in field + 1) + (1 byte for the boolean)
        """
        return (len(fieldName) + 1) + 1
    
    def __firestore_handle_string_size_calc(self, fieldName: str, value: str):
        """ Strings firestore size calc: 
        - (num chars in field + 1) + (num chars in value + 1)
        """
        return (len(fieldName) + 1) + (len(value) + 1)
    
    def __firestore_handle_int_float_size_calc(self, fieldName: str):
        """ Integer/Float firestore size calc:
        - (num chars in field + 1) + (8 bytes for integer/float)
        """
        return (len(fieldName) + 1) + 8
    
    def firestore_handle_list_size_calc(self, fieldName: str, valueList):
        fieldNameLen = len(fieldName) + 1
        listCumulativeSizeBytes = 0
        
        for item in valueList:
            listCumulativeSizeBytes += \
                    self.__firestore_size_calc_handler("", item)

        return fieldNameLen + listCumulativeSizeBytes

    def firestore_handle_dict_size_calc(self, fieldName: str, valueDict):
        """ Dict firestore size calc:
        - name of the dict + sum of each string size + sum of each field size +
            32 bytes.
        """
        fieldNameLen = len(fieldName) + 1
        dictCumulativeSizeBytes = 0

        for item in valueDict:
            dictCumulativeSizeBytes += \
                self.__firestore_size_calc_handler(item, valueDict[item])

        return fieldNameLen + dictCumulativeSizeBytes + 32


class FirestoreIdGenerator:
    def __init__(self):
        self.genChars = "0123456789abcdefghijklmnopqrstuvwxyz"

        self.__classname__ = "FirestoreIdGenerator"

        self.userIdLen = 8
    
    # * USER PROFILE ID(s) -----------------------------------------------------

    def generate_user_id(self):
        """ Generates a user id. """
        return "usr{}".format(self.__alphanumeric_generator(7))

    def generate_vertex_id(self):
        """ Generates a vertex id. """
        return 'vrt{}'.format(self.__alphanumeric_generator(10))

    # * CHUNK IDS --------------------------------------------------------------
    def generate_far_chunk_id(self):
        """ Generates a far chunk id. 
        
        36^2 = 1,296 (320 required)
        """
        return 'fch{}'.format(self.id_generator(2))

    def generate_standard_chunk_id(self):
        """ Generates a standard chunk id. 
        
        36^3 = 46,656 (5,120 required)
        """
        return 'sch{}'.format(self.id_generator(3))

    def generate_near_chunk_id(self):
        """ Generates a near chunk id.

        36^4 = 1,679,616 (81,920 required)
        
        """
        return "nch{}".format(self.id_generator(4))

    def generate_close_chunk_id(self):
        """ Generates a close chunk id. 
        
        36^4 = 1,679,616 (1,310,720 required)"""
        return "cch{}".format(self.id_generator(4))

    # * CHUNK DOC IDS ----------------------------------------------------------
    def generate_far_chunk_doc_id(self):
        """ Generates a far chunk id for a document. 
        
        36^2
        """
        return 'fcd{}'.format(self.id_generator(2))

    def generate_standard_chunk_doc_id(self):
        """ Generates a standard chunk id for a document.

        36^2
        """
        return 'scd{}'.format(self.id_generator(2))

    def generate_near_chunk_doc_id(self):
        """ Generates a near chunk id for a document.

        36^3
        """
        return 'ncd{}'.format(self.id_generator(3))

    def generate_close_chunk_doc_id(self):
        """ Generates a close chunk id for a document. 
        
        36^4
        """
        return 'ccd{}'.format(self.id_generator(4))

    # * INTERSECTION DATA DOCS -------------------------------------------------

    def generate_intersection_data_doc(self):
        """ Generates an intersection data doc id. 
        
        36^10
        """
        return 'idd{}'.format(self.id_generator(10))

    def __alphanumeric_generator(self, numChars: int) -> str:
        """ Generates a string of alphanumeric characters of a specified length.

        This method generates a random alphanumeric id that is outside the 
        reserved ids range. It does NOT guarentee uniqueness of said id.

        ARGS:
        - numChars: The number of alphanumeric characters we want to generate

        RETURNS:
        - A string containing the generated characters
        """
        ident = ""
        while True:
            # Generate the characters
            for i in range(0, numChars):
                ident += self.genChars[randint(0, len(self.genChars) - 1)]
            # Check if the generated characters are within the range of reserved
            # id's
            break
        return ident

    def id_generator(self, N: int):
        return ''.join(random.choices(string.ascii_lowercase + string.digits, k=N))
    
    def __check_reserve_ids(self, idToCheck: str) -> bool:
        """ Checks if the generated id is not in the reserve ids range. 
        
        For various reasons, we will need a guarenteed number of ids reserved.

        NOTE
        - This only applies for public ids. relative ids like project ids don't
            matter (as user projects are stored in a subcollection below the 
            users profile document.)

        approximately 1000 should do, hence this method checks if a particular
        alphanumeric id is within that range

        i.e. <prefix>00000000 -> <prefix>00001000
        """
        numbers = '0123456789'
        lastFourCharactersIndex = len(idToCheck) - 4
        

        # if there is any alphabetical character in there, return
        for i in range(0, len(idToCheck)):
            if idToCheck[i] not in numbers:
                return False
        
        # If we pass this point, then the id is all numbers
        if int(idToCheck) > 1000:
            return False
        else:
            return True

class FirestoreTokenHelper:
    def __init__(self):
        self.__classname__ = 'FirestoreTokenHelper'
        pass

    def generate_refresh_token(self):
        """ Generates a new refresh token for a given user and records it.
        
        The user will need to refresh their JWT in order to access the various
        API methods that require authentication. 

        NOTE:
        The programmer is responsible for writing the refresh token to the users
        profile after generation.

        RETURNS:
        - On success, the method returns the refresh token. Otherwise returns
            None.
        """

        try:
            return str(uuid.uuid4())
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.class_error_handler(fileName=__filename__,
                    className=self.__classname__, 
                    methodName='create_new_user_profile', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            return None

class FirestoreTimeHelper:
    def __init__(self):
        pass

    def get_unix_epoch_timestamp_ms(self):
        """ Returns the current time in milliseconds since the epoch.
        
        Epoch = 00:00:00 Jan 1st 1970
        """
        return round(time.time() * 1000)

    def get_unix_epoch_timestamp_sec(self):
        """ Returns the current time in seconds since the epoch. """
        return round(time.time())

    def get_one_hour_in_seconds(self):
        return 3600

    def get_one_day_in_seconds(self):
        return 86400

    def get_one_week_in_seconds(self):
        return 604800

    def get_one_month_in_seconds(self):
        """ 30.44 days """
        return 2629743

    def get_one_year_in_seconds(self):
        """ 365.24 days """
        return 31556926

    def sec_to_ms(self, unixTimestampSeconds: int):
        """ Converts a given timestamp from seconds to milliseconds """
        return round(unixTimestampSeconds * 1000)

    def ms_to_sec(self, unixTimestampMilliseconds: int):
        """ Converts a given timestamp from milliseconds to seconds (python) 
        
        ARGS:
        - unixTimestampMilliseconds: The unix timestamp in milliseconds
        """
        return unixTimestampMilliseconds / 1000.0

class ContextMetadataAsClass:
    def __init__(self, jwt: str, ipAddress: str):
        self.__jwt = jwt
        self.__ipAddress = ipAddress

    def get_jwt(self):
        return self.__jwt

    def get_ip_address(self):
        return self.__ipAddress

class FirestoreMetadataHelper:
    def __init__(self):
        self.__classname__ = 'FirestoreMetadataHelper'

    def get_context_metadata(self, metadata: dict
            ) -> Optional[ContextMetadataAsClass]:
        """ Extracts the relevant metadata elements from the metadata sequence. 
        
        The context.invocation_metadata() method will return a dict, which we
        can use to extract the following:

        - client ip address
        - client jwt

        ARGS:
        - metadata: The metadata dict we want to extract info from

        RETURNS:

        """
        try:
            jwt = ""
            ipAddress = ""

            # Get the users jwt and ip address
            for item in metadata:
                if item[0] == 'x-forwarded-authorization':
                    jwt = (item[1]).replace('Bearer ', "")
                elif item[0] == 'forwarded':
                    ipAddress = item[1]
            
            return ContextMetadataAsClass(jwt, ipAddress)
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.class_error_handler(fileName=__filename__,
                    className=self.__classname__, 
                    methodName='create_new_user_profile', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            return None

class FirestoreErrorHelper:
    def __init__(self):
        pass
    
    # ? Error messages ---------------------------------------------------------

    def get_server_error_message(self):
        """ Generic server error message. 
        
        When stuff goes wrong due to my garbage programming skills, we need
        to break it to the users gracefully that something went wrong but it
        isn't there fault.
        """
        return "ServerError: Please try again later"

    def get_claims_error_message(self):
        """ Unable to get claims from JWT error message. 
        
        When the JWT is malformed, and somehow gets passed ESPv2, then this 
        method should fire.
        """
        return "ServerError: Possibly malformed JWT, please logout/login"

    def get_invalid_inputs_error(self):
        """
        When the input from the user was invalid, return this.
        """
        return "InvalidInput: Request inputs invalid"
    

    # ? Exception / Error handlers ---------------------------------------------

    def transaction_error_handler(self, fileName: str, 
            methodName: str, exceptionMsg: Exception, lineNum: int) -> None:
        """ General purpose error handler for firestore transaction faults. 
        
        Logs the relevant information about the exception / error.

        ARGS:
        - fileName: What file this error came from
        - methodName: What method this error occured in
        - exceptionMsg: The exception that happened
        - lineNum: The line this exception occured on (within the try-except)
        """

        formattedString = ("ERROR:\n\t\
                File -> [ {} ]\n\t\
                Class -> [ {} ]\n\t\
                Method -> [ {} ]\n\t\
                Line Num -> [ {} ]".format( 
                        fileName, 
                        'transaction',
                        methodName, 
                        lineNum))

        logging.error(formattedString)

        logging.error("EXCEPTION: {}".format(exceptionMsg))

    def api_error_handler(self, fileName: str, 
            methodName: str, exceptionMsg: Exception, lineNum: int) -> None:
        """ General purpose error handler for firestore transaction faults. 
        
        ARGS:
        - fileName: What file this error came from
        - methodName: What method this error occured in
        - exceptionMsg: The exception that happened
        - lineNum: The line this exception occured on (within the try-except)
        """

        logging.error("ERROR:\n\t\
                File -> [ {} ]\n\t\
                Class -> [ {} ]\n\t\
                Method -> [ {} ]\n\t\
                Line Num -> [ {} ]".format( 
                        fileName, 
                        'API',
                        methodName, 
                        lineNum))
        
        logging.error("EXCEPTION: {}".format(exceptionMsg))

    def class_error_handler(self, fileName: str, className: str, 
            methodName: str, exceptionMsg: Exception, lineNum: int) -> None:
        """ Error handler for classes.

        ARGS:
        - fileName: What file this error came from
        - className: What class this error came from
        - methodName: What method this error occured in
        - exceptionMsg: The exception that happened
        - lineNum: The line this exception occured on (within the try-except)
        """

        logging.error("ERROR:\n\t\
                File -> [ {} ]\n\t\
                Class -> [ {} ]\n\t\
                Method -> [ {} ]\n\t\
                Line Num -> [ {} ]".format( 
                        fileName, 
                        className,
                        methodName, 
                        lineNum))
        
        logging.error("EXCEPTION: {}".format(exceptionMsg))