# Standard Libs
from alive_progress import alive_bar
import requests

from typing import List
from typing import Dict

from enum import Enum
from enum import auto

import time
import sys

import datetime

# Third Party Libs

from google.cloud import firestore

# Local Libs

from root.helpers.common_helper.common_firestore_helper import \
        FirestoreTimeHelper
from root.helpers.common_helper.common_firestore_helper import \
        FirestoreIdGenerator
from root.helpers.common_helper.common_firestore_helper import \
        FirestoreErrorHelper
from root.helpers.common_helper.common_firestore_helper import \
        FirestoreDocSizeHelper

# Base Classes
from root.base_classes.base_doc import BaseDocType

from root.base_classes.intersec_data_doc import IntersectionTscDataPair, IntersectionsTscData, ManifestInstance
from root.base_classes.intersec_data_doc import IntersectionDataDoc
from root.base_classes.intersec_data_doc import IntersectionDataManifestDoc

from root.base_classes.intersec_data_doc import \
        IntersectionDataManifestDocObjectFactory
from root.base_classes.intersec_data_doc import \
        IntersectionDataDocObjectFactory
from root.helpers.google_maps_traffic_helper.google_maps_traffic_helper import IntersectionsDocObjectFactory


# * DEBUG ----------------------------------------------------------------------

__filename__ = 'data_collector_firestore_helper.py'

# Set this to false to disable debug output.
__DEBUG_ENABLED__ = False

uniqueIdGeneratorExceptionString = "GeneratorExhaustedError"

# * CONSTANTS ------------------------------------------------------------------

intersectionsDataRootColName = 'data'
intersectionsDataManifestDocName = 'manifest'
intersectionsDataDocSubcolName = 'data_docs'

# * ENUM -----------------------------------------------------------------------

class DataCollectorTransactionStatus(Enum):
    TRANSACTION_SUCCESS = auto()
    TRANSACTION_ERROR = auto()

class IntersectionTimeRange(Enum):
    LESS_THAN_ONE_HOUR = auto()
    ONE_HOUR = auto()
    SIX_HOURS = auto()
    TWELVE_HOURS = auto()
    ONE_DAY = auto()
    THREE_DAYS = auto()
    ONE_WEEK = auto()
    ONE_FORTNIGHT = auto()
    ONE_MONTH = auto()
    ONE_YEAR = auto()
    ALL_TIME = auto()

# * TRANSACTION HELPERS --------------------------------------------------------

class TransactionSnippetAuxiliaryHelper(object):
    @staticmethod
    def generate_unique_intersection_data_doc_id(
            transaction: firestore.Transaction,
            dataRef: firestore.CollectionReference):
        fig = FirestoreIdGenerator()
        counter = 0
        try:
            while True:
                docId = fig.generate_intersection_data_doc()

                # Check if the ID exists
                result = dataRef.document(intersectionsDataManifestDocName)\
                        .collection(intersectionsDataDocSubcolName)\
                        .document(docId).get(transaction=transaction)
                    
                if not result.exists:
                    return docId

                counter += 1

                if counter > 100:
                    raise Exception(uniqueIdGeneratorExceptionString)

        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.transaction_error_handler(fileName=__filename__, 
                    methodName='generate_unique_intersection_data_doc_id', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            return None

    @staticmethod
    def add_new_manifest_instance(uniqueId: str, 
            manifDoc: IntersectionDataManifestDoc,
            currentCapacity: int,
            currentTime: int):
        """ Adds a new manifest instance to the manifest doc. 
        
        ARGS:
        - uniqueId: The id of the document the manifest will keep track of.
        - manifDoc: The intersection data manifest doc.
        - currentCapacity: The current capacity of the intersection data doc
                the manifest is keeping track of
        - currentTime: UNIX timestamp of the current server time.
        """
        fdsh = FirestoreDocSizeHelper()

        manifInstance = ManifestInstance(
                currentCapacity=currentCapacity,
                docName=uniqueId,
                endDate=currentTime + 1,
                startDate=currentTime)
        
       
        # Set the current values
        manifDoc.set_current_doc_name(newCurrentDocName=uniqueId)
        manifDoc.set_current_end_epoch(currentTime + 1)
        # manifDoc.set_current_start_epoch(currentTime)
        
        # Add the new manifest instance to the manifest document
        manifInstanceList = manifDoc.get_manifests_list()

        manifInstanceList.append(manifInstance)

        manifDoc.set_manifests_list(manifInstanceList)

        # Determine the new capacity for the manifest

        newSize = fdsh.firestore_doc_size_calc(
                docAbsPath=intersectionsDataRootColName,
                docName=intersectionsDataManifestDocName,
                docDict=manifDoc.to_dict())

        manifDoc.set_current_capacity(newSize)

    @staticmethod
    def create_new_intersection_data_doc(uniqueId: str,
            currentTime: int):
        """ Creates a new intersection data document.
        
        ARGS:
        - uniqueId: The id of the document to be created.
        - currentTime: UNIX timestamp of the current server time.

        RETURNS:
        - An empty `IntersectionDataDoc`

        """ 
        fdsh = FirestoreDocSizeHelper()
        
        # Create the intersection data doc
        dataDoc = IntersectionDataDoc(
            data=[],
            createTime=currentTime,
            docType=BaseDocType.INTERSECTION_DATA_DOC,
            lastEditted=currentTime,
            major=0,
            minor=3,
            patch=0,
            ownerId=uniqueId,
            currentCapacity=0)

        # Compute the current capacity of the intersection doc
        currentCapacity = fdsh.firestore_doc_size_calc(
                docAbsPath='{}/{}'.format(intersectionsDataRootColName,
                        intersectionsDataManifestDocName), 
                docName=uniqueId,
                docDict=dataDoc.to_dict())

        dataDoc.set_current_capacity(newCurrentCapacity=currentCapacity)
        
        return dataDoc

class DataCollectorTransactions(object):
    def __init__(self):
        pass
    
    def add_data_to_intersection_data_doc(self,
            transaction: firestore.Transaction,
            dataDoctoWrite: IntersectionDataDoc,
            dataDocManifest: IntersectionDataManifestDoc,
            dataDocRoot: firestore.CollectionReference,
            dataToAdd: List[IntersectionsTscData],
            currentTime: int):
        """ Adds data to a intersection_data_doc. 
    
        If adding the data to the doc will result in an overflow (i.e. the doc
            being > 1MiB in size), then the method will create a new data doc and 
            save the data there instead.

            It will also modify the manifest doc to point to that doc 
            instead of the previous doc.

        ARGS:
        - transaction: Reference to the firestore Transaction object.
        - dataDocToWrite: The data doc to write
        - dataDocManifest: The manifest doc keeping track of the various data docs.
        - dataDocRoot: Firestore collection reference pointing to the 'root>data'
            collection.
        - dataToAdd: List of IntersectionTscData objects to add to the data doc.
        - currentTime: Unix timestamp of current server time.

        RETURNS:
        - On success, the method returns `TRANSACTION_SUCCESS`. 
            Otherwise returns `TRANSACTION_ERROR`.
        """
        return _add_data_to_intersection_data_doc(
                transaction=transaction,
                dataDoctoWrite=dataDoctoWrite,
                dataDocManifest=dataDocManifest,
                dataDocRoot=dataDocRoot,
                dataToAdd=dataToAdd,
                currentTime=currentTime)

    def set_intersections_manifest(self,
            transaction: firestore.Transaction,
            dataDocRoot: firestore.CollectionReference,
            manifestDoc: IntersectionDataManifestDoc):
        """ Writes a given intersection data manifest doc to firestore. 
        
        ARGS:
        - transaction: Reference to the firestore transaction object
        - dataDocRoot: Reference to the root of the collection we are saving 
                the manifest
        - manifestDoc: Instance of the `IntersectionDataManifestDoc` class that
                we want to write to firestore.

        RETURNS:
        - On success, the method will return a 
            `DataCollectorTransactionStatus.TRANSACTION_SUCCESS` enum. Otherwise
            will return `DataCollectorTransactionStatus.TRANSACTION_ERROR`.
        """
        return _set_intersections_manifest(
                transaction=transaction,
                dataDocRoot=dataDocRoot,
                manifestDoc=manifestDoc)

    def set_intersections_data_doc(self,
            transaction: firestore.Transaction,
            dataDocRoot: firestore.CollectionReference,
            dataDoc: IntersectionDataDoc):
        """ Writes a given intersection data doc to firestore.

        ARGS:
        - transaction: Reference to the firestore transaction object.
        - dataDocRoot: Reference to the root of the collection we are saving 
                the manifest
        - dataDoc: Instance of the `IntersectionDataDoc` class that we want to
                write to firestore.
        
        RETURNS:
        - On success, the method will return a 
            `DataCollectorTransactionStatus.TRANSACTION_SUCCESS` enum. Otherwise
            will return `DataCollectorTransactionStatus.TRANSACTION_ERROR`.
        """
        return _set_intersections_data_doc(
                transaction=transaction,
                dataDocRoot=dataDocRoot,
                dataDoc=dataDoc)
    

# * AUXILIARY HELPERS ----------------------------------------------------------

class DataIndex(object):
    def __init__(self, tsc: int, cmf: int, timestamp: int):
        """ Index for a data entry from the 

        ARGS:
        tsc: The Traffic Signal Controller.
        cmf: Combined Measured Flow.
        timestamp: UNIX timestamp since epoch.
        """
        self.__tsc = tsc
        self.__cmf = cmf
        self.__timestamp = timestamp

    def add_to_cmf(self, mfToAdd: int):
        """ Adds an mf to the current cmf """
        self.__cmf += mfToAdd

    def get_timestamp(self):
        return self.__timestamp

    def get_tsc(self):
        return self.__tsc

    def get_cmf(self):
        return self.__cmf

# * MAIN HELPER ----------------------------------------------------------------

class DataCollectorManifest(object):
    def __init__(self):
        pass

class DataCollectorHelper(object):
    def __init__(self):
        self.db = firestore.Client()

        self.transaction = self.db.transaction()

        self.dataRef = self.db.collection(u'data') # type: ignore

        self.__classname__ = 'DataCollector'

        # URL to grab volume intersection data from
        self.__volumeUrl = 'https://www.data.brisbane.qld.gov.au/' + \
                'data/dataset/' + \
                '56e19d91-d571-4b45-bfdf-f0f00aeb2343/resource/' + \
                '651f7be1-c183-48b5-96a8-fe372e91adab/download/' + \
                'traffic-data-at-int.json'
        pass
    
    # ? PUBLIC METHODS ---------------------------------------------------------

    def DEBUG_get_document_time_ranges(self):
        """ NOTE:
        
        """
        try:
            # Get the manifest doc
            manifDoc = self.__load_manifest_doc()

            # Get the list of manifest instances
            manifInstances = manifDoc.get_manifests_list()

            # Sort the list
            manifInstances.sort(key=lambda x: x.get_start_date(), reverse=True)

            # Print out the manifest instances
            for instance in manifInstances:
                # We need to divide by 1000.0 because python requires the 
                #   timestamp to be in terms of num seconds, with everything 
                #   else being delimited by a decimal place.
                startDate = datetime.datetime.fromtimestamp(
                        instance.get_start_date()/1000.0)
                endDate = datetime.datetime.fromtimestamp(
                        instance.get_end_date()/1000.0)
                print('tsc{} -> Start: {}, End {} '.format(
                        instance.get_doc_name(),
                        startDate,
                        endDate))

        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.class_error_handler(fileName=__filename__,
                    className=self.__classname__, 
                    methodName='get_document_time_ranges', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            return None

    def get_available_ranges(self):
        """ Gets the available time ranges for data. """
        fth = FirestoreTimeHelper()
        try:
            # Get the manifest doc
            manifDoc = self.__load_manifest_doc()

            # Get the start and end epoch dates
            startDate = manifDoc.get_current_start_epoch()
            endDate = manifDoc.get_current_end_epoch()

            totalTime = endDate - startDate
            
            # Return the highest time range available
            if totalTime < fth.sec_to_ms(fth.get_one_hour_in_seconds()):
                return IntersectionTimeRange.LESS_THAN_ONE_HOUR
            elif totalTime < fth.sec_to_ms(6 * fth.get_one_hour_in_seconds()):
                return IntersectionTimeRange.ONE_HOUR
            elif totalTime < fth.sec_to_ms(12 * fth.get_one_hour_in_seconds()):
                return IntersectionTimeRange.SIX_HOURS
            elif totalTime < fth.sec_to_ms(fth.get_one_day_in_seconds()):
                return IntersectionTimeRange.TWELVE_HOURS
            elif totalTime < fth.sec_to_ms(fth.get_one_week_in_seconds()):
                return IntersectionTimeRange.ONE_DAY
            elif totalTime < fth.sec_to_ms(fth.get_one_month_in_seconds()):
                return IntersectionTimeRange.ONE_WEEK
            elif totalTime < fth.sec_to_ms(fth.get_one_year_in_seconds()):
                return IntersectionTimeRange.ONE_MONTH
            elif totalTime > fth.sec_to_ms(fth.get_one_year_in_seconds()):
                return IntersectionTimeRange.ONE_YEAR
            else:
                raise Exception("Error trying to get time range")
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.class_error_handler(fileName=__filename__,
                    className=self.__classname__, 
                    methodName='get_intersection_data', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            return None

    def get_intersection_data(self, tsc: int, timeRange: IntersectionTimeRange):
        """ Gets a list of processed intersection data from firestore.

        ARGS:
        - tsc: The tsc the user wants to get
        - timeRange: The time range the user wants

        RETURNS:
        - On success, the method will return a list of 
            `IntersectionTscDataPairs`. Otherwise will return `None`.
        """
        try:
            # Load the manifest and current data doc object
            manifObj = self.__load_manifest_doc()

            if manifObj == None:
                raise Exception("Error trying to get the manifest doc")
            
            # Determine if the requested tsc is in the allowed list
            

            # Figure out how much data / which documents we need to read in
            #   order to fulfill the request.
            docsToRead = self.__determine_docs_to_read(tsc=tsc, 
                    timeRange=timeRange,
                    manifObj=manifObj)

            totalDocsToRead = len(docsToRead)

            # Read each of the documents and get a list of their data points.
            # listOfDataPoints: List[IntersectionTscDataPair] = []
            for doc in docsToRead:
                dataDoc = self.__load_intersection_data_doc(
                        documentId=doc.get_doc_name())
                
                tempList: List[IntersectionTscDataPair] = []

                # Get the list of data points
                for tscData in dataDoc.get_data():
                    if tscData.get_tsc() == tsc:
                        # Append the list of data points to the end
                        tempList += tscData.get_data_pairs()
                        # Break out of the inner most for loop
                        break
                yield (tempList,totalDocsToRead)               
    
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.class_error_handler(fileName=__filename__,
                    className=self.__classname__, 
                    methodName='get_intersection_data', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            return None

    def get_intersections_data(self, allowedTscs: List[int], 
            timeRange: IntersectionTimeRange):
        """ Gets a list of all the relevant tscs """
        try:
            # Load the manifest and current data doc object
            manifObj = self.__load_manifest_doc()

            if manifObj == None:
                raise Exception("Error trying to get the manifest doc")
            
            # Determine if the requested tsc is in the allowed list
            

            # Figure out how much data / which documents we need to read in
            #   order to fulfill the request.
            docsToRead = self.__determine_docs_to_read(tsc=allowedTscs[0], 
                    timeRange=timeRange,
                    manifObj=manifObj)

            totalDocsToRead = len(docsToRead)
            print("Total docs to read: {}".format(totalDocsToRead))
            
            # Read each of the documents and get a list of their data points.
            # listOfDataPoints: List[IntersectionTscDataPair] = []
            with alive_bar(totalDocsToRead) as bar:
                for doc in docsToRead:
                    dataDoc = self.__load_intersection_data_doc(
                            documentId=doc.get_doc_name())
                    
                    tempDict: Dict[int, List[IntersectionTscDataPair]] = {}

                    # Get the list of data points
                    for tscData in dataDoc.get_data():
                        if tscData.get_tsc() in allowedTscs:
                            # Append the list of data points to the end
                            tempDict[tscData.get_tsc()] = tscData.get_data_pairs()
                            # Break out of the inner most for loop
                    
                    yield (tempDict, totalDocsToRead)  
                    bar()

        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.class_error_handler(fileName=__filename__,
                    className=self.__classname__, 
                    methodName='get_intersections_data', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            return None

    def get_5_min_intervals(self, timeRange: IntersectionTimeRange):
        """ Gets the number of 5 minute intervals for a given time range. """
        if timeRange == IntersectionTimeRange.ONE_HOUR:
            # 60 min * 60 sec * 1000 = ms
            return int(60 / 5)
        elif timeRange == IntersectionTimeRange.SIX_HOURS:
            return int(6 * 60 / 5)
        elif timeRange == IntersectionTimeRange.TWELVE_HOURS:
            return int(12 * 60 / 5)
        elif timeRange == IntersectionTimeRange.ONE_DAY:
            return int(24 * 60 / 5)
        elif timeRange == IntersectionTimeRange.THREE_DAYS:
            return int(3 * 24 * 60 / 5)
        elif timeRange == IntersectionTimeRange.ONE_WEEK:
            return int(7 * 24 * 60 / 5)
        elif timeRange == IntersectionTimeRange.ONE_FORTNIGHT:
            return int(2 * 7 * 24 * 60 / 5)
        elif timeRange == IntersectionTimeRange.ONE_MONTH:
            return int(30 * 24 * 60 / 5)
        else:
            raise Exception("Error trying to determine selected time range")

    def __determine_docs_to_read(self, tsc: int, 
            timeRange: IntersectionTimeRange, 
            manifObj: IntersectionDataManifestDoc):
        """ Determines (roughly) what documents need to be read. 
        
        This is done by first determining how long the section is in ms before 
            looking through the manifest file.
        """

        if timeRange == IntersectionTimeRange.ONE_HOUR:
            # 60 min * 60 sec * 1000 = ms
            timeInMs = 60 * 60 * 1000
        elif timeRange == IntersectionTimeRange.SIX_HOURS:
            timeInMs = 6 * 60 * 60 * 1000
        elif timeRange == IntersectionTimeRange.TWELVE_HOURS:
            timeInMs = 12 * 60 * 60 * 1000
        elif timeRange == IntersectionTimeRange.ONE_DAY:
            timeInMs = 24 * 60 * 60 * 1000
        elif timeRange == IntersectionTimeRange.THREE_DAYS:
            timeInMs = 3 * 24 * 60 * 60 * 1000
        elif timeRange == IntersectionTimeRange.ONE_WEEK:
            timeInMs = 7 * 24 * 60 * 60 * 1000
        elif timeRange == IntersectionTimeRange.ONE_FORTNIGHT:
            timeInMs = 2 * 7 * 24 * 60 * 60 * 1000
        elif timeRange == IntersectionTimeRange.ONE_MONTH:
            timeInMs = 30 * 24 * 60 * 60 * 1000
        else:
            raise Exception("Error trying to determine selected time range")

        # Now go through the manifest object
        listOfManifestInstances = manifObj.get_manifests_list()

        # Sort them instances based on their start date (descending order)
        listOfManifestInstances.sort(key=lambda x: x.get_start_date(), 
                reverse=True)
        
        listOfDocsToGet: List[ManifestInstance] = []
        for manif in listOfManifestInstances:
            # Get how long this segment is
            segmentLength = manif.get_end_date() - manif.get_start_date()
            
            listOfDocsToGet.append(manif)
            
            # Subtract the segment length from the time in ms
            timeInMs -= segmentLength

            if timeInMs <= 0:
                break
        
        # Check we have enough documents to fulfill the request (roughly)
        if timeInMs > 0:
            raise Exception("Error trying to gather enough " + \
                    "data to fulfill request")

        # Return the list of documents we need to read.
        return listOfDocsToGet

    def get_and_process_intersection_data_sample(self):
        """ Gets the volume intersection data from the `__volumeUrl`. 
        
        If retreival is successful, the method will process the data and save it
            to firestore.
        """
        dct = DataCollectorTransactions()
        fth = FirestoreTimeHelper()

        try:
            currentTime = round(fth.get_unix_epoch_timestamp_ms()/60000)*60000

            # load the manifest and current data doc object
            objs = self.__load_current_transaction_data_docs()

            if objs == None:
                raise Exception("Error trying to get manifest " + \
                        "and data doc objects")

            manifObj: IntersectionDataManifestDoc = objs[0]
            dataDocObj: IntersectionDataDoc = objs[1]

            # Get the data from the Brisbane government website
            r = requests.get(self.__volumeUrl)

            if r.status_code != 200:
                # Error handling
                raise Exception("Error trying to retrieve intersection data")

            # Convert the data to a JSON object        
            data = r.json()

            dataDict: Dict[int, int] = {}

            for line in data:
                # Check if this is a tsc that we care about recording 
                # TODO

                # Extract what we need from this line
                self.__intersection_data_line_extractor(line=line, 
                        dataDict=dataDict)

            # Convert the data dict to a list of IntersectionTscData
            dataToAdd = self.__convert_data_dict_to_intersection_tsc_list(
                    dataDict=dataDict,
                    currentTime=currentTime)
        
            # Save the newly acquired points
            dct.add_data_to_intersection_data_doc(
                    transaction=self.transaction,
                    currentTime=currentTime,
                    dataDocManifest=manifObj,
                    dataDocRoot=self.dataRef,
                    dataDoctoWrite=dataDocObj,
                    dataToAdd=dataToAdd)

        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.class_error_handler(fileName=__filename__,
                    className=self.__classname__, 
                    methodName='get_and_process_intersection_data_sample', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            return None

    def initialise_data_collector(self):
        """ Initialises the data collector documents in firestore. 
        
        Creates the following documents:
        - 1 x IntersectionManifestDoc 
        - 1 x IntersectionDataDoc
        
        """
        fth = FirestoreTimeHelper()
        dct = DataCollectorTransactions()
        fdsh = FirestoreDocSizeHelper()

        currentTime = round(fth.get_unix_epoch_timestamp_ms()/60000)*60000

        # Generate a unique data doc id
        uniqueId = self.__generate_unique_data_doc_id()

        if not isinstance(uniqueId, str):
            raise Exception("Error trying to initialise data collector")

        # Create the intersection data doc
        dataDoc = IntersectionDataDoc(
            data=[],
            createTime=currentTime,
            docType=BaseDocType.INTERSECTION_DATA_DOC,
            lastEditted=currentTime,
            major=0,
            minor=3,
            patch=0,
            ownerId=uniqueId,
            currentCapacity=0)

        # Compute the current capacity of the intersection doc
        currentCapacity = fdsh.firestore_doc_size_calc(
                docAbsPath='{}/{}'.format(intersectionsDataRootColName,
                        intersectionsDataManifestDocName), 
                docName=uniqueId,
                docDict=dataDoc.to_dict())

        dataDoc.set_current_capacity(newCurrentCapacity=currentCapacity)

        # Create the instance for the manifest doc
        manifInstance = ManifestInstance(
                currentCapacity=currentCapacity,
                docName=uniqueId,
                endDate=currentTime + 1,
                startDate=currentTime)

        # Create the manifest doc
        manifDoc = IntersectionDataManifestDoc(
            manifestsList=[manifInstance],
            createTime=currentTime,
            currentDocName=uniqueId,
            currentEndEpoch=currentTime + 1,
            currentStartEpoch=currentTime,
            docType=BaseDocType.INTERSECTION_DATA_DOC_MANIFEST,
            lastEditted=currentTime,
            major=0,
            minor=3,
            patch=0,
            ownerId=intersectionsDataManifestDocName,
            currentCapacity=0)

        # Compute the current capacity of the manifest doc
        manifCurrentCapacity = fdsh.firestore_doc_size_calc(
                docAbsPath='{}'.format(intersectionsDataRootColName),
                docName=intersectionsDataManifestDocName,
                docDict=manifDoc.to_dict())

        manifDoc.set_current_capacity(manifCurrentCapacity)

        # Save the manifest doc
        dct.set_intersections_manifest(
                transaction=self.transaction,
                dataDocRoot=self.dataRef,
                manifestDoc=manifDoc)

        # Save the data doc
        dct.set_intersections_data_doc(
                transaction=self.transaction,
                dataDocRoot=self.dataRef,
                dataDoc=dataDoc)



    # ? PRIVATE METHODS --------------------------------------------------------

    def __convert_data_dict_to_intersection_tsc_list(self, 
            dataDict: dict,
            currentTime: int):
        """ Converts a certain dict to a list of `IntersectionTscData` objs 
        
        The dict should have the following form:

        ```
            tsc : mfN
            ----------
            493 : 71
            540 : 32
            ... : ...
        ```

        ARGS:
        - dataDict: Dictionary containing the entries to convert
        """
        listToReturn: List[IntersectionsTscData] = []
        for entry in dataDict:
            if not isinstance(entry, int) or \
                    not isinstance(dataDict[entry], int):
                print("exception")
                raise Exception("Data dictionary is not correctly formatted") 
            listToReturn.append(IntersectionsTscData(
                tsc=entry,
                dataPairs=[IntersectionTscDataPair(
                        timestamp=currentTime,
                        cmf=dataDict[entry])]))
        return listToReturn

    def __load_manifest_doc(self):
        """ Loads the manifest data doc. 
        
        RETURNS:
        - On success, the method returns an `IntersectionDataManifestDoc`.
            Otherwise, raises an `Exception`.
        """
        idmdof = IntersectionDataManifestDocObjectFactory()
        try:
            # Load the manifest
            manifRef = self.dataRef.document(intersectionsDataManifestDocName)\
                    .get()  # type: ignore

            if not manifRef.exists:
                raise Exception("Manifest document does not exist")
            
            # Convert the manifest document to an object
            manifestObj = idmdof.to_obj_v_0_3_0(
                    inputDict=manifRef.to_dict()) # type: ignore

            if not isinstance(manifestObj, IntersectionDataManifestDoc):
                raise Exception("Error trying to convert manifest doc to obj")
            
            return manifestObj
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.class_error_handler(fileName=__filename__,
                    className=self.__classname__, 
                    methodName='__load_manifest_doc', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            raise Exception("Error trying to load the manifest data doc.")
        

    def __load_intersection_data_doc(self, documentId: str):
        """ Loads the intersection data doc. 
        
        ARGS:
        - documentId: The id of the intersection data document that you want to
            load.

        RETURNS:
        - On success, the method returns an `IntersectionDataDoc`. Otherwise,
            raises an `Exception`.
        """
        idof = IntersectionDataDocObjectFactory()
        try:
            # Load the current data doc
            dataDocRef = self.dataRef\
                    .document(intersectionsDataManifestDocName)\
                    .collection(intersectionsDataDocSubcolName)\
                    .document(documentId).get()

            if not dataDocRef.exists:
                raise Exception("Current Data doc does not exist")
            
            # Convert the data document to an object
            dataDocObj = idof.to_obj_v_0_3_0(inputDict=dataDocRef.to_dict())

            if not isinstance(dataDocObj, IntersectionDataDoc):
                raise Exception("Error trying to convert data doc to obj")

            return dataDocObj
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.class_error_handler(fileName=__filename__,
                    className=self.__classname__, 
                    methodName='__load_intersection_data_doc', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            raise Exception("Error trying to load the intersection data doc.")

    def __load_current_transaction_data_docs(self):
        """ Loads the manifest and current intersection data doc. """
        
        idmdof = IntersectionDataManifestDocObjectFactory()
        idof = IntersectionDataDocObjectFactory()
        try:
            # Load the manifest
            manifRef = self.dataRef.document(intersectionsDataManifestDocName)\
                    .get()  # type: ignore
            
            if not manifRef.exists:
                raise Exception("Manifest document does not exist")
            
            # Convert the manifest document to an object
            manifestObj = idmdof.to_obj_v_0_3_0(
                    inputDict=manifRef.to_dict()) # type: ignore

            if not isinstance(manifestObj, IntersectionDataManifestDoc):
                raise Exception("Error trying to convert manifest doc to obj")


            # Load the current data doc
            dataDocRef = self.dataRef\
                    .document(intersectionsDataManifestDocName)\
                    .collection(intersectionsDataDocSubcolName)\
                    .document(manifestObj.get_current_doc_name()).get()

            if not dataDocRef.exists:
                raise Exception("Current Data doc does not exist")
            
            # Convert the data document to an object
            dataDocObj = idof.to_obj_v_0_3_0(inputDict=dataDocRef.to_dict())

            if not isinstance(dataDocObj, IntersectionDataDoc):
                raise Exception("Error trying to convert data doc to obj")

            # Return the manifest and data doc objects as a tuple
            return (manifestObj, dataDocObj)

        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.class_error_handler(fileName=__filename__,
                    className=self.__classname__, 
                    methodName='__load_current_transaction_data_docs', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            return None

    def __intersection_data_line_extractor(self, line: dict, dataDict: dict):
        """ Modifies the state of the data dict.

        Different intersections may have different configurations.

        In accordance with how the algorithm was structured (i.e. assume
            that all input flows = output flows), the flows from each lane 
            are summed, and each instance of the intersection are summed as well

        ARGS:
        - line: The line from the 
        
        """
        if __DEBUG_ENABLED__:
            print('\nLine:')
            print(line)

        cmf = 0
        if 'mf1' in line:
            cmf += line['mf1']
        
        if 'mf2' in line:
            cmf += line['mf2']

        if 'mf3' in line:
            cmf += line['mf3']

        if 'mf4' in line:
            cmf += line['mf4']

        if line['tsc'] in dataDict:
            dataDict[line['tsc']] += cmf
        else:
            dataDict[line['tsc']] = cmf

    def __generate_unique_data_doc_id(self):
        """ Generates a unique intersections data doc id. """
        fig = FirestoreIdGenerator()
        counter = 0
        try:
            while True:
                docId = fig.generate_intersection_data_doc()
                
                # Check if id exists
                result = self.dataRef.document('manifest')\
                        .collection('data_docs').document(docId).get()
                
                if not result.exists:
                    return docId
                
                counter += 1

                if counter > 100:
                    raise Exception(uniqueIdGeneratorExceptionString)

        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.class_error_handler(fileName=__filename__,
                    className=self.__classname__, 
                    methodName='__manifest_instance_to_obj', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
        pass

    def generate_unique_data_doc_manifest_id(self):
        pass

    # ? STATIC METHODS ---------------------------------------------------------

# * TRANSACTION SNIPPETS -------------------------------------------------------


# * TRANSACTIONS ---------------------------------------------------------------

@firestore.transactional
def _add_data_to_intersection_data_doc(
        transaction: firestore.Transaction,
        dataDoctoWrite: IntersectionDataDoc,
        dataDocManifest: IntersectionDataManifestDoc,
        dataDocRoot: firestore.CollectionReference,
        dataToAdd: List[IntersectionsTscData],
        currentTime: int):
    # Preconditions
    # TODO

    fdsh = FirestoreDocSizeHelper()
    tsah = TransactionSnippetAuxiliaryHelper()

    try:
        # Determine how much space the addition will take up
        dataDict = {}
        for d in dataToAdd:
            dataDict = dataDict | d.to_dict()
        
        # for entry in dataDict:
        #     print(entry)
        #     print(dataDict[entry])
        #     input("Press enter to continue")

        sizeToAdd = fdsh.firestore_doc_size_calc(docAbsPath='', docName='', 
                docDict=dataDict)
        
        uniqueId = dataDoctoWrite.get_owner_id()

        # Check if adding this size to the document will exceed its capacity
        #   (~95% to be precise).
        if (dataDoctoWrite.get_current_capacity() + sizeToAdd > \
                fdsh.get_firestore_doc_max_size()) * 0.95:
            # Capacity exceeded, save the current data doc, and then create
            #   a new one.
            currentDocRef = dataDocRoot.document(
                    intersectionsDataManifestDocName).\
                    collection(intersectionsDataDocSubcolName).\
                    document(dataDoctoWrite.get_owner_id())

            # Now create a new document and continue the process.
            uniqueId = tsah.generate_unique_intersection_data_doc_id(
                    transaction=transaction,
                    dataRef=dataDocRoot)

            if uniqueId == None:
                raise Exception("Error trying to create new " + \
                        "intersection data doc id")

            temp = tsah.create_new_intersection_data_doc(uniqueId=uniqueId,
                    currentTime=currentTime)

            if temp == None:
                raise Exception("Error trying to create new " + \
                        "intersection data doc")

            # ! BELOW THIS LINE, NOT MORE READS

            transaction.set(currentDocRef, dataDoctoWrite.to_dict())

            dataDoctoWrite = temp

            currentCapacity = fdsh.firestore_doc_size_calc(
                    docAbsPath='{}/{}'.format(intersectionsDataRootColName, 
                            intersectionsDataManifestDocName),
                    docName=uniqueId,
                    docDict=dataDoctoWrite.to_dict())

            # Create a new instance in the manifest doc
            tsah.add_new_manifest_instance(uniqueId=uniqueId,
                    manifDoc=dataDocManifest,
                    currentCapacity=currentCapacity,
                    currentTime=currentTime)
        
        # Go ahead an add the new data
        dataDocList = dataDoctoWrite.get_data()

        for dtoA in dataToAdd:
            added = False
            # Go through the current list of data and check if the relevant
            #   tsc already exists
            for currentData in dataDocList:
                if currentData.get_tsc() == dtoA.get_tsc():
                    # Get the current data's list of pairs and add a new index
                    #   on the end
                    listOfPairs = currentData.get_data_pairs()
                    for pair in dtoA.get_data_pairs():
                        listOfPairs.append(pair)
                    # Tell the end condition this record does indeed exist, 
                    #   and we don't need to add a new one.
                    added = True
                    # Break out of the the above loop
                    break
            if not added:
                dataDocList.append(dtoA)
        
        dataDoctoWrite.set_data(dataDocList)

        # Compute the new firestore data doc size
        newCapacity = fdsh.firestore_doc_size_calc(docAbsPath='{}/{}'.format(
                intersectionsDataRootColName,
                intersectionsDataManifestDocName),
                docName=dataDoctoWrite.get_owner_id(),
                docDict=dataDoctoWrite.to_dict())
        
        dataDoctoWrite.set_current_capacity(newCapacity)

        # Update the manifest instances current capacity
        for manifest in dataDocManifest.get_manifests_list():
            if manifest.get_doc_name() == dataDoctoWrite.get_owner_id():
                # Note, because this is an object in a list, invoking a class
                #   method will also change the classes state.
                manifest.set_current_capacity(newCapacity)
                manifest.set_end_date(currentTime)
                break
        
        dataDocManifest.set_current_end_epoch(currentTime)

        # Write the documents to firestore
        dataDocRef = dataDocRoot.document(intersectionsDataManifestDocName)\
                .collection(intersectionsDataDocSubcolName)\
                .document(uniqueId)

        transaction.set(dataDocRef, dataDoctoWrite.to_dict())

        manifestDocRef = dataDocRoot.document(intersectionsDataManifestDocName)

        transaction.set(manifestDocRef, dataDocManifest.to_dict())

        return DataCollectorTransactionStatus.TRANSACTION_SUCCESS
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        feh = FirestoreErrorHelper()
        feh.transaction_error_handler(fileName=__filename__, 
                methodName='_add_data_to_intersection_data_doc', 
                exceptionMsg=e,
                lineNum=exc_tb.tb_lineno)   # type: ignore
        return DataCollectorTransactionStatus.TRANSACTION_ERROR

@firestore.transactional
def _set_intersections_manifest(
        transaction: firestore.Transaction,
        dataDocRoot: firestore.CollectionReference,
        manifestDoc: IntersectionDataManifestDoc):
    try:
        # Get the reference to the document
        dataDocRef = dataDocRoot.document(intersectionsDataManifestDocName)

        # Set the document
        transaction.set(dataDocRef, manifestDoc.to_dict())
        
        return DataCollectorTransactionStatus.TRANSACTION_SUCCESS
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        feh = FirestoreErrorHelper()
        feh.transaction_error_handler(fileName=__filename__, 
                methodName='_set_intersections_manifest', 
                exceptionMsg=e,
                lineNum=exc_tb.tb_lineno)   # type: ignore
        return DataCollectorTransactionStatus.TRANSACTION_ERROR

@firestore.transactional
def _set_intersections_data_doc(
        transaction: firestore.Transaction,
        dataDocRoot: firestore.CollectionReference,
        dataDoc: IntersectionDataDoc):
    try:
        dataDocRef = dataDocRoot.document(intersectionsDataManifestDocName)\
                .collection(intersectionsDataDocSubcolName)\
                .document(dataDoc.get_owner_id())

        transaction.set(dataDocRef, dataDoc.to_dict())

        return DataCollectorTransactionStatus.TRANSACTION_SUCCESS
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        feh = FirestoreErrorHelper()
        feh.transaction_error_handler(fileName=__filename__, 
                methodName='_set_intersections_data_doc', 
                exceptionMsg=e,
                lineNum=exc_tb.tb_lineno)   # type: ignore
        return DataCollectorTransactionStatus.TRANSACTION_ERROR

# if __name__ == '__main__':
#     dc = DataCollectorHelper()
#     # dc.initialise_data_collector()
    
#     # dc.get_intersection_volume()

#     # dc.DEBUG_get_document_time_ranges()
#     result = dc.get_available_ranges()
#     if result != None:
#         print(result.name)

#     print("Value: {}".format(IntersectionTimeRange.LESS_THAN_ONE_HOUR.value))

#     dataGen = dc.get_intersection_data(tsc=110, 
#             timeRange=IntersectionTimeRange.ONE_HOUR)

#     for result in dataGen:
#         result.reverse()
#         for item in result:
#             timestamp = datetime.datetime.fromtimestamp(
#                     item.get_timestamp()/1000.0)
#             print('cmf: {}, timestamp: {}'.format(item.get_cmf(), timestamp))