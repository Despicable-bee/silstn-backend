"""
	FILENAME:		jwk_cron_job_service.py

	AUTHOR(s):		Harry Nowakowski

	DESCRIPTION:	Various helper classes for generating, checking and writing
                    JWTs, JWKs, and Refresh tokens to GCP

	VERSION:		0.0.1

	DESTINATION:    Google Cloud Run

	LICENSE:		MIT
"""

# Standard libs
from collections import UserDict
from datetime import datetime
from enum import Enum
from enum import auto
from typing import Optional

import sys

import uuid
import json
from typing import Dict
from typing import List

# Third party libs
from jwcrypto import jwt, jwk, jwe, jws

from google.cloud import storage

# Local libs

from root.helpers.common_helper.common_firestore_helper import \
        FirestoreTimeHelper
from root.helpers.common_helper.common_firestore_helper import \
        FirestoreErrorHelper

from root.helpers.user_profile.user_profile_firestore_helper import \
        UserProfileFirestoreHelper
from root.helpers.user_profile.user_profile_firestore_helper import \
        UserProfileDoc

# * DEBUG ----------------------------------------------------------------------

__filename__ = 'jwt_helper.py'

# * ENUM -----------------------------------------------------------------------

class JwtRoles(Enum):
    def _generate_next_value_(name, start, count, last_values): # type: ignore
        """ Automatic numbering function. Use this to specify what 
        <Enum_instance>.value will return.
        """
        return count + 1
    USER = auto()

class OldJwtCheckStatus(Enum):
    def _generate_next_value_(name, start, count, last_values): # type: ignore
        """ Automatic numbering function. Use this to specify what 
        <Enum_instance>.value will return.
        """
        return count + 1
    JWT_VALID = auto()
    JWT_INVALID = auto()
    JWT_EXPIRED = auto()
    JWT_ERROR = auto()

# * AUXILIARY HELPERS ----------------------------------------------------------

class JwtClaims(object):
    def __init__(self, audience: List[str], context: Dict, subject: str,
            issuer: str, expiry: int, issued: int, 
            status: OldJwtCheckStatus=None):
        self.__audience = audience
        self.__context = context
        self.__subject = subject
        self.__issuer = issuer
        self.__expiry = expiry
        self.__issued = issued
        self.__status = status
    
    def get_audience(self):
        return self.__audience

    def set_audience(self):
        pass

    def get_status(self):
        return self.__status

    def set_status(self, newStatus: OldJwtCheckStatus):
        """ Sets the status of the JWT """
        self.__status = newStatus

    def get_roles(self):
        return self.__context['roles']

    def get_subject(self):
        """ Returns the subject (userId) of the jwt. """
        return self.__subject

    def get_expiry(self):
        """ Returns the expiry of the jwt (unix epoch in seconds). """
        return self.__expiry

    def get_issued(self):
        """ Return the time the jwt was issued (unix epoch in seconds). """
        return self.__issued

    def to_dict(self):
        """ Returns the contents of the class as a dictionary. """
        return {
            'audience': self.__audience,
            'context': self.__context,
            'subject': self.__subject,
            'issuer': self.__issuer,
            'expiry': self.__expiry,
            'issued': self.__issued
        }

class JwkStorageHelper(object):
    def __init__(self):
        self.storageClient = storage.Client()

        # Private bucket (place where only specific files are publically 
        # viewable)
        self.privateBucketName = 'silstn-testing-bucket'
        
        # Private key (can sign and verify signature)
        self.privateKeyLocation = 'keys/silsn_PRIVATE_jwk.json'
        
        # Public key (can verify the signature)
        self.publicKeyLocation = 'keys/silstn_PUBLIC_jwk.json'
        
        # PUBLIC bucket (user read only)
        self.publicBucketName = 'silstn-public-bucket'

        self.__classname__ = 'JwkStorageHelper'

    def upload_jwks(self, publicJwk: str, privateJwk: str):
        """ Uploads the new public and private keyts to GCS. 
        
        ARGS:
        - publicJwk: The jwk to be saved to 
                'keys/silstn_PUBLIC_jwk.json'
        - privateJwk: The jwk to be saved to 
                'keys/silstn_PRIVATE_jwk.json'

        RETURNS:
        - On success, the method returns True. Otherwise returns False
        """
        try:
            bucket = self.storageClient.bucket(self.privateBucketName)

            # Set the destination address of the private key
            privateBlob = bucket.blob(self.privateKeyLocation)

            # Upload the private key
            privateBlob.upload_from_string(privateJwk)

            # Now do the same for the public jwk
            publicBlob = bucket.blob(self.publicKeyLocation)

            publicBlob.upload_from_string(publicJwk)

            # Set the public key json file to public
            publicBlob.make_public()

            return True
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.class_error_handler(fileName=__filename__,
                    className=self.__classname__, 
                    methodName='upload_jwks', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            return False

    def get_private_jwk(self):
        """ Gets the private jwk from storage. """
        bucket = self.storageClient.bucket(self.privateBucketName)
        blob = bucket.blob(self.privateKeyLocation)
        keyAsJSON = json.loads(blob.download_as_bytes())

        # If you're wondering what **dict does, it unpacks the dict into the
        # function call (see below for more info):
        # https://python-reference.readthedocs.io/en/latest/docs/operators/dict_unpack.html
        return jwk.JWK(**keyAsJSON)

    def check_old_jwt(self, jwtToCheck: str):
        """ Checks whether a given JWT was issued by the key in rotation. 
        
        A JWT's signature can be verified using teh original private key.
        There are a few configurations a JWT can find itself in.

        1. JWT is valid:
            If a JWT is valid, that means that it has NOT expired and can
            readily be used to access the different API across the platform.
        2. JWT is expired:
            If a JWT has expired, this means that the JWT is valid, but has
            passed the expiration period recognised by the server.
        3. JWT is invalid:
            The JWT has an invalid signature and cannot be accepted.

        ARGS:
        - jwtToCheck: The JWT we want to verify

        RETURNS
        - If the JWT is valid, the method will return a JWTClaims object
            with corresponding status.

        EXAMPLES:
        https://github.com/latchset/jwcrypto/issues/14
        """
        # Get the private key
        privateKey = self.get_private_jwk()
        fth = FirestoreTimeHelper()
        try:
            token = jwt.JWT(key=privateKey, jwt=jwtToCheck)

            # Get the claims and add the return condition
            claims = self.__get_jwt_claims(json.loads(token.claims))
            if claims == None:
                raise Exception("Claims don't exist")
            else:
                # Check JWT expired
                if claims.get_expiry() < fth.get_unix_epoch_timestamp_sec():
                    raise jwt.JWTExpired

                claims.set_status(OldJwtCheckStatus.JWT_VALID)
                return claims
        
        except jwt.JWTExpired as jwExp:
            # JWT is valid, but has expired.
            # Get the JWS
            expToken = jwt.JWT(jwt=jwtToCheck).token

            expToken.verify(privateKey) # type: ignore

            claims = self.__get_jwt_claims(json.loads(
                    expToken.payload)) # type: ignore
            
            if claims == None:
                raise Exception("Failed to refresh JWT")
            else:
                claims.set_status(OldJwtCheckStatus.JWT_EXPIRED)
                return claims
        
        except jwt.JWException as jwe:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.class_error_handler(fileName=__filename__,
                    className=self.__classname__, 
                    methodName='check_old_jwt', 
                    exceptionMsg=jwe,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            return OldJwtCheckStatus.JWT_INVALID

        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.class_error_handler(fileName=__filename__,
                    className=self.__classname__, 
                    methodName='check_old_jwt', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            return OldJwtCheckStatus.JWT_ERROR

    def __get_jwt_claims(self, payload: dict):
        """ Extracts the claims out of the payload and returns them. 
        
        ARGS:
        - payload: The dictionary containing the claims

        RETURNS:
        - Upon success, the method will return a JwtClaims object containing the
        relevant claims from the payload. Otherwise will return None.
        """
        try:
            return JwtClaims(
                    audience=payload['aud'],
                    context=payload['context'],
                    subject=payload['sub'],
                    issuer=payload['iss'],
                    expiry=payload['exp'],
                    issued=payload['iat'])
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.class_error_handler(fileName=__filename__,
                    className=self.__classname__, 
                    methodName='__get_jwt_claims', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            return None

class JwtHelper:
    def __init__(self, tokenLifetime: Optional[int]=None):
        """ Generates JWTs and refresh token.
        
        ARGS:
        - tokenLifetime: An optional parameter used for debugging.

        NOTE
        - This method does not perform any writes to firestore.
        """
        if tokenLifetime != None:
            self.tokenLiftime = tokenLifetime
        else:
            self.tokenLiftime = 3600 # 60 sec * 60 sec = 3600 = 1 hour
    
    def generate_jwt(self, 
            roles: List[JwtRoles]=None, 
            audience: List[str]=None, 
            identifier: str=None,
            oldJwtClaims: Dict=None):
        """ Generates a JWT that contains roles and authority of the user. 
        
        In order to distinguish users that have elevated privileges from
        regular users when making API calls, an access token must be generated,
        but also cannot be modified (without us noticing).

        ARGS:
        - roles: A list of roles that indicate the privileges of the user.
        - audience: The audience this JWT is intended for (i.e. users,
                employers, advertisers, admins, etc).
        - identifier: An identifier for the owner of the JWT, used for 
                identifying data ownership.

        RETURNS:
        - On success, the method returns a signed and serialised JWT.
                Otherwise returns None.
        """
        fth = FirestoreTimeHelper()
        jsh = JwkStorageHelper()

        # Get current timestamp in seconds.
        currentTime = fth.get_unix_epoch_timestamp_sec()

        # Get the private key
        privateKey = jsh.get_private_jwk()

        rolesToCheck: List[JwtRoles] = []

        if oldJwtClaims == None:
            assert audience != None, 'Audience must be defined'
            assert identifier != None, 'Identifier must be defined'
            assert roles != None, "Roles must be defined"

            # No old JWT provided, generate a new JWT from scratch.
            setId = identifier
            aud = audience
            rolesToCheck = roles
        else:
            assert isinstance(oldJwtClaims, Dict), \
                    "oldJwtClaims must be a valid dictionary"
            # Old JWT exists, get identifier and audience values
            setId = oldJwtClaims['subject']
            aud = oldJwtClaims['audience']
            for role in oldJwtClaims['context']['roles']:
                rolesToCheck.append(JwtRoles(role))
        
        # Generate the token
        

        rolesList: List[str] = []
        for role in rolesToCheck:
            rolesList.append(role.value)

        token = jwt.JWT(
            header={
                'alg': 'RS256',
                'typ': "JWT",
                'kid': 'leander-mediterranean'      # Must match public
            },
            claims = {
                'iss': 'silstn-issuer',
                'sub': setId,
                'aud': aud,
                'exp': currentTime + self.tokenLiftime,
                'iat': currentTime,
                'context': {
                    # This 'context' section can be filled with whatever you
                    # want
                    "roles": rolesList
                }
            })

        # Sign the token with the private key (generates the signature bit)
        token.make_signed_token(privateKey)

        # Converts the token into a base64 string (...).(...).(...)
        serialToken = token.serialize()

        return serialToken

    def check_refresh_token(self, userId: str, refreshToken: str):
        """ Checks whether the given refresh token matches the one in firestore.

        JWTs expire roughly every hour. When this happens, users don't want to
        be forced to log in again, just to acquire a new JWT.

        We can facilitate the automatic refreshing of the old JWT using a 
        refresh token.

        Refresh tokens are stored in two places, locally on the users computer,
        and on firestore.

        When the jwt needs to be refreshed, the refresh token is handed to the
        backend along with the old jwt.

        If the refresh token handed to the backend matches its firestore 
        counterpart, then the jwt is refreshed.

        ARGS:
        - userId: The id of the user that owns the JWT.
        - refreshToken: The refresh token to check.

        RETURNS:
        - If the provided refresh token matches the one
            in firestore, then the method return True. Otherwise
            returns False.
        
        """
        upfh = UserProfileFirestoreHelper()
        userProfile = upfh.get_user_profile(userId)

        if not isinstance(userProfile, UserProfileDoc):
            pass
        # TODO - This is a relatively minor function, hence transfer a lot of
        # TODO      this text to the README and call it a day.
        raise NotImplementedError

# * TRANSACTIONS ---------------------------------------------------------------
# None