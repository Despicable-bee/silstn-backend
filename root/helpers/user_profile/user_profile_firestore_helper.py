# Standard Libs
from typing import Dict
from typing import Optional
from typing import Union
from typing import List
from typing import Tuple

from enum import Enum
from enum import auto
import sys

# Third Party Libs
from google.cloud import firestore

# Local Libs
from root.helpers.common_helper.common_firestore_helper import \
        FirestoreTimeHelper 
from root.helpers.common_helper.common_firestore_helper import \
        FirestoreErrorHelper
from root.helpers.common_helper.common_firestore_helper import \
        FirestoreIdGenerator
from root.helpers.common_helper.common_firestore_helper import \
        FirestoreTokenHelper

# Base Classes
from root.base_classes.user_profile_doc import BaseDocType

from root.base_classes.user_profile_doc import UserProfileDoc
from root.base_classes.user_profile_doc import UserProfilePassword


# * DEBUG ----------------------------------------------------------------------

__filename__ = 'user_profile_firestore_helper.py'

# * ENUMS ----------------------------------------------------------------------

class UserProfileTransactionStatus(Enum):
    TRANSACTION_SUCCESS = auto()
    TRANSACTION_ERROR = auto()

# * AUXILIARY HELPERS ----------------------------------------------------------

class UserProfileObjectFactory:
    def __init__(self):
        """ Creates objects from firestore dictionaries. 
        
        Firestore is a noSQL database. Hence it uses dictionaries as a means
        to structure data.

        This class merely takes these dictionaries and converts them to the
        corresponding base class.

        This makes it easier to modify the data in a firestore document,
        keep track of where changes are made, and extend documents /
        functionality.
        """
        pass

    def to_obj_v_0_3_0(self, inputDict: Dict) -> Optional[UserProfileDoc]:
        """ Converts the user profile dict to its corresponding object. 
        
        VERSION:
        - This method supports doc_version 0.3.0

        ARGS:
        - inputDict: The dictionary recieved from firestore.

        RETURNS:
        - On success, the method returns a UserProfileDoc. Otherwise returns
            None.
        """
        docVersion = inputDict['doc_version'].split('.')
        
        # Put everything together.
        return UserProfileDoc(
                email=inputDict['email'],
                emailVerified=inputDict['email_verified'],
                dob=inputDict['dob'],
            
                firstname=inputDict['firstname'],
                surname=inputDict['surname'],
                refreshToken=inputDict['refresh_token'],

                docType=BaseDocType(inputDict['doc_type']),
                createTime=inputDict['create_time'],
                lastEditted=inputDict['last_editted'],
                ownerId=inputDict['owner_id'],
                major=int(docVersion[0]),
                minor=int(docVersion[1]),
                patch=int(docVersion[2]),
                saltPasswordHash=inputDict['password'])

class UserProfileFirestoreHelperTransactionSnippets:
    def __init__(self):
        pass

    def get_user_profile_transaction_snippet(self, 
            transaction: firestore.Transaction, 
            userId: str,
            usersDocRoot: firestore.CollectionReference):
        """ TRANSACTION SNIPPET: PROCEED WITH CAUTION. """
        return _get_user_profile_document_transaction_snippet(
                transaction=transaction,
                usersDocRoot=usersDocRoot,
                userId=userId)

# * MAIN HELPER ----------------------------------------------------------------

class UserProfileFirestoreHelper:
    def __init__(self):
        """ Faciliates the reading and writing of data to and from firestore.

        The full pipeline for data flow is as follows:

        >>> Firestore <-> Helper <-> BaseClass <-> API <-> Frontend
        """
        # Reference to the root (default) database
        self.db = firestore.Client()

        # Reference to the transactions object (used for preventing race 
        # conditions)
        self.transaction = self.db.transaction()

        # Reference to the 'users' collection of the firestore database
        # (contains all user information, well no shit sherlock)
        self.usersRef = self.db.collection(u'users') # type: ignore

        # Maximum number of experiences a user can have on their profile
        self.maxExperiences = 20

        # Maximum number of qualifications a user can have on their profile.
        self.maxQualifications = 20
        
        # Versioning info
        self.latestDocVersionMajor = 0
        self.latestDocVersionMinor = 3
        self.latestDocVersionPatch = 0

        # Debugging variables
        self.__classname__ = 'UserProfileFirestoreHelper'
    
    # * PUBLIC METHODS ---------------------------------------------------------

    def create_new_user_profile(self, email: str,
            password: str, firstname: str, surname: str,
            dateOfBirth: str) -> Optional[Union[Tuple[str,str], bool, str]]:
        """ Creates a new user profile document (and thus a new user!). 
        
        This method first verifies whether the information provided by the 
        client doesn't already exist in some other profile on the platform.

        Then creates a new user id.

        Then checks if the provided information is formatted correctly.

        ARGS:
        - email: The email identifying the user
        - password: The plain text password provided by the user.
        - firstname: The users specified firstname
        - surname: The users specified surname
        - dateOfBirth: The users date of birth (DD/MM/YYYY)
        
        RETURNS:
        - On success, the method returns a tuple containing the user id that 
            was generated and the refresh token respectively. 
        - If the user provided some incorrectly formatted inputs, the 
            AssertionError will catch it, and the method will return False.
        - If anything else goes wrong, and an exception gets triggered, the 
            method will return None.

        """
        fth = FirestoreTimeHelper()
        currentTime = fth.get_unix_epoch_timestamp_ms()

        # Verify the user data does not exist already.
        if not self.__check_email_unique(email):
            return False

        # Generate a new user id.
        userId = self.__generate_new_user_id()
        
        if userId == None:
            return False

        # Try to create a user profile document
        fth = FirestoreTokenHelper()
        refreshToken = fth.generate_refresh_token()

        if not isinstance(refreshToken, str):
            raise Exception("Error generating refresh token")

        try:
            userProfileDoc = UserProfileDoc(
                email=email,
                emailVerified=False,
                dob=dateOfBirth,
                firstname=firstname,
                surname=surname,
                refreshToken=refreshToken,
                docType=BaseDocType.USER_PROFILE,
                createTime=currentTime,
                lastEditted=currentTime,
                ownerId=userId,
                major=self.latestDocVersionMajor,
                minor=self.latestDocVersionMinor,
                patch=self.latestDocVersionPatch,
                passwordPlainText=password)

            # User profile has correct information, go ahead and create the
            # document in firestore.
            result = _set_user_profile_document(self.transaction,
                    self.usersRef, userProfileDoc)

            if result == UserProfileTransactionStatus.TRANSACTION_SUCCESS:
                return (userId, refreshToken)
            else:
                return None

        except AssertionError as e: 
            # Some of the profile information was wrong, return the error msg.
            return str(e)

        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.class_error_handler(fileName=__filename__,
                    className=self.__classname__, 
                    methodName='create_new_user_profile', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            return None

    def get_user_profile(self, userId: str):
        """ Returns the users profile document if it exists. 
        
        ARGS:
        - userId: The id of the user who owns the profile.

        RETURNS:
        - On success, the method will return a UserProfileDoc object.
                If the document does not exist, or the input is incorrect, then 
                the method will return False. Otherwise (in the case of an 
                error) will return None.
        """
        try:
            upof = UserProfileObjectFactory()
        
            userDoc = self.usersRef.document(userId).get()  # type: ignore

            if userDoc.exists:
                return upof.to_obj_v_0_3_0(userDoc.to_dict())   # type: ignore
            else:
                return False

        except Exception as e:
            # TODO - error handling
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.class_error_handler(fileName=__filename__,
                    className=self.__classname__, 
                    methodName='get_user_profile', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            return None

    def delete_user_profile(self, userId: str):
        """ Deletes the profile of a specified user id. 
        
        ARGS:
        - userId: The id of the user that owns the profile.

        RETURNS:
        - On success, the method will return True. Otherwise returns False.
        """
        try:
            self.usersRef.document(userId).delete() # type: ignore
            return True
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.class_error_handler(fileName=__filename__,
                    className=self.__classname__, 
                    methodName='delete_user_profile', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            return False
        

    def check_user_login_info(self, email: str, 
            password: str) -> Optional[Union[bool, str]]:
        """ Checks the login info for validity. 

        ARGS:
        - email: The email provided by the user.
        - password: The password provided by the user.

        RETURNS:
        - On success (i.e. the password matches the hash), then the method
                returns the userId. If the password does NOT match the hash,
                then the method returns False. If an error occurs, the method
                returns None.
        """

        queryRef = self.usersRef.where(u'email', u'==', email).limit(1)
        try:
            upof = UserProfileObjectFactory()

            userDoc = queryRef.get()

            if isinstance(userDoc, list):
                userDoc = userDoc[0]

            if userDoc.exists:  # type: ignore
                userProfile = upof.to_obj_v_0_3_0(
                        userDoc.to_dict()) # type: ignore
            
            if isinstance(userProfile, UserProfileDoc): # type: ignore
                result = userProfile.check_hashed_password_matches(password)
                if result:
                    # The password matches the hash, return the user Id.
                    return userProfile.get_owner_id()
                else:
                    # The password doesn't match, return False.
                    return False      
            else:
                raise Exception("Error trying to find user profile")
        except Exception as e:
            # TODO - Error handling
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.class_error_handler(fileName=__filename__, 
                    className=self.__classname__,
                    methodName='check_user_login_info', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            return None

    def update_user_profile(self, newUserProfile: UserProfileDoc) -> bool:
        """ Replaces an existing user profile with a new one. 
        
        ARGS:
        - newUserProfile: the user profile with updated data, which will
                overwrite the existing document.

        RETURNS:
        - On success, the method returns True. Otherwise returns False.
        """
        result = _update_user_profile_document(self.transaction, self.usersRef, 
                newUserProfile)
        
        if result == UserProfileTransactionStatus.TRANSACTION_SUCCESS:
            return True
        else:
            return False

    def __generate_new_user_id(self):
        """ Generates a unique user id. """
        counter = 0
        try:
            fig = FirestoreIdGenerator()
            while True:
                userId = fig.generate_user_id()

                # Check if id exists.
                queryRef = self.usersRef.where('user_id', u'==', userId)\
                        .limit(1)
                doc = queryRef.get()
                if(len(doc) == 0):  # type: ignore
                    # No documents returned, therefore userid unique
                    return userId
                else:
                    counter += 1

                # If we exceed the max number of retries, throw an error
                if counter >= 100:
                    raise Exception("GeneratorRetriesExhaustedError")
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.class_error_handler(fileName=__filename__,
                    className=self.__classname__, 
                    methodName='__generate_new_user_id', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            return None

    # * PRIVATE METHODS --------------------------------------------------------

    def __check_email_unique(self, email: str):
        """ Checks if the email provided already exists in firestore. 
        
        ARGS:
        - email: The email address we want to check.

        RETURNS:
        - On success, the method returns True. Otherwise returns False.
        """
        emailQueryRef = self.usersRef.where('email', '==', email).limit(1)
        emailDoc = emailQueryRef.get()

        if len(emailDoc) > 0:   # type: ignore
            # Document that contains email already exists
            return False
        else:
            # email not found in any document (so it's unique).
            return True


# * TRANSACTION SNIPPETS -------------------------------------------------------

def _get_user_profile_document_transaction_snippet(
        transaction: firestore.Transaction, 
        usersDocRoot: firestore.CollectionReference,
        userId: str):
    """ TRANSACTION SNIPPET: PROCEED WITH CAUTION 
    
    CONTAINS READS !
    """
    upof = UserProfileObjectFactory()
    try:
        userProfileDocRef = usersDocRoot.document(userId)
        result = userProfileDocRef.get(transaction=transaction)  # type: ignore

        if result.exists:   # type: ignore
            return upof.to_obj_v_0_3_0(result.to_dict())   # type: ignore
        else:
            return False
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        feh = FirestoreErrorHelper()
        feh.transaction_error_handler(fileName=__filename__, 
                methodName='_set_user_profile_document', 
                exceptionMsg=e,
                lineNum=exc_tb.tb_lineno)   # type: ignore
        return None

# * TRANSACTIONS ---------------------------------------------------------------

@firestore.transactional
def _update_user_profile_document(
        transaction: firestore.Transaction,
        usersDocRoot: firestore.CollectionReference,
        userProfileToUpdate: UserProfileDoc):
    """ Updates a user profile document in firestore. """
    try:
        # Create a reference to the new document
        # i.e. root > users > usr12345678
        userDocRef = usersDocRoot.document(userProfileToUpdate.get_owner_id())

        userProfileDict = userProfileToUpdate.to_dict()

        # Check if anything went wrong
        # NOTE - This method cannot fail

        # update the document
        transaction.update(userDocRef, userProfileDict)

        return UserProfileTransactionStatus.TRANSACTION_SUCCESS
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        feh = FirestoreErrorHelper()
        feh.transaction_error_handler(fileName=__filename__, 
                methodName='_update_user_profile_document', 
                exceptionMsg=e,
                lineNum=exc_tb.tb_lineno)   # type: ignore
        return UserProfileTransactionStatus.TRANSACTION_ERROR

@firestore.transactional
def _set_user_profile_document(
        transaction: firestore.Transaction,
        usersDocRoot: firestore.CollectionReference,
        userProfileToWrite: UserProfileDoc):
    """ Sets a user profile document in firestore. """
    try:
        # Create a reference to the new document
        # i.e. root > users > usr12345678
        userDocRef = usersDocRoot.document(userProfileToWrite.get_owner_id())

        userProfileDict = userProfileToWrite.to_dict()

        # Check if anything went wrong
        # NOTE - This method cannot fail

        # Set the document
        transaction.set(userDocRef, userProfileDict)

        return UserProfileTransactionStatus.TRANSACTION_SUCCESS
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        feh = FirestoreErrorHelper()
        feh.transaction_error_handler(fileName=__filename__, 
                methodName='_set_user_profile_document', 
                exceptionMsg=e,
                lineNum=exc_tb.tb_lineno)   # type: ignore
        return UserProfileTransactionStatus.TRANSACTION_ERROR