# Standard Libs
from operator import indexOf
from typing import Dict
from typing import List
from math import sqrt
from math import atan2
from math import pi
from math import asin

# Third Party Libs

import numpy as np


# Local Libs
from root.helpers.common_helper.common_firestore_helper import \
        FirestoreIdGenerator

# Base Classes

# * DEBUG ----------------------------------------------------------------------

# * ENUM -----------------------------------------------------------------------

# * AUXILIARY HELPERS ----------------------------------------------------------

class Vertex(object):
    def __init__(self, x: float, y: float, z: float, vertId: str, 
            multiplier: float):
        # Preconditions 
        assert isinstance(float(x), float), "X must be a real number"
        assert isinstance(float(y), float), "Y must be a real number"
        assert isinstance(float(z), float), "Z must be a real number"

        self.__x = multiplier * x
        self.__y = multiplier * y
        self.__z = multiplier * z
        self.__vertId = vertId

    def get_vert_id(self):
        return self.__vertId

    def set_vert_id(self, newVertId: str):
        self.__vertId = newVertId

    def get_x(self):
        return self.__x

    def set_x(self, newX: float):
        assert isinstance(float(newX), float), "newX must be a real number"
        self.__x = newX

    def get_y(self):
        return self.__y

    def set_y(self, newY: float):
        assert isinstance(float(newY), float), "newY must be a real number"
        self.__y = newY

    def get_z(self):
        return self.__z

    def set_z(self, newZ: float):
        assert isinstance(float(newZ), float), "newZ must be a real number"
        self.__z = newZ

    def to_cartographic_coordinates(self):
        """ Converts the set of cartesian coordinates to cartographic coords.  
        
        Latitude and longitude baby!

        RETURNS:
        - A tuple containing the latitude and longitude coordinates
        """
        radius = sqrt(self.__x**2 + self.__y**2 + self.__z**2).real

        # We're assuming you're standing on the surface of the earth, so 
        # altitude is zero
        h = 0

        longitude = atan2(self.__y, self.__x) * (180/pi)
        latitude = asin(self.__z / radius) * (180/pi)
        
        return (latitude, longitude)

    def to_numpy_array(self):
        """ Returns the contents of the class as a numpy array. """
        return np.array((self.__x, self.__y, self.__z))

class Triangle(object):
    def __init__(self, vert1: Vertex, vert2: Vertex, vert3: Vertex):
        self.__vert1 = vert1
        self.__vert2 = vert2
        self.__vert3 = vert3

    def get_vert1(self):
        return self.__vert1

    def get_vert2(self):
        return self.__vert2

    def get_vert3(self):
        return self.__vert3

    def project_onto_sphere(self):
        """ Projects the given vertices onto a sphere of radius 1. """
        vertsList = [self.__vert1, self.__vert2, self.__vert3]
        for vert in vertsList:
            absP = sqrt(vert.get_x()**2 + vert.get_y()**2 + vert.get_z()**2).real
            Q = 1 / absP

            vert.set_x(Q * vert.get_x())
            vert.set_y(Q * vert.get_y())
            vert.set_z(Q * vert.get_z())

    def get_cartographic_coords(self):
        """ Returns the vertices of the triangles as cartographic coords. """
        return (self.__vert1.to_cartographic_coordinates(),
                self.__vert2.to_cartographic_coordinates(),
                self.__vert3.to_cartographic_coordinates())

    def tessellate(self, newVertId1: str, newVertId2: str, newVertId3: str):
        """ Given the 3 vertices of the given triangle, returns 4 new triangles. 
        
        """
        newVert1 = Vertex(x=(self.__vert1.get_x() + self.__vert2.get_x())/2,
                y=(self.__vert1.get_y() + self.__vert2.get_y())/2,
                z=(self.__vert1.get_z() + self.__vert2.get_z())/2,
                vertId=newVertId1,
                multiplier=1)

        newVert2 = Vertex(x=(self.__vert1.get_x() + self.__vert3.get_x())/2,
                y=(self.__vert1.get_y() + self.__vert3.get_y())/2,
                z=(self.__vert1.get_z() + self.__vert3.get_z())/2,
                vertId=newVertId2,
                multiplier=1)

        newVert3 = Vertex(x=(self.__vert3.get_x() + self.__vert2.get_x())/2,
                y=(self.__vert3.get_y() + self.__vert2.get_y())/2,
                z=(self.__vert3.get_z() + self.__vert2.get_z())/2,
                vertId=newVertId3,
                multiplier=1)

        newTriangle1 = Triangle(vert1=self.__vert1, 
                vert2=newVert1, 
                vert3=newVert2)

        newTriangle2 = Triangle(vert1=newVert1, 
                vert2=self.__vert2, 
                vert3=newVert3)
        
        newTriangle3 = Triangle(vert1=newVert2, 
                vert2=newVert3, 
                vert3=self.__vert3)

        newTriangle4 = Triangle(vert1=newVert1,
                vert2=newVert2,
                vert3=newVert3)

        return (newTriangle1, newTriangle2, newTriangle3, newTriangle4)

# * MAIN HELPER ----------------------------------------------------------------

# * TRANSACTIONS ---------------------------------------------------------------

class Icosahedron(object):
    def __init__(self):
        """ Initialises a standard icosahedron. """
        self.__vertex_ids_dict: Dict[str, bool] = {}

        self.__trianglesList: List[Triangle] = []

        # Generate the base icosahedron
        self.__generate_base_icosahedron()

    # * PUBLIC METHODS ---------------------------------------------------------

    def to_cartographic_coords(self):
        fileOpen = open('dump.txt', 'w')
        for triangle in self.__trianglesList:
            coords = triangle.get_cartographic_coords()
            fileOpen.write("[\n")
            for coord in coords:
                fileOpen.write("\t{{ lat: {}, lng: {} }},\n".format(coord[0], coord[1]))
            fileOpen.write("],\n")
        fileOpen.close()

    def project_triangles_onto_sphere(self):
        # get all the vertices
        vertsList: List[Vertex] = []
        for triangle in self.__trianglesList:
            if triangle.get_vert1() not in vertsList:
                vertsList.append(triangle.get_vert1())
            
            if triangle.get_vert2() not in vertsList:
                vertsList.append(triangle.get_vert2())
            
            if triangle.get_vert3() not in vertsList:
                vertsList.append(triangle.get_vert2())
        
        # Project each vertex onto the sphere
        for vert in vertsList:
            absP = sqrt(vert.get_x()**2 + vert.get_y()**2 + vert.get_z()**2).real
            Q = 1 / absP

            vert.set_x(Q * vert.get_x())
            vert.set_y(Q * vert.get_y())
            vert.set_z(Q * vert.get_z())

    def get_triangles(self):
        return self.__trianglesList

    def get_unique_vertex_id(self):
        fig = FirestoreIdGenerator()
        counter = 0
        while True:
            newVertId = fig.generate_vertex_id()
            if newVertId not in self.__vertex_ids_dict:
                self.__vertex_ids_dict[newVertId] = True
                return newVertId
            
            counter += 1
            
            if counter > 100:
                raise Exception("Id Generation exhausted error.")

    def tessellate_triangles(self):
        newTrianglesList: List[Triangle] = []
        for triangle in self.__trianglesList:
            newTriangles = triangle.tessellate(newVertId1="", 
                    newVertId2="", 
                    newVertId3="")
            for t in newTriangles:
                newTrianglesList.append(t)
        
        self.__trianglesList = newTrianglesList

    def visualise_icosahedron(self):
        """ Renders the icosahedron using mayavi. """
        x = []
        y = []
        z = []

        # Get all the vertices
        vertsList: List[Vertex] = []
        for triangle in self.__trianglesList:
            if triangle.get_vert1() not in vertsList:
                vertsList.append(triangle.get_vert1())
            
            if triangle.get_vert2() not in vertsList:
                vertsList.append(triangle.get_vert2())
            
            if triangle.get_vert3() not in vertsList:
                vertsList.append(triangle.get_vert2())
        
        # For each triangle, get the index of where their vertex exists
        triangleTuplesList: List[tuple] = []
        for triangle in self.__trianglesList:
            triangleTuplesList.append(
                (vertsList.index(triangle.get_vert1()),
                 vertsList.index(triangle.get_vert2()),
                 vertsList.index(triangle.get_vert3()))
                )

        # Convert the list of vertices into tuples
        for vert in vertsList:
            x.append(vert.get_x())
            y.append(vert.get_y())
            z.append(vert.get_z())
        
        x = np.array([x]).T
        y = np.array([y]).T
        z = np.array([z]).T

        # mlab.triangular_mesh(x , y , z, triangleTuplesList, color=(1,0,0), 
        #         representation='wireframe')
        # mlab.axes()
        # mlab.show()
    
    # * PRIVATE METHODS --------------------------------------------------------

    def __generate_base_icosahedron(self):
        """ Generates a base icosahedron. 
        
        The base icosahedron contains 20 faces and 11 vertices
        """
        t = ((1 + sqrt(5)) / 2).real

        multiplier = 1 / t

        # Generate 11 unique vertex ids
        counter = 0
        vertexIds: List[str] = []
        while True:
            vertexIds.append(self.get_unique_vertex_id())
            counter += 1
            if counter == 12:
                break

        v0 = Vertex(x=t, y=1, z=0, vertId=vertexIds[0], multiplier=multiplier)
        v1 = Vertex(x=-t, y=1, z=0, vertId=vertexIds[1], multiplier=multiplier)
        v2 = Vertex(x=t, y=-1, z=0, vertId=vertexIds[2], multiplier=multiplier)
        v3 = Vertex(x=-t, y=-1, z=0, vertId=vertexIds[3], multiplier=multiplier)
        v4 = Vertex(x=1, y=0, z=t, vertId=vertexIds[4], multiplier=multiplier)
        v5 = Vertex(x=1, y=0, z=-t, vertId=vertexIds[5], multiplier=multiplier)
        v6 = Vertex(x=-1, y=0, z=t, vertId=vertexIds[6], multiplier=multiplier)
        v7 = Vertex(x=-1, y=0, z=-t, vertId=vertexIds[7], multiplier=multiplier)
        v8 = Vertex(x=0, y=t, z=1, vertId=vertexIds[8], multiplier=multiplier)
        v9 = Vertex(x=0, y=-t, z=1, vertId=vertexIds[9], multiplier=multiplier)
        v10 = Vertex(x=0, y=t, z=-1, vertId=vertexIds[10], multiplier=multiplier)
        v11 = Vertex(x=0, y=-t, z=-1, vertId=vertexIds[11], multiplier=multiplier)

        # T0
        self.__trianglesList.append(Triangle(vert1=v0, vert2=v8, vert3=v4))
        self.__trianglesList.append(Triangle(vert1=v0, vert2=v5, vert3=v10))
        # T2
        self.__trianglesList.append(Triangle(vert1=v2, vert2=v4, vert3=v9))
        self.__trianglesList.append(Triangle(vert1=v2, vert2=v11, vert3=v5))
        # T4
        self.__trianglesList.append(Triangle(vert1=v1, vert2=v6, vert3=v8))
        self.__trianglesList.append(Triangle(vert1=v1, vert2=v10, vert3=v7))
        # T6
        self.__trianglesList.append(Triangle(vert1=v3, vert2=v9, vert3=v6))
        self.__trianglesList.append(Triangle(vert1=v3, vert2=v7, vert3=v11))
        # T8
        self.__trianglesList.append(Triangle(vert1=v0, vert2=v10, vert3=v8))
        self.__trianglesList.append(Triangle(vert1=v1, vert2=v8, vert3=v10))
        # T10
        self.__trianglesList.append(Triangle(vert1=v2, vert2=v9, vert3=v11))
        self.__trianglesList.append(Triangle(vert1=v3, vert2=v9, vert3=v11))
        # T12
        self.__trianglesList.append(Triangle(vert1=v4, vert2=v2, vert3=v0))
        self.__trianglesList.append(Triangle(vert1=v5, vert2=v0, vert3=v2))
        # T14
        self.__trianglesList.append(Triangle(vert1=v6, vert2=v1, vert3=v3))
        self.__trianglesList.append(Triangle(vert1=v7, vert2=v3, vert3=v1))
        # T16
        self.__trianglesList.append(Triangle(vert1=v8, vert2=v6, vert3=v4))
        self.__trianglesList.append(Triangle(vert1=v9, vert2=v4, vert3=v6))
        # T18
        self.__trianglesList.append(Triangle(vert1=v10, vert2=v5, vert3=v7))
        self.__trianglesList.append(Triangle(vert1=v11, vert2=v7, vert3=v5))

if __name__ == '__main__':
    ico = Icosahedron()
    ico.tessellate_triangles()
    ico.tessellate_triangles()
    # FAR <--
    ico.tessellate_triangles()
    ico.tessellate_triangles()
    # STANDARD <--
    ico.tessellate_triangles()
    ico.tessellate_triangles()
    # NEAR < --
    ico.project_triangles_onto_sphere()
    ico.to_cartographic_coords()
    # ico.visualise_icosahedron()
