# Standard libs
from typing import Dict
import requests
import tempfile
import os
from datetime import datetime
import json
import sys
from typing import Optional
import logging

from typing import List
from typing import Tuple

# Third party libs
import numpy as np

# Local libs

from root.helpers.data_collector.data_collector_firestore_helper import \
        DataCollectorHelper
from root.helpers.data_collector.data_collector_firestore_helper import \
        IntersectionTimeRange

from root.helpers.common_helper.common_firestore_helper import \
        FirestoreErrorHelper

from root.helpers.chunk_bounds_checker.intersection_doc_getters import \
        IntersectionDocGetter

# Base classes
from root.base_classes.intersections_doc import IntersectionsDocManifest
from root.base_classes.intersec_data_doc import IntersectionTscDataPair

# * DEBUG ----------------------------------------------------------------------

__filename__ = 'prediction_api_helper.py'

# * ENUMS ----------------------------------------------------------------------

# * AUXILIARY HELPERS ----------------------------------------------------------

# * MAIN HELPER ----------------------------------------------------------------

class PredictionApiHelper:
    def __init__(self):
        ipAddress = '34.69.211.221'
        self.__forecastUrl = 'http://{}:9501/app/forecast'.format(ipAddress)
        self.__initTrainUrl = 'http://{}:9501/app/init_train'.format(ipAddress)
        
        self.__timestepInMs = 300000

        self.__totalCols = 119

        self.__trainingTime = IntersectionTimeRange.ONE_FORTNIGHT

    def request_forecast_2(self, 
            tsc: int) -> Optional[Tuple[np.ndarray, List[int]]]:
        """ Sends a request to the server to forecast the next hour.

        ARGS:
        - tsc: The tsc we want to get data from:
        """
        dch = DataCollectorHelper()
        idg = IntersectionDocGetter()

        try:
            # Get the intersections manifest doc
            intersecManifestDoc = idg.get_intersections_manifest_doc()

            if not isinstance(intersecManifestDoc, IntersectionsDocManifest):
                raise Exception(
                        "Error trying to get intersections manifest doc")

            # Check if the requested tsc is allowed
            if tsc not in intersecManifestDoc.get_prediction_tscs_as_list():
                raise Exception("Specified tsc not available for forecast")
            
            maxTimestamp = 0

            # Get the data
            allIntersectionTscsData: Dict[int, 
                    List[IntersectionTscDataPair]] = {}
            
            allowedTscs: List[int] = \
                    intersecManifestDoc.get_prediction_tscs_as_list()

            tscIndex = allowedTscs.index(tsc)

            # Get the data
            for result in dch.get_intersections_data(
                    allowedTscs=allowedTscs, # type: ignore 
                    timeRange=IntersectionTimeRange.ONE_HOUR):

                intersecDataList: List[IntersectionTscDataPair] = []
                
                for r in result[0]:
                    listToProcess = result[0][r]

                    # Reverse to put latest results at the top of the list
                    listToProcess.reverse()

                    for item in listToProcess:
                        timestamp = item.get_timestamp()
                
                        if timestamp > maxTimestamp or maxTimestamp == 0:
                            maxTimestamp = timestamp
                        
                        intersecDataList.append(item)
                    
                    if r not in allIntersectionTscsData:
                        allIntersectionTscsData[r] = listToProcess
                    else:
                        allIntersectionTscsData[r] += listToProcess

            timespan = dch.get_5_min_intervals(\
                    IntersectionTimeRange.ONE_HOUR)
            
            interpolatedResultsMatrix = \
                        self.__get_matrix_of_traffic_data(
                    allowedTscs=allowedTscs,
                    timespan=timespan,
                    allIntersectionTscsData=allIntersectionTscsData,
                    maxTimestamp=maxTimestamp)

            if not isinstance(interpolatedResultsMatrix, np.ndarray):
                raise Exception("Error trying to get matrix of traffic data")

            tmp = tempfile.NamedTemporaryFile(delete=False)
            fileToWrite = open(tmp.name, "w")
            for line in interpolatedResultsMatrix:
                # Convert a line to a string
                fileToWrite.write(np.array2string(line, separator=",")\
                        .replace('\n',"")\
                        .replace("[","")\
                        .replace("]","")\
                        .replace(" ","") + '\n')

            fileToWrite.close()


            # Open the file(s)
            bytesFile = open(tmp.name, "rb")
            
            # load the traffic file into a dictionary
            files = {
                'traffic_file': bytesFile
            }

            r = requests.post(self.__forecastUrl, files=files)
            JSONData = r.json()

            matrix = np.array(JSONData['data']['data'])

            outputVector = matrix[:, tscIndex]

            # Get the latest time
            latestTimestamp = allIntersectionTscsData[tsc][0]\
                    .get_timestamp()

            timestampList = []
            for i in range(0, len(outputVector)):
                timestampList.append(latestTimestamp + \
                        (1 + i) * self.__timestepInMs)

            return (outputVector, timestampList)

        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.api_error_handler(fileName=__filename__,
                    methodName='request_forecast_2', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            return None

    def request_training(self):
        """ Sends a request to prediction server to begin / check on training. 
        """
        
        dch = DataCollectorHelper()
        idg = IntersectionDocGetter()
        try:
            # Get the intersections manifest doc
            intersecManifestDoc = idg.get_intersections_manifest_doc()

            if not isinstance(intersecManifestDoc, IntersectionsDocManifest):
                raise Exception("Error trying to get intersections manifest doc")
            
            maxTimestamp = 0

            # Get the data
            allIntersectionTscsData: Dict[int, 
                    List[IntersectionTscDataPair]] = {}
            
            allowedTscs: List[int] = \
                    intersecManifestDoc.get_prediction_tscs_as_list()

            for result in dch.get_intersections_data(
                    allowedTscs=allowedTscs, # type: ignore 
                    timeRange=self.__trainingTime):

                intersecDataList: List[IntersectionTscDataPair] = []
                
                for r in result[0]:

                    listToProcess = result[0][r]

                    # Reverse to put latest results at the top of the list
                    listToProcess.reverse()
                    for item in listToProcess:
                        timestamp = item.get_timestamp()

                        if timestamp > maxTimestamp or maxTimestamp == 0:
                            maxTimestamp = timestamp
                        
                        intersecDataList.append(item)
                    
                    if r not in allIntersectionTscsData:
                        allIntersectionTscsData[r] = listToProcess
                    else:
                        allIntersectionTscsData[r] += listToProcess

            timespan = dch.get_5_min_intervals(self.__trainingTime)

            interpolatedResultsMatrix = \
                        self.__get_matrix_of_traffic_data(
                    allowedTscs=allowedTscs,
                    timespan=timespan,
                    allIntersectionTscsData=allIntersectionTscsData,
                    maxTimestamp=maxTimestamp)

            if not isinstance(interpolatedResultsMatrix, np.ndarray):
                raise Exception("Error trying to get matrix of traffic data")


            tmp = tempfile.NamedTemporaryFile(delete=False)
            fileToWrite = open(tmp.name, "w")
            for line in interpolatedResultsMatrix:
                # Convert a line to a string
                fileToWrite.write(np.array2string(line, separator=",")\
                        .replace('\n',"")\
                        .replace("[","")\
                        .replace("]","")\
                        .replace(" ","") + '\n')

            fileToWrite.close()

            # Open the file(s)
            bytesFile = open(tmp.name, "rb")
            adjFile = open("city_w_adj.npy", "rb")
            
            # load the traffic file into a dictionary
            files = {
                'traffic_file': bytesFile,
                'tsc_file': adjFile
            }

            r = requests.post(self.__initTrainUrl, files=files)
            # JSONData = r.json()
            # logging.info("Response from server:")
            
            # logging.info("Code: {}".format(JSONData['code']))
            # logging.info('Msg: {}'.format(JSONData['msg']))
            # logging.info("Data: {}".format(JSONData['data']))
            
            

        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.api_error_handler(fileName=__filename__,
                    methodName='request_training', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            return None

    def test_training_method(self):
        bytesFile = open('city_traffic_signal_simulate_train.csv', "rb")
        adjFile = open("city_w_adj.npy", "rb")
        
        # load the traffic file into a dictionary
        files = {
            'traffic_file': bytesFile,
            'tsc_file': adjFile
        }

        r = requests.post(self.__initTrainUrl, files=files)
        JSONData = r.json()
        print("Response from server:")
        
        print("Code: {}".format(JSONData['code']))
        print('Msg: {}'.format(JSONData['msg']))
        print("Data: {}".format(JSONData['data']))

    # ? PRIVATE METHODS --------------------------------------------------------

    def __get_matrix_of_traffic_data(self, allowedTscs: List[int], 
            timespan: int, 
            allIntersectionTscsData: Dict[int, List[IntersectionTscDataPair]],
            maxTimestamp: int):
        """ Generates a matrix of values based on the available values.

        If there are missing values, the method will perform some basic 
            interpolation in order to fill in the gaps.
        
        ARGS:
        - allowedTscs: A list of the allowed tscs
        - timespan: The number of 5 minute timesteps you want
        - allIntersectionTscsData: Dictionary containing the lists of data 
            points collected by the server (index 0 = most recent timestamp)
        - maxTimestamp: The latest timestamp in the allIntersectionsTscsData 
            dict.
        
        RETURNS:
        - On success, method returns a numpy matrix of all the tsc values
            in order.
        """
        try:
            interpolatedResultsMatrix = np.zeros((timespan, self.__totalCols), 
                    dtype=int)
            for tsc in allowedTscs:
                timestepCounter = 0
                currentIndex = allowedTscs.index(tsc)
                interpolatedTscList = []
                
                # If the particular route is missing, just skip it
                # (all values will be zero)
                try:
                    currentTsc = allIntersectionTscsData[tsc]
                except KeyError as e:
                    print(e)
                    continue
                
                
                # Get the latest timestamp available from this particular tsc
                latestTimestamp = currentTsc[0].get_timestamp()

                deltaLeadingEntries = (maxTimestamp - latestTimestamp) / \
                        self.__timestepInMs

                if deltaLeadingEntries > 1:
                    # Hold interpolation
                    latestCmf = currentTsc[0]
                    for _ in range(0,int(deltaLeadingEntries)):
                        interpolatedTscList.append(latestCmf)
                        timestepCounter += 1
                else:
                    interpolatedTscList.append(currentTsc[0])
                
                previousIntersec = currentTsc[0]
                # Inbetween interpolation
                for i in range(1, len(currentTsc)):
                    deltaTime = abs(currentTsc[i].get_timestamp() - \
                            previousIntersec.get_timestamp())
                    
                    if deltaTime != self.__timestepInMs:
                        # Interpolating...
                        numSteps = int(deltaTime/self.__timestepInMs)
                        
                        deltaCmf = currentTsc[i].get_cmf() - \
                                previousIntersec.get_cmf()
                        
                        for j in range(1, numSteps):
                            newTimestamp = previousIntersec.get_timestamp() + \
                                    self.__timestepInMs * j
                            newCmf = int(previousIntersec.get_cmf() + \
                                    deltaCmf/numSteps * j)
                            interpolatedTscList.append(
                                IntersectionTscDataPair(
                                    timestamp=newTimestamp, 
                                    cmf=newCmf))
                    # Add the current intersecDataTscPair to the list
                    interpolatedTscList.append(currentTsc[i])
                    previousIntersec = currentTsc[i]

                # Hold result if there still aren't 12 items in the array
                if len(interpolatedTscList) != timespan:
                    for i in range(0, timespan - len(interpolatedTscList)):
                        interpolatedTscList.append(interpolatedTscList[-1])

                # Cut off any addition samples we don't need
                interpolatedTscList = interpolatedTscList[:timespan]

                # Extract the cmf data only and put it the appropriate column
                cmfDataList: List[int] = []
                for intersec in interpolatedTscList:
                    cmfDataList.append(intersec.get_cmf())

                # Replace the specfic column with the newly interpolated data
                transposedCmf = np.array(cmfDataList).reshape(-1,1)

                interpolatedResultsMatrix[:,currentIndex] = transposedCmf[:,0]

            return interpolatedResultsMatrix
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.api_error_handler(fileName=__filename__,
                    methodName='__get_matrix_of_traffic_data', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            return None
        

    def __matrix_formatter(self, columnVector: np.ndarray, tscIndex: int, 
            tscsList: List[int]):
        """ Formats a column vector as an ndarray """
        if tscIndex == 0:
            print("Append to start")
            # Append to the start
            zerosArr = np.zeros((12,len(tscsList) - 1))
            print(zerosArr.shape)
            return np.append(columnVector, zerosArr, axis=1)
        elif tscIndex == len(tscsList) - 1:
            # Append to the end
            print("Append to end")
            zerosArr = np.zeros((12,len(tscsList) - 1))
            return np.append(zerosArr, columnVector, axis=1)
        else:
            # Normal
            print("Normal append")
            zerosArr1 = np.zeros((12, tscIndex))
            zerosArr2 = np.zeros((12, len(tscsList) - tscIndex - 1))
            return np.append(np.append(zerosArr1, columnVector, axis=1), 
                    zerosArr2, axis=1)

if __name__ == '__main__':
    pah = PredictionApiHelper()
    # pah.test_training_method()
    pah.request_forecast_2(365)
    #pah.request_training()