# Standard Libs
import requests
import pprint
import json
from typing import List
from typing import Dict
from typing import Tuple
import sys
from alive_progress import alive_bar
import time
import logging

from enum import Enum
from enum import auto

# Third Party Libs
from google.cloud import firestore
from root.base_classes.base_doc import BaseDocType


# Local Libs
from root.helpers.icosahedron_helper.icosahedron_helper import Icosahedron, Triangle
from root.helpers.common_helper.common_firestore_helper import \
        FirestoreDocSizeHelper
from root.helpers.common_helper.common_firestore_helper import \
        FirestoreIdGenerator
from root.helpers.common_helper.common_firestore_helper import \
        FirestoreTimeHelper
from root.helpers.common_helper.common_firestore_helper import \
        FirestoreErrorHelper

# Base Classes
from root.base_classes.common_classes import Coordinates

from root.base_classes.chunk_doc import Chunk, ChunkManifestBoundsType, ChunkMode, LatLngVertex
from root.base_classes.chunk_doc import ChunkBoundingBox
from root.base_classes.chunk_doc import ChunkDoc
from root.base_classes.chunk_doc import ChunkManifestDoc
from root.base_classes.chunk_doc import ChunkManifestIndex
from root.base_classes.chunk_doc import ChunkType
from root.base_classes.chunk_doc import DocManifest

from root.base_classes.intersections_doc import Arm, IntersectionsDocManifest
from root.base_classes.intersections_doc import Intersection
from root.base_classes.intersections_doc import IntersectionsDoc
from root.base_classes.intersections_doc import Route
from root.base_classes.intersections_doc import Step


# * DEBUG ----------------------------------------------------------------------

__filename__ = 'google_maps_traffic_helper.py'

# * ENUM -----------------------------------------------------------------------

class GoogleMapsTrafficHelperTransactionStatus(Enum):
    TRANSACTION_SUCCESS = auto()
    TRANSACTION_ERROR = auto()

class ViewportBoundsConfig(Enum):
    RHS = auto()
    LHS = auto()
    BOTH = auto()
    SPLIT = auto()

# * AUXILIARY HELPERS ----------------------------------------------------------

class BoundingBoxHelper(object):
    def __init__(self):
        pass
    
    def check_viewport_split(self, viewportBounds: ChunkBoundingBox):
        """ Determines if the bounds of the viewport are split. 
        
        ARGS:
        - viewportBounds: The bounding box defined by the viewport.

        RETURNS:
        - If the viewport is indeed split, then the methopd will return True.
            Otherwise will return False.
        """
        
        v1 = viewportBounds.get_top_right_vert()
        v2 = viewportBounds.get_bottom_left_vert()
        if v1.get_lng() < v2.get_lng():
            return True
        else:
            return False

    def check_viewport_bounds_config(self, viewportBounds: ChunkBoundingBox):
        """ Determines the configuration of the viewport bounds. 
        
        The viewport can be one of 4 configurations:
        1. `LHS`: The entirety of the viewport bounds is in the LHS quadrant.
        2. `RHS`: The entirety of the viewport bounds is in the RHS quadrant.
        3. `Both`: The viewport bounds cross from -ve to +ve, but does not 
            cross the split line.
        4. `Split`: The viewport bounds cross the split line.
        """
        v1 = viewportBounds.get_top_right_vert()
        v2 = viewportBounds.get_bottom_left_vert()

        if v1.get_lng() < v2.get_lng():
            # Viewport is split
            return ViewportBoundsConfig.SPLIT
        else:
            # Viewport is not split
            if v1.get_lng() < 0 and v2.get_lng() < 0:
                return ViewportBoundsConfig.RHS
            elif v1.get_lng() >= 0 and v2.get_lng() >= 0:
                return ViewportBoundsConfig.LHS
            else:
                return ViewportBoundsConfig.BOTH

    def check_bounding_box_in_view(self, viewportBounds: ChunkBoundingBox,
            chunkBoundingBox: ChunkBoundingBox):
        """ Determines if the chunk is within the bounds of the viewport. 
        
        NOTES:
        - Optimise this please :)

        ARGS:
        - viewportBounds: Bounding box representing the viewport from the user.

        RETURNS:
        - If either of the viewport verticies are within the bounding box, then
            the method will return `True`. Otherwise will return `False`
        """
        v1 = chunkBoundingBox.get_top_right_vert()
        v2 = chunkBoundingBox.get_bottom_left_vert()
        p1 = viewportBounds.get_top_right_vert()
        p2 = viewportBounds.get_bottom_left_vert()

        v1lat = v1.get_lat()
        v1lng = v1.get_lng()
        v2lat = v2.get_lat()
        v2lng = v2.get_lng()

        p1lat = p1.get_lat()
        p1lng = p1.get_lng()
        p2lat = p2.get_lat()
        p2lng = p2.get_lng()

        # Check the outside chunk cases
        if (v1lng < p2lng) or (p1lat < v2lat) or (p1lng < v2lng) or (v1lat < p2lat):
            # logging.info("Chunk outside")
            return False

        # If it's not outside, then it must be inside :D
        return True

    def check_viewport_lhs_or_rhs(self, viewportBounds: ChunkBoundingBox):
        """ Checks whether the viewport is to the RHS or LHS """
        pass

class ChunkManifestObjectFactory(object):
    def __init__(self):
        self.__classname__ = "ChunkManifestObjectFactory"

    def to_obj_v_0_3_0(self, inputDict: dict):
        """ Converts the firestore dict to its corresponding object. 
        
        VERSION:
        - This method supports doc_version 0.3.0

        ARGS:
        - inputDict: The dictionary received from firestore

        RETURNS:
        - On success, the method returns a `ChunkManifestDoc`. Otherwise returns
            `None`. 
        """
        try:
            docVersion = inputDict['doc_version'].split('.')
            
            # Get all of the manifests
            manifestsList = self.__manifest_instances_to_obj(
                    inputDict=inputDict)

            # Put together the chunk document
            return ChunkManifestDoc(chunkManifests=manifestsList,
                    createTime=inputDict['create_time'],
                    docType=BaseDocType(inputDict['doc_type']),
                    lastEditted=inputDict['last_editted'],
                    major=int(docVersion[0]),
                    minor=int(docVersion[1]),
                    patch=int(docVersion[2]),
                    ownerId=inputDict['owner_id'])
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.class_error_handler(fileName=__filename__,
                    className=self.__classname__, 
                    methodName='to_obj_v_0_3_0', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            return None

    
    def __manifest_instances_to_obj(self, inputDict: dict):
        manifestsList: List[ChunkManifestIndex] = []
        for instance in inputDict['manifests']:
            manifestsList.append(ChunkManifestIndex(
                chunkGroupBoundingBox=ChunkBoundingBox(
                    topRightLat=instance['group_bounds']['top_right']['lat'],
                    topRightLng=instance['group_bounds']['top_right']['lng'],
                    bottomLeftLat=instance['group_bounds']['bottom_left']['lat'],
                    bottomLeftLng=instance['group_bounds']['bottom_left']['lng']),
                currentCapacity=instance['current_capacity'],
                docId=instance['doc_id'],
                numChunks=instance['num_chunks']))
        return manifestsList

class ChunkDocObjectFactory(object):
    def __init__(self):
        self.__classname__ = "ChunkDocObjectFactory"

    def to_obj_v_0_3_0(self, inputDict: dict):
        try:
            docVersion = inputDict['doc_version'].split('.')

            chunksList = self.__chunk_instances_to_obj(inputDict)

            return ChunkDoc(chunkType=ChunkType(inputDict['chunk_type']),
                    bounds=ChunkBoundingBox(
                            topRightLat=inputDict['bounds']['top_right']['lat'],
                            topRightLng=inputDict['bounds']['top_right']['lng'],
                            bottomLeftLat=inputDict['bounds']['bottom_left']['lat'],
                            bottomLeftLng=inputDict['bounds']['bottom_left']['lng']),
                    chunks=chunksList,
                    boundsType=ChunkManifestBoundsType(inputDict['bounds_type']),
                    createTime=inputDict['create_time'],
                    currentCapacity=inputDict['current_capacity'],
                    docType=BaseDocType(inputDict['doc_type']),
                    lastEditted=inputDict['last_editted'],
                    major=int(docVersion[0]),
                    minor=int(docVersion[1]),
                    patch=int(docVersion[2]),
                    ownerId=inputDict['owner_id'])

        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.class_error_handler(fileName=__filename__,
                    className=self.__classname__, 
                    methodName='to_obj_v_0_3_0', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            return None


    def __chunk_instances_to_obj(self, inputDict: dict):
        chunksList: List[Chunk] = []
        for chunk in inputDict['chunks']:
            chunksList.append(Chunk(
                lat1=chunk['vert_1']['lat'],
                lat2=chunk['vert_2']['lat'],
                lat3=chunk['vert_3']['lat'],
                lng1=chunk['vert_1']['lng'],
                lng2=chunk['vert_2']['lng'],
                lng3=chunk['vert_3']['lng'],
                intersectionsIdsList=chunk['intersection_ids'],
                chunkId=chunk['chunk_id']))
        return chunksList

class IntersectionsDocObjectFactory(object):
    def __init__(self):
        pass

    def to_obj_v_0_3_0(self, inputDict: dict):
        docVersion = inputDict['doc_version'].split('.')
        
        listOfIntersections: List[Intersection] = []
        for intersection in inputDict['intersections']:
            listOfIntersections.append(
                    self.__intersections_to_obj(intersection))

        # Construct the full document
        return IntersectionsDoc(createTime=inputDict['create_time'],
                currentCapacity=inputDict['current_capacity'],
                docType=BaseDocType(inputDict['doc_type']),
                intersections=listOfIntersections,
                lastEditted=inputDict['last_editted'],
                major=int(docVersion[0]),
                minor=int(docVersion[1]),
                patch=int(docVersion[2]),
                ownerId=inputDict['owner_id'])
    
    def __intersections_to_obj(self, inputDict: dict):
        """ Converts each of the intersection dicts to object representations. 
        """
        listOfArms: List[Arm] = []
        for arm in inputDict['arms']:
            # Gather all the steps in this arm
            listOfSteps: List[Step] = []
            for step in arm['route']['steps']:
                # Gather all the coordinates in this step
                listOfCoords: List[Coordinates] = []
                for coord in step['coords']:
                    listOfCoords.append(Coordinates(
                            latitude=coord['lat'],
                            longitude=coord['lng']))
                listOfSteps.append(Step(coords=listOfCoords,
                        distanceValue=step['distance_value'],
                        durationValue=step['duration_value']))
                
            listOfArms.append(Arm(nextTsc=arm['next_tsc'],
                    route=Route(steps=listOfSteps),
                    streetName=arm['street_name']))

        return Intersection(origin=Coordinates(
                latitude=inputDict['origin']['lat'],
                longitude=inputDict['origin']['lng']),
                arms=listOfArms,
                tsc=inputDict['tsc'])

class DataFabricator(object):
    def __init__(self):
        """ Contains methods that help in synthesising new data from limited
            datasets. 
        """
        pass

class GoogleMapsTrafficHelperTransactions(object):
    def __init__(self):
        pass

class IntersectionsTransactions(object):
    def __init__(self):
        self.db = firestore.Client()

        self.transaction = self.db.transaction()

        self.intersectionsRef = self.db.collection(u'intersections') # type: ignore

    def get_intersection_docs(self):
        """ Retreives a intersections doc from firestore. """
        docStream = self.intersectionsRef.stream()  # type: ignore
        listOfIntersectionDocs: List[IntersectionsDoc] = []
        idof = IntersectionsDocObjectFactory() 
        for doc in docStream:
            yield idof.to_obj_v_0_3_0(doc.to_dict()) # type: ignore

    def get_intersection_doc_manifest(self):
        """ Retreives the intersections doc manifest from firestore. """
        pass

    def write_intersection_doc_manifest(self, 
            manifToWrite: IntersectionsDocManifest):
        """ Writes an intersections doc manifest to firestore. """
        return _write_intersection_doc_manifest_to_firestore(
                transaction=self.transaction,
                intersectionDocManifest=manifToWrite,
                intersectionsDocRoot=self.intersectionsRef)

    def write_intersections_doc(self, 
            intersectionsDocToWrite: IntersectionsDoc):
        """ Writes an intersections doc to firestore. """
        return _write_intersection_doc_to_firestore(
                transaction=self.transaction,
                intersectionDoc=intersectionsDocToWrite,
                intersectionsDocRoot=self.intersectionsRef)

class ChunkGetter(object):
    def __init__(self):
        self.db = firestore.Client()

        self.transaction = self.db.transaction()

        self.chunksRef = self.db.collection(u'chunks') # type: ignore

        # Debugging variables
        self.__classname__ = 'GoogleMapsTrafficHelper'

    def get_all_chunks(self, chunkType: ChunkType):
        """ Gets all chunks of a particular type. 
        
        NOTE: 
        - Do not call this if your database is large.
        """
        try:
            if chunkType == ChunkType.FAR_CHUNK:
                manifest = 'far_chunk_manifest'
            elif chunkType == ChunkType.STANDARD_CHUNK:
                manifest = 'standard_chunk_manifest'
            elif chunkType == ChunkType.NEAR_CHUNK:
                manifest = 'near_chunk_manifest'
            else:
                manifest = 'close_chunk_manifest'
            chunksStream = self.chunksRef.document(manifest)\
                        .collection('chunk_data').stream()

            cdof = ChunkDocObjectFactory()
            listOfChunkDocs: List[ChunkDoc] = []
            for doc in chunksStream:
                chunkDoc = cdof.to_obj_v_0_3_0(doc.to_dict())
                
                if not isinstance(chunkDoc, ChunkDoc):
                    raise Exception("Error trying to get chunks")
                
                listOfChunkDocs.append(chunkDoc)

            return listOfChunkDocs

        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.class_error_handler(fileName=__filename__,
                    className=self.__classname__,
                    methodName='get_all_chunks', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            return False
    
    def write_chunk_to_firestore(self, chunkDoc: ChunkDoc, chunkType: ChunkType):
        try:
            if chunkType == ChunkType.FAR_CHUNK:
                manifest = 'far_chunk_manifest'
            elif chunkType == ChunkType.STANDARD_CHUNK:
                manifest = 'standard_chunk_manifest'
            elif chunkType == ChunkType.NEAR_CHUNK:
                manifest = 'near_chunk_manifest'
            else:
                manifest = 'close_chunk_manifest'
                
            result = _save_chunks_doc_to_firestore(transaction=self.transaction,
                    chunksDocRoot=self.chunksRef,
                    chunkManifestId=manifest,
                    chunkDoc=chunkDoc)
            return result
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.class_error_handler(fileName=__filename__,
                    className=self.__classname__,
                    methodName='write_chunk_to_firestore', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            return False
        pass
        

    

# * MAIN HELPER ----------------------------------------------------------------

class GoogleMapsTrafficHelper(object):
    def __init__(self):
        """ Contains methods that help in acquiring new data from google cloud. 
        """
        self.db = firestore.Client()

        self.transaction = self.db.transaction()

        self.chunksRef = self.db.collection(u'chunks') # type: ignore

        # Debugging variables
        self.__classname__ = 'GoogleMapsTrafficHelper'

        self.__apiKey = "AIzaSyBkGJaqO5x2uOTT3mknrMozrx0UeLJ6G90"

        # Variables purely for generating the database
        self.__farChunksManifest: ChunkManifestDoc
        self.__standardChunksManifest: ChunkManifestDoc
        self.__nearChunksManifest: ChunkManifestDoc
        self.__closeChunksManifest: ChunkManifestDoc

        # Chunk buffers
        self.__currentFarChunkDoc: ChunkDoc
        self.__currentStandardChunkDoc: ChunkDoc
        self.__currentNearChunkDoc: ChunkDoc
        self.__currentCloseChunkDoc: ChunkDoc
    
    # PUBLIC METHODS -----------------------------------------------------------

    def get_traffic_data(self, origin: Coordinates, 
            destination: Coordinates):
        """ Gets the traffic data for a given origin and destination. 
        
        ARGS:
        - origin: The latitude and longitude coordinates for the origin
        - destination: The latitude and longitude coordinates for the destination

        RETURNS:
        - On success, the method will return the estimated travel time in 
            seconds.

        NOTE:
        As of 9/03/2022, there is no way to know whether the route changes 
            during peak times.
        """
        requestString = "https://maps.googleapis.com/maps/api/directions/" + \
                "json?origin={}, {}&".format(origin.get_latitude(), 
                        origin.get_longitude()) + \
                "destination={}, {}&".format(destination.get_latitude(),
                        destination.get_longitude()) + \
                "departure_time=now&key={}".format(self.__apiKey)
        
        response = requests.post(requestString)

        
        for route in json.loads(response.text)['routes']:
            print("{} seconds".format(route['legs'][0]['duration_in_traffic']\
                    ['value']))

    def delete_current_database(self):
        """ Deletes the current database.

        ### WARNING !
        This method is no joke, it will recursively delete the entire database.
        """
        try:
            @firestore.transactional
            def transactionThing(transaction: firestore.Transaction):
                docs = self.chunksRef.document('close_chunk_manifest').collection('chunk_data').limit(10).stream(transaction=transaction)
                for doc in docs:
                    docRef = doc.reference
                    self.transaction.delete(docRef)

            manifest = self.chunksRef.document('close_chunk_manifest').get()    # type: ignore

            # totalChunkDocs = len(manifest.to_dict()['manifests']) # type: ignore
            iterations = round(1000/10.0)
            for iterThing in range(0, iterations - 1):
                transactionThing(transaction=self.transaction)
            return True
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.class_error_handler(fileName=__filename__,
                    className=self.__classname__,
                    methodName='init_database', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            return False

    def save_chunk_to_firestore(self,
            chunksDocRoot: firestore.CollectionReference,
            chunkManifestDoc: ChunkManifestDoc,
            chunkDoc: ChunkDoc):
        """ Saves a specified chunk document to firestore. """
        return _save_chunk_to_firestore(
                transaction=self.transaction,
                chunksDocRoot=chunksDocRoot,
                chunkManifestDoc=chunkManifestDoc,
                chunkDoc=chunkDoc)

    def get_chunks_in_view(self, viewMode: ChunkType, 
            viewportBounds: ChunkBoundingBox,
            viewportBoundsConfig: ViewportBoundsConfig):
        """ Grabs the set of chunks that are in view of the camera. 
        
        Behaviour will change depending on the current mode of the camera.
        (i.e. if the user requests chunks within the viewport at FAR mode, then
                chunks will be sampled from the `far_chunks_manifest`)
        """
        try:
            manifestDoc = self.__get_chunk_manifest(viewMode)
            specialManifestDoc = self.__get_special_chunk_manifest(viewMode)
            
            
            
            if manifestDoc == None or specialManifestDoc == None:
                raise Exception("Error trying to get manifestDoc " + \
                        "or specialManifestDoc")

            chunksToReturn: List[Chunk] = []
            listOfChunkIds: List[str] = []

            if viewportBoundsConfig == ViewportBoundsConfig.LHS or \
                    viewportBoundsConfig == ViewportBoundsConfig.RHS:
                chunksToReturn = self.check_chunks_in_view(
                        viewportBounds=viewportBounds,
                        manifestDoc=manifestDoc,
                        specialManifestDoc=specialManifestDoc,
                        viewMode=viewMode,
                        viewportConfig=viewportBoundsConfig)

            else:
                if viewportBoundsConfig == ViewportBoundsConfig.BOTH:
                    # Split the viewport into LHS and RHS about zero degrees.
                    viewportLHS, viewportRHS = self.__split_viewport_down_0(
                            viewportBounds=viewportBounds)
                else:
                    # Split the viewport into LHS and RHS about 180 degrees.
                    viewportLHS, viewportRHS = self.__split_viewport_down_180(
                            viewportBounds=viewportBounds)
                
                # LHS
                lhsChunksToReturn = self.check_chunks_in_view(
                        viewportBounds=viewportLHS,
                        manifestDoc=manifestDoc,
                        specialManifestDoc=specialManifestDoc,
                        viewMode=viewMode,
                        viewportConfig=ViewportBoundsConfig.LHS)
                
                # RHS
                rhsChunksToReturn = self.check_chunks_in_view(
                        viewportBounds=viewportRHS,
                        manifestDoc=manifestDoc,
                        specialManifestDoc=specialManifestDoc,
                        viewMode=viewMode,
                        viewportConfig=ViewportBoundsConfig.RHS)

                # Merge the two lists
                for chunk in lhsChunksToReturn:
                    if chunk.get_chunk_id() not in listOfChunkIds:
                        chunksToReturn.append(chunk)
                        listOfChunkIds.append(chunk.get_chunk_id())

                for chunk in rhsChunksToReturn:
                    if chunk.get_chunk_id() not in listOfChunkIds:
                        chunksToReturn.append(chunk)
                        listOfChunkIds.append(chunk.get_chunk_id())

            return chunksToReturn
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.class_error_handler(fileName=__filename__,
                    className=self.__classname__, 
                    methodName='get_chunks_in_view', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            return None

    

    # PRIVATE METHODS ----------------------------------------------------------

    def check_chunks_in_view(self,
            viewportBounds: ChunkBoundingBox,
            viewMode: ChunkType,
            manifestDoc: ChunkManifestDoc,
            specialManifestDoc: ChunkManifestDoc,
            viewportConfig: ViewportBoundsConfig):
        """ Checks if the chunks are in view on the LHS (rel to split). """
        chunksToReturn: List[Chunk] = []
        
        bbh = BoundingBoxHelper()

        # Determine which document has chunks in our view
        for manifest in manifestDoc.get_chunk_manifests():
            if bbh.check_bounding_box_in_view(viewportBounds, 
                    manifest.get_bounding_box()):
                chunkDoc = self.__get_chunk_doc(viewMode, 
                        manifest.get_doc_id())

                if not isinstance(chunkDoc, ChunkDoc):
                    raise Exception("Error getting chunk from firestore {}"\
                            .format(manifest.get_doc_id()))

                # Get the chunks that are in the viewport
                for chunk in chunkDoc.get_chunks():
                    if bbh.check_bounding_box_in_view(viewportBounds, 
                            chunk.get_bounding_box()):
                        chunksToReturn.append(chunk)
            else:
                pass
                # logging.info('No manifests in range: {}'.format(manifest.get_doc_id()))
        
        # TODO
        if viewportConfig == ViewportBoundsConfig.LHS:
            # LHS
            for sManifest in specialManifestDoc.get_chunk_manifests():
                chunkManifestBoundingBox = sManifest.get_bounding_box()
                
                v1 = chunkManifestBoundingBox.get_top_right_vert()
                v1.set_lng(v1.get_lng() + 360.0)
                chunkManifestBoundingBox.set_top_right_vert(v1)

                if bbh.check_bounding_box_in_view(
                        viewportBounds=viewportBounds,
                        chunkBoundingBox=chunkManifestBoundingBox):
                    chunkDoc = self.__get_special_chunk_doc(viewMode,
                            sManifest.get_doc_id())
                    
                    if not isinstance(chunkDoc, ChunkDoc):
                        raise Exception("Error getting chunk from firestore {}"\
                                .format(sManifest.get_doc_id()))

                    # Get the chunks that are in the viewport
                    for chunk in chunkDoc.get_chunks():
                        chunkBB = chunk.get_bounding_box()
                        if chunk.get_chunk_mode() == ChunkMode.SPLIT:
                            v1 = chunkBB.get_top_right_vert()
                            v1.set_lng(v1.get_lng() + 360.0)
                            chunkBB.set_top_right_vert(v1)

                        if bbh.check_bounding_box_in_view(
                                viewportBounds=viewportBounds,
                                chunkBoundingBox=chunkBB):
                            chunksToReturn.append(chunk)

        elif viewportConfig == ViewportBoundsConfig.RHS:
            # RHS
            for sManifest in specialManifestDoc.get_chunk_manifests():
                chunkManifestBoundingBox = sManifest.get_bounding_box()
                v2 = chunkManifestBoundingBox.get_bottom_left_vert()
                v2.set_lng(v2.get_lng() - 360.0)
                chunkManifestBoundingBox.set_bottom_left_vert(v2)
                    

                if bbh.check_bounding_box_in_view(
                        viewportBounds=viewportBounds,
                        chunkBoundingBox=chunkManifestBoundingBox):
                    chunkDoc = self.__get_special_chunk_doc(viewMode,
                            sManifest.get_doc_id())
                    
                    if not isinstance(chunkDoc, ChunkDoc):
                        raise Exception("Error getting chunk from firestore {}"\
                                .format(sManifest.get_doc_id()))

                    # Get the chunks that are in the viewport
                    for chunk in chunkDoc.get_chunks():
                        chunkBB = chunk.get_bounding_box()
                        if chunk.get_chunk_mode() == ChunkMode.SPLIT:
                            v2 = chunkBB.get_bottom_left_vert()
                            v2.set_lng(v2.get_lng() - 360.0)
                            chunkBB.set_bottom_left_vert(v2)
                        
                        if bbh.check_bounding_box_in_view(
                                viewportBounds=viewportBounds,
                                chunkBoundingBox=chunkBB):
                            chunksToReturn.append(chunk)

        return chunksToReturn

    def __split_viewport_down_180(self, 
            viewportBounds: ChunkBoundingBox
            ) -> Tuple[ChunkBoundingBox, ChunkBoundingBox]:
        """ Splits the viewport in two, about the 180 degree line. 
        
        RETURNS:
        - On success, the method returns a `Tuple` containing the LHS and RHS
            viewport bounds respectively.
        """
        v1 = viewportBounds.get_top_right_vert()
        v2 = viewportBounds.get_bottom_left_vert()
        
        viewportLHS = ChunkBoundingBox(
            topRightLat=v1.get_lat(),
            topRightLng=180.0,
            bottomLeftLat=v2.get_lat(),
            bottomLeftLng=v2.get_lng())

        viewportRHS = ChunkBoundingBox(
            topRightLat=v1.get_lat(),
            topRightLng=v1.get_lng(),
            bottomLeftLat=v2.get_lat(),
            bottomLeftLng=-180.0)
        
        return (viewportLHS, viewportRHS)

    def __split_viewport_down_0(self, viewportBounds: ChunkBoundingBox):
        """ Splits the viewport in two, about the 0 degree line. 
        
        RETURNS:
        - On success, the method returns a `Tuple` containing the LHS and RHS
            viewport bounds respectively.
        """
        v1 = viewportBounds.get_top_right_vert()
        v2 = viewportBounds.get_bottom_left_vert()
        
        viewportLHS = ChunkBoundingBox(
            topRightLat=v1.get_lat(),
            topRightLng=0,
            bottomLeftLat=v2.get_lat(),
            bottomLeftLng=v2.get_lng())

        viewportRHS = ChunkBoundingBox(
            topRightLat=v1.get_lat(),
            topRightLng=v1.get_lng(),
            bottomLeftLat=v2.get_lat(),
            bottomLeftLng=0)
        
        return (viewportLHS, viewportRHS)

    def __get_chunk_manifest(self, chunkType: ChunkType):
        """ Gets the manifest for a particular chunk type. """
        if chunkType == ChunkType.FAR_CHUNK:
            docSnapshot = self.chunksRef.document('far_chunk_manifest')\
                    .get()  # type: ignore
        elif chunkType == ChunkType.STANDARD_CHUNK:
            docSnapshot = self.chunksRef.document('standard_chunk_manifest')\
                    .get()  # type: ignore
        elif chunkType == ChunkType.NEAR_CHUNK:
            docSnapshot = self.chunksRef.document('near_chunk_manifest')\
                    .get()  # type: ignore
        else:
            docSnapshot = self.chunksRef.document('close_chunk_manifest')\
                    .get()  # type: ignore

        if not docSnapshot.exists:
                raise Exception("Type {} Chunk manifest doesn't exist".format(
                        chunkType.value))

        # Convert the manifest to an object
        cmof = ChunkManifestObjectFactory()
        return cmof.to_obj_v_0_3_0(docSnapshot.to_dict())  # type: ignore

    def __get_special_chunk_manifest(self, chunkType: ChunkType):
        """ Gets the manifest for a particular special chunk type. 
        
        The `special` part, refers to chunks that are either `on-the-line` or
            `split` along the abrupt 180 to -180 degree jump in longitude.
        """ 
        if chunkType == ChunkType.FAR_CHUNK:
            docSnapshot = self.chunksRef.document('special_far_chunk_manifest')\
                    .get()  # type: ignore
        elif chunkType == ChunkType.STANDARD_CHUNK:
            docSnapshot = self.chunksRef.document('special_standard_chunk_manifest')\
                    .get()  # type: ignore
        elif chunkType == ChunkType.NEAR_CHUNK:
            docSnapshot = self.chunksRef.document('special_near_chunk_manifest')\
                    .get()  # type: ignore
        else:
            docSnapshot = self.chunksRef.document('special_close_chunk_manifest')\
                    .get()  # type: ignore

        if not docSnapshot.exists:
                raise Exception("Type {} Special Chunk manifest doesn't exist".format(
                        chunkType.value))

        # Convert the manifest to an object
        cmof = ChunkManifestObjectFactory()
        return cmof.to_obj_v_0_3_0(docSnapshot.to_dict())  # type: ignore

    def __get_special_chunk_doc(self, chunkType: ChunkType, chunkDocId: str):
        """ Gets the special chunk doc. """
        if chunkType == ChunkType.FAR_CHUNK:
            docRef = self.chunksRef.document('special_far_chunk_manifest')
        elif chunkType == ChunkType.STANDARD_CHUNK:
            docRef = self.chunksRef.document('special_standard_chunk_manifest')
        elif chunkType == ChunkType.NEAR_CHUNK:
            docRef = self.chunksRef.document('special_near_chunk_manifest')
        else:
            docRef = self.chunksRef.document('special_close_chunk_manifest')
        
        # Get the specific chunk doc
        docSnapshot = docRef.collection('chunk_data').document(chunkDocId).get()

        if not docSnapshot.exists:
                raise Exception("Type {} Chunk doc {} doesn't exist".format(
                        chunkType.value, chunkDocId))

        cdof = ChunkDocObjectFactory()
        return cdof.to_obj_v_0_3_0(docSnapshot.to_dict())

    def __get_chunk_doc(self, chunkType: ChunkType, chunkDocId: str):
        """ Gets the chunk doc """
        if chunkType == ChunkType.FAR_CHUNK:
            docRef = self.chunksRef.document('far_chunk_manifest')
        elif chunkType == ChunkType.STANDARD_CHUNK:
            docRef = self.chunksRef.document('standard_chunk_manifest')
        elif chunkType == ChunkType.NEAR_CHUNK:
            docRef = self.chunksRef.document('near_chunk_manifest')
        else:
            docRef = self.chunksRef.document('close_chunk_manifest')

        # Get the specific chunk doc
        docSnapshot = docRef.collection('chunk_data').document(chunkDocId).get()

        if not docSnapshot.exists:
                raise Exception("Type {} Chunk doc {} doesn't exist".format(
                        chunkType.value, chunkDocId))

        cdof = ChunkDocObjectFactory()
        return cdof.to_obj_v_0_3_0(docSnapshot.to_dict())

# * TRANSACTIONS ---------------------------------------------------------------

@firestore.transactional
def _save_chunk_to_firestore(
        transaction: firestore.Transaction,
        chunksDocRoot: firestore.CollectionReference,
        chunkManifestDoc: ChunkManifestDoc,
        chunkDoc: ChunkDoc):
    """  """
    try:
        chunkManifestRef = chunksDocRoot.document(chunkManifestDoc\
                .get_owner_id())

        chunkDocRef = chunksDocRoot.document(chunkManifestDoc.get_owner_id())\
                .collection('chunk_data').document(chunkDoc.get_owner_id())

        transaction.set(chunkManifestRef, chunkManifestDoc.to_dict())
        transaction.set(chunkDocRef, chunkDoc.to_dict())

        return GoogleMapsTrafficHelperTransactionStatus.TRANSACTION_SUCCESS
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        feh = FirestoreErrorHelper()
        feh.transaction_error_handler(fileName=__filename__, 
                methodName='_save_chunk_to_firestore', 
                exceptionMsg=e,
                lineNum=exc_tb.tb_lineno)   # type: ignore
        return GoogleMapsTrafficHelperTransactionStatus.TRANSACTION_ERROR

@firestore.transactional
def _save_chunks_doc_to_firestore(transaction: firestore.Transaction,
        chunksDocRoot: firestore.CollectionReference,
        chunkManifestId: str,
        chunkDoc: ChunkDoc):
    try:
        chunkDocRef = chunksDocRoot.document(chunkManifestId)\
                .collection('chunk_data').document(chunkDoc.get_owner_id())

        transaction.set(chunkDocRef, chunkDoc.to_dict())
        return GoogleMapsTrafficHelperTransactionStatus.TRANSACTION_SUCCESS
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        feh = FirestoreErrorHelper()
        feh.transaction_error_handler(fileName=__filename__, 
                methodName='_save_chunk_to_firestore', 
                exceptionMsg=e,
                lineNum=exc_tb.tb_lineno)   # type: ignore
        return GoogleMapsTrafficHelperTransactionStatus.TRANSACTION_ERROR

@firestore.transactional
def _write_intersection_doc_to_firestore(transaction: firestore.Transaction,
        intersectionsDocRoot: firestore.CollectionReference,
        intersectionDoc: IntersectionsDoc):
    try:
        intersectionDocRef = intersectionsDocRoot\
                .document('intersections_doc_manifest')\
                .collection('intersection_docs')\
                .document(intersectionDoc.get_owner_id())

        transaction.set(intersectionDocRef, intersectionDoc.to_dict())
        return GoogleMapsTrafficHelperTransactionStatus.TRANSACTION_SUCCESS
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        feh = FirestoreErrorHelper()
        feh.transaction_error_handler(fileName=__filename__, 
                methodName='_write_intersection_doc_to_firestore', 
                exceptionMsg=e,
                lineNum=exc_tb.tb_lineno)   # type: ignore
        return GoogleMapsTrafficHelperTransactionStatus.TRANSACTION_ERROR

@firestore.transactional
def _write_intersection_doc_manifest_to_firestore(transaction: firestore.Transaction,
        intersectionsDocRoot: firestore.CollectionReference,
        intersectionDocManifest: IntersectionsDocManifest):
    try:
        intersectionsManifestDocRef = intersectionsDocRoot\
                .document('intersections_doc_manifest')

        transaction.set(intersectionsManifestDocRef, 
                intersectionDocManifest.to_dict())
        return GoogleMapsTrafficHelperTransactionStatus.TRANSACTION_SUCCESS
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        feh = FirestoreErrorHelper()
        feh.transaction_error_handler(fileName=__filename__, 
                methodName='_write_intersection_doc_manifest_to_firestore', 
                exceptionMsg=e,
                lineNum=exc_tb.tb_lineno)   # type: ignore
        return GoogleMapsTrafficHelperTransactionStatus.TRANSACTION_ERROR


if __name__ == '__main__':
    gmth = GoogleMapsTrafficHelper()
    # origin = Coordinates(-27.47308, 153.005263)
    # destination = Coordinates(-27.485878, 153.031746)
    # gmth.get_traffic_data(origin, destination)
    # barSize = 20 * 16 * 16 * 16
    # with alive_bar(barSize) as bar:
    #     gmth.init_database(bar)

    gmth.delete_current_database()
    