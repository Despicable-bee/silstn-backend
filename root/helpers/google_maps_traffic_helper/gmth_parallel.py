# Standard Libs
from enum import Enum
from enum import auto

from typing import Union
from typing import Optional
from typing import List
from typing import Dict

from alive_progress import alive_bar

from multiprocessing import Process
from multiprocessing import Queue

import sys
import os
# Third Party Libs

from google.cloud import firestore
from root.helpers.google_maps_traffic_helper.google_maps_traffic_helper import BoundingBoxHelper

# Local Libs

from root.helpers.icosahedron_helper.icosahedron_helper import Icosahedron
from root.helpers.icosahedron_helper.icosahedron_helper import Triangle

from root.helpers.common_helper.common_firestore_helper import \
        FirestoreIdGenerator
from root.helpers.common_helper.common_firestore_helper import \
        FirestoreTimeHelper
from root.helpers.common_helper.common_firestore_helper import \
        FirestoreErrorHelper

# Base Classes

from root.base_classes.base_doc import BaseDocType

from root.base_classes.chunk_doc import Chunk
from root.base_classes.chunk_doc import ChunkManifestBoundsType
from root.base_classes.chunk_doc import ChunkMode
from root.base_classes.chunk_doc import LatLngVertex
from root.base_classes.chunk_doc import ChunkBoundingBox
from root.base_classes.chunk_doc import ChunkDoc
from root.base_classes.chunk_doc import ChunkManifestDoc
from root.base_classes.chunk_doc import ChunkManifestIndex
from root.base_classes.chunk_doc import ChunkType
from root.base_classes.chunk_doc import DocManifest

# Base Classes

# * DEBUG ----------------------------------------------------------------------

__filename__ = 'gmth_parallel.py'

# * ENUM -----------------------------------------------------------------------

class GoogleMapsTrafficHelperTransactionStatus(Enum):
    TRANSACTION_SUCCESS = auto()
    TRANSACTION_ERROR = auto()

class QueueMessageType(Enum):
    # Requests requiring a response
    CHILD_REQUEST_FAR_CHUNK_ID = auto()
    CHILD_REQUEST_STANDARD_CHUNK_ID = auto()
    CHILD_REQUEST_NEAR_CHUNK_ID = auto()
    CHILD_REQUEST_CLOSE_CHUNK_ID = auto()

    CHILD_REQUEST_FAR_CHUNK_DOC_ID = auto()
    CHILD_REQUEST_STANDARD_CHUNK_DOC_ID = auto()
    CHILD_REQUEST_NEAR_CHUNK_DOC_ID = auto()
    CHILD_REQUEST_CLOSE_CHUNK_DOC_ID = auto()

    CHILD_SEND_FAR_CHUNK_DOC = auto()
    CHILD_SEND_STANDARD_CHUNK_DOC = auto()
    CHILD_SEND_NEAR_CHUNK_DOC = auto()
    CHILD_SEND_CLOSE_CHUNK_DOC = auto()

    # Tells the main process that the child process is ready to be joined 
    CHILD_SEND_READY_TO_JOIN = auto()

    CHILD_SEND_PID = auto()

    CHILD_WAITING_FOR_ORDERS = auto()

    # Acknowledgement Responses
    ACK = auto()
    NACK = auto()

    # Process joining status codes
    CHILD_PROCESS_COMPLETED = auto()
    CHILD_PROCESS_ERROR = auto()

    # Parent Orders
    PARENT_ORDER_CHILD_TERMINATE = auto()
    PARENT_ORDER_CHILD_PROCESS_TRIANGLE = auto()

class QueueMessageRequiredAction(Enum):
    """ Enum indicating the required action in response to a message."""
    REQUIRES_RESPONSE = auto()
    REQUIRES_ACKNOWLEDGE = auto()
    NO_RESPONSE_REQUIRED = auto()

# * MP MESSAGES ----------------------------------------------------------------

class UniqueChunkIdData(object):
    def __init__(self, chunkType: ChunkType, uniqueChunkId: str):
        self.__chunkType = chunkType
        self.__uniqueChunkId = uniqueChunkId

    def get_chunk_type(self):
        return self.__chunkType

    def set_chunk_type(self, newChunkType: ChunkType):
        self.__chunkType = newChunkType

    def get_unique_chunk_id(self):
        return self.__uniqueChunkId

    def set_unique_chunk_id(self, newChunkId: str):
        self.__uniqueChunkId = newChunkId    

class QueueMessage(object):
    def __init__(self, 
            msgType: QueueMessageType, 
            msgFrom: int,
            msgAction: QueueMessageRequiredAction,
            msgData: Optional[Union[
                    UniqueChunkIdData, 
                    str, 
                    Chunk, 
                    Triangle, 
                    ChunkDoc]]=None):
        """ Queue message that is used to exchange information between processes.

        ARGS:
        - msgType: The type of message that is being sent
        - msgAction: The required action the reciever should take in order to
            respond to this message.
        - msgData: The data associated with this particular message.
        """
        self.__msgType = msgType
        self.__msgFrom = msgFrom
        self.__msgAction = msgAction
        self.__msgData = msgData
    
    def get_message_type(self):
        return self.__msgType

    def set_message_type(self, newMessageType: QueueMessageType):
        self.__msgType = newMessageType

    def get_message_from(self):
        return self.__msgFrom

    def get_message_action(self):
        return self.__msgAction

    def set_message_action(self, newMessageAction: QueueMessageRequiredAction):
        self.__msgAction = newMessageAction

    def get_message_data(self):
        return self.__msgData

    def set_message_data(self, 
            newMessageData: Optional[Union[UniqueChunkIdData]]):
        self.__msgData = newMessageData
    
class QueueContainer(object):
    def __init__(self, fromProcessQueue: Queue, toProcessQueue: Queue):
        self.__fromProcessQueue = fromProcessQueue
        self.__toProcessQueue = toProcessQueue

    def get_from_process_queue(self):
        """ Queue to read from. """
        return self.__fromProcessQueue

    def get_to_process_queue(self):
        """ Queue to write to. """
        return self.__toProcessQueue

# * CHILD PROCESSES ------------------------------------------------------------

class ChildProcessDataTracker(object):
    def __init__(self, toChildQueue: Queue, process: Process):
        self.__toChildQueue = toChildQueue
        self.__process = process
    
    def get_process_handle(self):
        return self.__process

    def get_to_child_queue(self):
        return self.__toChildQueue

    def get_pid(self):
        return self.__process.pid

class ChildProcess(object):
    def __init__(self, queueContainer: QueueContainer):
        # Queues
        self.__toParentQueue = queueContainer.get_to_process_queue()
        self.__fromParentQueue = queueContainer.get_from_process_queue()
        
        # Process id of the current child process
        self.__pid = os.getpid()
        
        
        fth = FirestoreTimeHelper()
        self.__currentTime = fth.get_unix_epoch_timestamp_ms()

        # Firestore Variables
        self.db = firestore.Client()

        self.transaction = self.db.transaction()

        self.chunksRef = self.db.collection(u'chunks') # type: ignore        

    def get_pid(self):
        return self.__pid

    def get_to_parent_queue(self):
        return self.__toParentQueue

    def get_from_parent_queue(self):
        return self.__fromParentQueue

    def main_loop(self):
        """ Main loop for the child process. """

        # Initialise the chunk docs
        self.__init_chunk_docs()

        # Tell the parent that we're ready for directive
        while True:
            self.__toParentQueue.put(QueueMessage(
                    msgType=QueueMessageType.CHILD_WAITING_FOR_ORDERS,
                    msgAction=QueueMessageRequiredAction.REQUIRES_RESPONSE,
                    msgFrom=self.__pid))

            response: QueueMessage = self.__fromParentQueue.get(block=True)
            
            result = self.__process_parent_message(response)

            if not result:
                # Child process ready to join
                self.__send_parent_join_request()
                break

    def __process_parent_message(self, message: QueueMessage):
        """ Processes a message from the parent. 
        
        RETURNS: Regardless of whether the method succeeds or fails, it will
            always return either `True` or `False`.

            If the method returns `True`, then the child process should wait for
            the next instruction.

            If the method returns `False`, then the child process should 
                    terminate.
        """
        try:
            if message.get_message_type() == QueueMessageType\
                        .PARENT_ORDER_CHILD_PROCESS_TRIANGLE:
                # See if there is a triangle in the data
                triangle = message.get_message_data()

                if not isinstance(triangle, Triangle):
                    print("Expected a " + \
                            "triangle, got: {}".format(type(triangle)))
                    return

                # Begin processing
                self.__process_icosahedron_far_chunk(triangle=triangle)
                
                return True

            elif message.get_message_type() == QueueMessageType\
                    .PARENT_ORDER_CHILD_TERMINATE:
                # Send the remaining chunk docs to the parent 
                self.__save_chunk_doc_to_firestore(
                        newChunkDocType=QueueMessageType\
                                .CHILD_SEND_FAR_CHUNK_DOC,
                        boundsType=ChunkManifestBoundsType.NORMAL,
                        newChunkDoc=self.__currentFarChunkDoc)
                
                self.__save_chunk_doc_to_firestore(
                        newChunkDocType=QueueMessageType\
                                .CHILD_SEND_FAR_CHUNK_DOC,
                        boundsType=ChunkManifestBoundsType.SPECIAL,
                        newChunkDoc=self.__currentSpecialFarChunkDoc)

                # Standard chunk docs
                self.__save_chunk_doc_to_firestore(
                        newChunkDocType=QueueMessageType\
                                .CHILD_SEND_STANDARD_CHUNK_DOC,
                        boundsType=ChunkManifestBoundsType.NORMAL,
                        newChunkDoc=self.__currentStandardChunkDoc)
                
                self.__save_chunk_doc_to_firestore(
                        newChunkDocType=QueueMessageType\
                                .CHILD_SEND_STANDARD_CHUNK_DOC,
                        boundsType=ChunkManifestBoundsType.SPECIAL,
                        newChunkDoc=self.__currentSpecialStandardChunkDoc)

                # Near Chunk Docs
                self.__save_chunk_doc_to_firestore(
                        newChunkDocType=QueueMessageType\
                                .CHILD_SEND_NEAR_CHUNK_DOC,
                        boundsType=ChunkManifestBoundsType.NORMAL,
                        newChunkDoc=self.__currentNearChunkDoc)

                self.__save_chunk_doc_to_firestore(
                        newChunkDocType=QueueMessageType\
                                .CHILD_SEND_NEAR_CHUNK_DOC,
                        boundsType=ChunkManifestBoundsType.SPECIAL,
                        newChunkDoc=self.__currentSpecialNearChunkDoc)
                
                # Close chunk docs
                self.__save_chunk_doc_to_firestore(
                        newChunkDocType=QueueMessageType\
                                .CHILD_SEND_CLOSE_CHUNK_DOC,
                        boundsType=ChunkManifestBoundsType.NORMAL,
                        newChunkDoc=self.__currentCloseChunkDoc)
                
                self.__save_chunk_doc_to_firestore(
                        newChunkDocType=QueueMessageType\
                                .CHILD_SEND_CLOSE_CHUNK_DOC,
                        boundsType=ChunkManifestBoundsType.SPECIAL,
                        newChunkDoc=self.__currentSpecialCloseChunkDoc)

                return False
            else:
                self.__send_parent_error_message("Unknown order: {}".format(
                        message.get_message_type().name))
                return True
            
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.transaction_error_handler(fileName=__filename__, 
                    methodName='__process_parent_direction', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            return False

    def __process_icosahedron_far_chunk(self, triangle: Triangle):
        """ Processes one of the 320 icosahedron far chunks. """
        cartCoords = triangle.get_cartographic_coords()

        # Request a unique far chunk id
        chunkId = self.__ask_parent_for_unique_id(
                    uniqueIdType=QueueMessageType.\
                            CHILD_REQUEST_FAR_CHUNK_ID)

        # Create the new chunk
        newChunk = Chunk(
                lat1=cartCoords[0][0],
                lat2=cartCoords[1][0],
                lat3=cartCoords[2][0],
                lng1=cartCoords[0][1],
                lng2=cartCoords[1][1],
                lng3=cartCoords[2][1],
                intersectionsIdsList=[],
                chunkId=chunkId)

        if newChunk.get_chunk_mode() != ChunkMode.NORMAL:
            boundsType = ChunkManifestBoundsType.SPECIAL
        else:
            boundsType = ChunkManifestBoundsType.NORMAL

        self.__add_chunk_to_doc(chunkType=ChunkType.FAR_CHUNK,
                    boundsType=boundsType, 
                    newChunk=newChunk)

        # Proceed with processing the standard chunk
        self.__process_icosahedron_standard_chunks(triangle)    

    def __process_icosahedron_standard_chunks(self, triangle: Triangle):
        """ Process a FAR chunk into 16 STANDARD chunks. """
        # Tessellate twice
        newTrianglesList: List[Triangle] = []
        tempTriangles = triangle.tessellate("", "", "")
        for tempTri in tempTriangles:
            temp = tempTri.tessellate("", "", "")
            for t in temp:
                newTrianglesList.append(t)
        
        # Create new chunks from this triangle and add to the buffer.
        for newTriangle in newTrianglesList:
            cartCoords = newTriangle.get_cartographic_coords()
            
            # ask parent for unique standard id
            chunkId = self.__ask_parent_for_unique_id(
                    uniqueIdType=QueueMessageType.\
                            CHILD_REQUEST_STANDARD_CHUNK_ID)

            # Create the new chunk
            newChunk = Chunk(
                lat1=cartCoords[0][0],
                lat2=cartCoords[1][0],
                lat3=cartCoords[2][0],
                lng1=cartCoords[0][1],
                lng2=cartCoords[1][1],
                lng3=cartCoords[2][1],
                intersectionsIdsList=[],
                chunkId=chunkId)

            if newChunk.get_chunk_mode() != ChunkMode.NORMAL:
                boundsType = ChunkManifestBoundsType.SPECIAL
            else:
                boundsType = ChunkManifestBoundsType.NORMAL

            self.__add_chunk_to_doc(chunkType=ChunkType.STANDARD_CHUNK,
                        boundsType=boundsType, 
                        newChunk=newChunk)

            # Proceed with processing the near chunk
            self.__process_icosahedron_near_chunks(newTriangle)

    def __process_icosahedron_near_chunks(self, triangle: Triangle):
        """ Process a STANDARD chunk into 16 NEAR chunks. """
        
        # Tessellate twice
        newTrianglesList: List[Triangle] = []
        tempTriangles = triangle.tessellate("", "", "")
        for tempTri in tempTriangles:
            temp = tempTri.tessellate("", "", "")
            for t in temp:
                newTrianglesList.append(t)

        # Create new chunks from this triangle and add to the buffer.
        for newTriangle in newTrianglesList:
            cartCoords = newTriangle.get_cartographic_coords()

            # Ask the parent for a unique id
            chunkId = self.__ask_parent_for_unique_id(
                    uniqueIdType=QueueMessageType.\
                            CHILD_REQUEST_NEAR_CHUNK_ID)

            # Create the new chunk
            newChunk = Chunk(
                lat1=cartCoords[0][0],
                lat2=cartCoords[1][0],
                lat3=cartCoords[2][0],
                lng1=cartCoords[0][1],
                lng2=cartCoords[1][1],
                lng3=cartCoords[2][1],
                intersectionsIdsList=[],
                chunkId=chunkId)

            if newChunk.get_chunk_mode() != ChunkMode.NORMAL:
                boundsType = ChunkManifestBoundsType.SPECIAL
            else:
                boundsType = ChunkManifestBoundsType.NORMAL

            self.__add_chunk_to_doc(chunkType=ChunkType.NEAR_CHUNK,
                        boundsType=boundsType, 
                        newChunk=newChunk)

            # Proceed with processing the near chunk
            self.__process_icosahedron_close_chunks(newTriangle)

    def __process_icosahedron_close_chunks(self, triangle: Triangle):
        """ Process a NEAR chunk into 16 CLOSE chunks. """

        # Tessellate twice
        newTrianglesList: List[Triangle] = []
        tempTriangles = triangle.tessellate("", "", "")
        for tempTri in tempTriangles:
            temp = tempTri.tessellate("", "", "")
            for t in temp:
                newTrianglesList.append(t)

        # Create new chunks from this triangle and add to the buffer.
        for newTriangle in newTrianglesList:
            cartCoords = newTriangle.get_cartographic_coords()

            # Ask the parent for a unique id
            chunkId = self.__ask_parent_for_unique_id(
                    uniqueIdType=QueueMessageType.\
                            CHILD_REQUEST_CLOSE_CHUNK_ID)

            # Create the new chunk
            newChunk = Chunk(
                lat1=cartCoords[0][0],
                lat2=cartCoords[1][0],
                lat3=cartCoords[2][0],
                lng1=cartCoords[0][1],
                lng2=cartCoords[1][1],
                lng3=cartCoords[2][1],
                intersectionsIdsList=[],
                chunkId=chunkId)

            # Give the parent the new chunk
            if newChunk.get_chunk_mode() != ChunkMode.NORMAL:
                boundsType = ChunkManifestBoundsType.SPECIAL
            else:
                boundsType = ChunkManifestBoundsType.NORMAL

            self.__add_chunk_to_doc(chunkType=ChunkType.CLOSE_CHUNK,
                        boundsType=boundsType, 
                        newChunk=newChunk)

    def __ask_parent_for_unique_id(self, uniqueIdType: QueueMessageType):
        """ Asks parent process for unique CHUNK id. """
        self.__toParentQueue.put(QueueMessage(
                msgType=uniqueIdType,
                msgAction=QueueMessageRequiredAction.REQUIRES_RESPONSE,
                msgFrom=self.__pid))

        response: QueueMessage = self.__fromParentQueue.get(block=True)

        if response.get_message_type() != QueueMessageType.ACK:
            self.__send_parent_error_message("Expected an ACK")
            raise Exception("Expected an ACK")

        chunkId = response.get_message_data()

        if not isinstance(chunkId, str):
            self.__send_parent_error_message("Expected a chunk id")
            raise Exception("Expected a chunk id")

        return chunkId

    def __send_parent_join_request(self):
        """ Tells the parent that the child is ready to join (terminate). """
        self.__toParentQueue.put(QueueMessage(
                msgType=QueueMessageType.CHILD_SEND_READY_TO_JOIN,
                msgAction=QueueMessageRequiredAction.NO_RESPONSE_REQUIRED,
                msgFrom=self.__pid))

    def __send_parent_ack_message(self):
        """ Tells the parent that the child acknowledges the request. """
        self.__toParentQueue.put(QueueMessage(
                msgType=QueueMessageType.ACK,
                msgAction=QueueMessageRequiredAction.NO_RESPONSE_REQUIRED,
                msgFrom=self.__pid))

    def __send_parent_error_message(self, errMsg: str):
        """ Sends the parent process an error message. """
        self.__toParentQueue.put(QueueMessage(
                msgType=QueueMessageType.CHILD_PROCESS_ERROR,
                msgAction=QueueMessageRequiredAction.NO_RESPONSE_REQUIRED,
                msgData=errMsg,
                msgFrom=self.__pid))

    def __save_chunk_doc_to_firestore(self, newChunkDocType: QueueMessageType,
            newChunkDoc: ChunkDoc, boundsType: ChunkManifestBoundsType):
        """ Saves a given chunk doc to firestore. """
        if newChunkDoc.get_chunk_type() == ChunkType.FAR_CHUNK:
            if boundsType == ChunkManifestBoundsType.NORMAL:
                chunkManifestDocId = 'far_chunk_manifest'
            else:
                chunkManifestDocId = 'special_far_chunk_manifest'
        elif newChunkDoc.get_chunk_type() == ChunkType.STANDARD_CHUNK:
            if boundsType == ChunkManifestBoundsType.NORMAL:
                chunkManifestDocId = 'standard_chunk_manifest'
            else:
                chunkManifestDocId = 'special_standard_chunk_manifest'
        elif newChunkDoc.get_chunk_type() == ChunkType.NEAR_CHUNK:
            if boundsType == ChunkManifestBoundsType.NORMAL:
                chunkManifestDocId = 'near_chunk_manifest'
            else:
                chunkManifestDocId = 'special_near_chunk_manifest'
        else:
            if boundsType == ChunkManifestBoundsType.NORMAL:
                chunkManifestDocId = 'close_chunk_manifest'
            else:
                chunkManifestDocId = 'special_close_chunk_manifest'

        # Save the chunk to the appropriate firestore doc
        result = _save_chunk_to_firestore(transaction=self.transaction,
                chunksDocRoot=self.chunksRef,
                chunkManifestDocId=chunkManifestDocId,
                chunkDoc=newChunkDoc)

        if result != GoogleMapsTrafficHelperTransactionStatus.TRANSACTION_SUCCESS:
            raise Exception("Error trying to save chunk to firestore")

        # Tell the parent that we save the document
        self.__toParentQueue.put(QueueMessage(
                    msgType=newChunkDocType,
                    msgAction=QueueMessageRequiredAction.REQUIRES_ACKNOWLEDGE,
                    msgData=newChunkDoc,
                    msgFrom=self.__pid))

        response: QueueMessage = self.__fromParentQueue.get(block=True)

        if response.get_message_type() != QueueMessageType.ACK:
            raise Exception("Error trying to acknowledge sending doc to parent")

    def __add_chunk_to_doc(self, chunkType: ChunkType, 
            boundsType: ChunkManifestBoundsType, newChunk: Chunk):
        """  """
        if chunkType == ChunkType.FAR_CHUNK:
            newChunkDocType = QueueMessageType.CHILD_SEND_FAR_CHUNK_DOC
            uniqueDocIdType = QueueMessageType.CHILD_REQUEST_FAR_CHUNK_DOC_ID
            if boundsType == ChunkManifestBoundsType.NORMAL:
                chunkDoc = self.__currentFarChunkDoc
            else:
                chunkDoc = self.__currentSpecialFarChunkDoc
        elif chunkType == ChunkType.STANDARD_CHUNK:
            newChunkDocType = QueueMessageType.CHILD_SEND_STANDARD_CHUNK_DOC
            uniqueDocIdType = QueueMessageType.CHILD_REQUEST_STANDARD_CHUNK_DOC_ID
            if boundsType == ChunkManifestBoundsType.NORMAL:
                chunkDoc = self.__currentStandardChunkDoc
            else:
                chunkDoc = self.__currentSpecialStandardChunkDoc
        elif chunkType == ChunkType.NEAR_CHUNK:
            newChunkDocType = QueueMessageType.CHILD_SEND_NEAR_CHUNK_DOC
            uniqueDocIdType = QueueMessageType.CHILD_REQUEST_NEAR_CHUNK_DOC_ID
            if boundsType == ChunkManifestBoundsType.NORMAL:
                chunkDoc = self.__currentNearChunkDoc
            else:
                chunkDoc = self.__currentSpecialNearChunkDoc
        else:
            newChunkDocType = QueueMessageType.CHILD_SEND_CLOSE_CHUNK_DOC
            uniqueDocIdType = QueueMessageType.CHILD_REQUEST_CLOSE_CHUNK_DOC_ID
            if boundsType == ChunkManifestBoundsType.NORMAL:
                chunkDoc = self.__currentCloseChunkDoc
            else:
                chunkDoc = self.__currentSpecialCloseChunkDoc
        
        # Attempt to add the chunk to the chunk doc.
        if newChunk.get_chunk_mode() != ChunkMode.NORMAL:
            
            result = chunkDoc.add_chunk(newChunk)
            
            if not result:
                # Send chunk doc to parent
                self.__save_chunk_doc_to_firestore(
                        newChunkDocType=newChunkDocType,
                        newChunkDoc=chunkDoc,
                        boundsType=ChunkManifestBoundsType.SPECIAL)
                
                # Create a new chunk doc
                farChunkDocId = self.__ask_parent_for_unique_id(
                        uniqueIdType=uniqueDocIdType)

                chunkDoc = self.__create_new_chunk_doc(chunkType=chunkType,
                        boundsType=boundsType,
                        chunkId=farChunkDocId)

                # Attempt to add the new chunk again
                result = chunkDoc.add_chunk(newChunk)

                if not result:
                    raise Exception("Error trying to create new " + \
                            "special chunk doc type: {}".format(chunkType.name))

        else:
            result = chunkDoc.add_chunk(newChunk)

            if not result:
                # Document is full, thus, save it to firestore and set a 
                #   blank one
                self.__save_chunk_doc_to_firestore(
                            newChunkDocType=newChunkDocType,
                            newChunkDoc=chunkDoc,
                            boundsType=ChunkManifestBoundsType.NORMAL)

                # Create a new chunk doc
                farChunkDocId = self.__ask_parent_for_unique_id(
                            uniqueIdType=uniqueDocIdType)

                chunkDoc = self.__create_new_chunk_doc(chunkType=chunkType,
                            boundsType=boundsType,
                            chunkId=farChunkDocId)

                # Attempt to add the new chunk again
                result = chunkDoc.add_chunk(newChunk)

                if not result:
                    raise Exception("Error trying to create new " + \
                            "chunk doc type: {}".format(chunkType.name))

    def __create_new_chunk_doc(self, chunkType: ChunkType, 
            boundsType: ChunkManifestBoundsType, chunkId: str):
        """ Creates new chunk doc, saves it to corresponding class variable. """
        if chunkType == ChunkType.FAR_CHUNK:
            baseDocType = BaseDocType.FAR_CHUNK_DOC
        elif chunkType == ChunkType.STANDARD_CHUNK:
            baseDocType = BaseDocType.STANDARD_CHUNK_DOC
        elif chunkType == ChunkType.NEAR_CHUNK:
            baseDocType = BaseDocType.NEAR_CHUNK_DOC
        elif chunkType == ChunkType.CLOSE_CHUNK:
            baseDocType = BaseDocType.CLOSE_CHUNK_DOC

        baseChunkDoc = ChunkDoc(
                chunkType=chunkType,
                chunks=[],
                createTime=self.__currentTime,
                docType=baseDocType,
                lastEditted=self.__currentTime,
                boundsType=boundsType,
                major=0,
                minor=3,
                patch=0,
                ownerId=chunkId,
                bounds=ChunkBoundingBox(0,0,0,0))

        if chunkType == ChunkType.FAR_CHUNK:
            if boundsType == ChunkManifestBoundsType.NORMAL:
                self.__currentFarChunkDoc = baseChunkDoc
                return self.__currentFarChunkDoc
            else:
                self.__currentSpecialFarChunkDoc = baseChunkDoc
                return self.__currentSpecialFarChunkDoc
        elif chunkType == ChunkType.STANDARD_CHUNK:
            if boundsType == ChunkManifestBoundsType.NORMAL:
                self.__currentStandardChunkDoc = baseChunkDoc
                return self.__currentStandardChunkDoc
            else:
                self.__currentSpecialStandardChunkDoc = baseChunkDoc
                return self.__currentSpecialStandardChunkDoc
        elif chunkType == ChunkType.NEAR_CHUNK:
            if boundsType == ChunkManifestBoundsType.NORMAL:
                self.__currentNearChunkDoc = baseChunkDoc
                return self.__currentNearChunkDoc
            else:
                self.__currentSpecialNearChunkDoc = baseChunkDoc
                return self.__currentSpecialNearChunkDoc
        elif chunkType == ChunkType.CLOSE_CHUNK:
            if boundsType == ChunkManifestBoundsType.NORMAL:
                self.__currentCloseChunkDoc = baseChunkDoc
                return self.__currentCloseChunkDoc
            else:
                self.__currentSpecialCloseChunkDoc = baseChunkDoc
                return self.__currentSpecialCloseChunkDoc

    def __init_chunk_docs(self):
        """ Initialises the various chunk docs for db generation. """ 
        # Request initial far chunk id
        farChunkId = self.__ask_parent_for_unique_id(
                uniqueIdType=QueueMessageType.CHILD_REQUEST_FAR_CHUNK_DOC_ID)

        # Current Far chunks
        self.__create_new_chunk_doc(chunkType=ChunkType.FAR_CHUNK,
                boundsType=ChunkManifestBoundsType.NORMAL, 
                chunkId=farChunkId)

        # Request initial special far chunk id
        specialFarChunkId = self.__ask_parent_for_unique_id(
                uniqueIdType=QueueMessageType.CHILD_REQUEST_FAR_CHUNK_DOC_ID)

        self.__create_new_chunk_doc(chunkType=ChunkType.FAR_CHUNK,
                boundsType=ChunkManifestBoundsType.SPECIAL, 
                chunkId=specialFarChunkId)

        # Request initial standard chunk id
        standardChunkId = self.__ask_parent_for_unique_id(
                uniqueIdType=QueueMessageType.CHILD_REQUEST_STANDARD_CHUNK_DOC_ID)

        # Current Standard chunks
        self.__create_new_chunk_doc(chunkType=ChunkType.STANDARD_CHUNK,
                boundsType=ChunkManifestBoundsType.NORMAL, 
                chunkId=standardChunkId)

        # Request initial standard chunk id
        specialStandardChunkId = self.__ask_parent_for_unique_id(
                uniqueIdType=QueueMessageType.CHILD_REQUEST_STANDARD_CHUNK_DOC_ID)

        self.__create_new_chunk_doc(chunkType=ChunkType.STANDARD_CHUNK,
                boundsType=ChunkManifestBoundsType.SPECIAL, 
                chunkId=specialStandardChunkId)

        # Request initial near chunk id
        nearChunkId = self.__ask_parent_for_unique_id(
                uniqueIdType=QueueMessageType.CHILD_REQUEST_NEAR_CHUNK_DOC_ID)

        # Current Near Chunks
        self.__create_new_chunk_doc(chunkType=ChunkType.NEAR_CHUNK,
                boundsType=ChunkManifestBoundsType.NORMAL, 
                chunkId=nearChunkId)

        # Request initial special near chunk id
        specialNearChunkId = self.__ask_parent_for_unique_id(
                uniqueIdType=QueueMessageType.CHILD_REQUEST_NEAR_CHUNK_DOC_ID)

        self.__create_new_chunk_doc(chunkType=ChunkType.NEAR_CHUNK,
                boundsType=ChunkManifestBoundsType.SPECIAL, 
                chunkId=specialNearChunkId)
        
        closeChunkId = self.__ask_parent_for_unique_id(
                uniqueIdType=QueueMessageType.CHILD_REQUEST_CLOSE_CHUNK_DOC_ID)

        # Current Close Chunks
        self.__create_new_chunk_doc(chunkType=ChunkType.CLOSE_CHUNK,
                boundsType=ChunkManifestBoundsType.NORMAL, 
                chunkId=closeChunkId)

        specialCloseChunkId = self.__ask_parent_for_unique_id(
                uniqueIdType=QueueMessageType.CHILD_REQUEST_CLOSE_CHUNK_DOC_ID)

        self.__create_new_chunk_doc(chunkType=ChunkType.CLOSE_CHUNK,
                boundsType=ChunkManifestBoundsType.SPECIAL, 
                chunkId=specialCloseChunkId)

def child_process(queueContainer: QueueContainer):
    childProcess = ChildProcess(queueContainer=queueContainer)
    
    # Send the parent our pid
    childProcess.get_to_parent_queue().put(QueueMessage(
            msgType=QueueMessageType.CHILD_SEND_PID,
            msgAction=QueueMessageRequiredAction.REQUIRES_ACKNOWLEDGE,
            msgFrom=childProcess.get_pid()))

    # Wait for ACK from the parent
    response: QueueMessage = childProcess.get_from_parent_queue().get(
            block=True)

    if response.get_message_type() != QueueMessageType.ACK:
        print("PID {}: Expected an ACK".format(os.getpid()))
        return
    else:
        # Begin the child processes main loop
        childProcess.main_loop()

# * MAIN PROCESS ---------------------------------------------------------------

class MainProcess(object):
    def __init__(self):

        self.db = firestore.Client()

        self.transaction = self.db.transaction()

        self.chunksRef = self.db.collection(u'chunks') # type: ignore

        self.__classname__ = 'MainProcess'

        self.__ppid = os.getpid()

        self.__farChunkIdCounter = 0
        self.__standardChunkIdCounter = 0
        self.__nearChunkIdCounter = 0
        self.__closeChunkIdCounter = 0

        # Get the current time
        fth = FirestoreTimeHelper()
        currentTime = fth.get_unix_epoch_timestamp_ms()

        self.returnCodeHandler = {
            QueueMessageType.CHILD_REQUEST_FAR_CHUNK_ID: self.\
                    __handle_child_request_chunk_id,
            QueueMessageType.CHILD_REQUEST_STANDARD_CHUNK_ID: self.\
                    __handle_child_request_chunk_id,
            QueueMessageType.CHILD_REQUEST_NEAR_CHUNK_ID: self.\
                    __handle_child_request_chunk_id,
            QueueMessageType.CHILD_REQUEST_CLOSE_CHUNK_ID: self.\
                    __handle_child_request_chunk_id,

            QueueMessageType.CHILD_REQUEST_FAR_CHUNK_DOC_ID: self.\
                    __handle_child_request_chunk_doc_id,
            QueueMessageType.CHILD_REQUEST_STANDARD_CHUNK_DOC_ID: self.\
                    __handle_child_request_chunk_doc_id,
            QueueMessageType.CHILD_REQUEST_NEAR_CHUNK_DOC_ID: self.\
                    __handle_child_request_chunk_doc_id,
            QueueMessageType.CHILD_REQUEST_CLOSE_CHUNK_DOC_ID: self.\
                    __handle_child_request_chunk_doc_id,

            QueueMessageType.CHILD_SEND_FAR_CHUNK_DOC: self.\
                    __handle_child_send_chunk_doc,
            QueueMessageType.CHILD_SEND_STANDARD_CHUNK_DOC: self.\
                    __handle_child_send_chunk_doc,
            QueueMessageType.CHILD_SEND_NEAR_CHUNK_DOC: self.\
                    __handle_child_send_chunk_doc,
            QueueMessageType.CHILD_SEND_CLOSE_CHUNK_DOC: self.\
                    __handle_child_send_chunk_doc,

            QueueMessageType.CHILD_WAITING_FOR_ORDERS: self.\
                    __handle_child_waiting_for_orders,

            QueueMessageType.CHILD_SEND_READY_TO_JOIN: self.\
                    __handle_child_ready_to_join_request
        }

        # Initialise each of the manifests
        if not self.__init_chunk_manifests(currentTime=currentTime):
            return

        # Initialise each of the buffers
        if not self.__init_chunk_buffers():
            return

        # Initialise the current chunk docs
        if not self.__init_current_chunk_docs(currentTime=currentTime,
                farChunksIdsList=self.__farChunksIdList,
                specialFarChunksIdsList=self.__specialFarChunksIdList,
                standardChunksIdsList=self.__standardChunksIdList,
                specialStandardChunksIdsList=self.__specialStandardChunksIdList,
                nearChunksIdsList=self.__nearChunksIdList,
                specialNearChunksIdsList=self.__specialNearChunksIdList,
                closeChunksIdsList=self.__closeChunksIdList,
                specialCloseChunksIdsList=self.__specialCloseChunksIdList):
            return
        
        # Initialise the icosahedron triangle data
        if not self.__init_icosahedron_data():
            return

        # Initialise parallel computing data
        if not self.__init_parallel_computing_data():
            return

        # Initialise all the chunk ids
        if not self.__initialise_all_chunk_ids():
            return 

        # Begin the main loop
        barSize = self.__icoCounterMax * 16 * 16 * 16
        with alive_bar(barSize) as bar:
            self.__bar = bar
            print("Main loop commencing")
            self.__main_loop()

    # * PUBLIC METHODS ---------------------------------------------------------

    # * PRIVATE METHODS --------------------------------------------------------

    def __main_loop(self):
        """ Main loop for the parent process. """
        
        while True:
             # Wait for a message from a child process
            message: QueueMessage = self.__fromChildToParentQueue.\
                    get(block=True)

            # Determine what the child message is
            try:
                result = self.returnCodeHandler[message.get_message_type()](message)

                if not result:
                    print("breaking after message: {}".format(message.get_message_type()))
                    break
            except Exception as e:
                exc_type, exc_obj, exc_tb = sys.exc_info()
                feh = FirestoreErrorHelper()
                feh.class_error_handler(fileName=__filename__,
                        className=self.__classname__,
                        methodName='__main_loop', 
                        exceptionMsg=e,
                        lineNum=exc_tb.tb_lineno)   # type: ignore
                sys.exit()
            
        self.__clean_up_main_process()

    def __initialise_all_chunk_ids(self):
        """ Initialises all 1.3 million+ chunk ids """
        buffer = 10
        # Generate all the FAR chunk ids
        barSize = self.__icoCounterMax
        with alive_bar(barSize) as bar1:
            print("Initialising Far Chunk ids")
            counter = 0
            while counter < barSize:
                self.__farChunkIds.append(
                        self.__generate_new_chunk_id(
                                chunkType=ChunkType.FAR_CHUNK))
                counter += 1
                bar1()
        
        barSize = self.__icoCounterMax * 16
        with alive_bar(barSize) as bar2:
            print("Initialising Standard Chunk ids")
            counter = 0
            while counter < barSize:
                self.__standardChunkIds.append(self.__generate_new_chunk_id(
                        chunkType=ChunkType.STANDARD_CHUNK))
                counter += 1
                bar2()
        
        barSize = self.__icoCounterMax * 16 * 16
        with alive_bar(barSize) as bar3:
            print("Initialising Near Chunk ids")
            counter = 0
            while counter < barSize:
                self.__nearChunkIds.append(self.__generate_new_chunk_id(
                        chunkType=ChunkType.NEAR_CHUNK))
                counter += 1
                bar3()

        barSize = self.__icoCounterMax * 16 * 16 * 16
        with alive_bar(barSize) as bar4:
            print("Initialising Close Chunk ids")
            counter = 0
            while counter < barSize:
                self.__closeChunkIds.append(self.__generate_new_chunk_id(
                        chunkType=ChunkType.CLOSE_CHUNK))
                counter += 1
                bar4()
                
        return True

    def __clean_up_main_process(self):
        """ Cleans up the main process, saves the current manifests. """
        print("Cleaning up")
        # Far chunks
        self.__save_chunk_doc_manifest(
                chunkType=ChunkType.FAR_CHUNK,
                boundsType=ChunkManifestBoundsType.NORMAL)
        self.__save_chunk_doc_manifest(
                chunkType=ChunkType.FAR_CHUNK,
                boundsType=ChunkManifestBoundsType.SPECIAL)
        
        # Standard chunks
        self.__save_chunk_doc_manifest(
                chunkType=ChunkType.STANDARD_CHUNK,
                boundsType=ChunkManifestBoundsType.NORMAL)
        self.__save_chunk_doc_manifest(
                chunkType=ChunkType.STANDARD_CHUNK,
                boundsType=ChunkManifestBoundsType.SPECIAL)
        
        # Near chunks
        self.__save_chunk_doc_manifest(
                chunkType=ChunkType.NEAR_CHUNK,
                boundsType=ChunkManifestBoundsType.NORMAL)
        self.__save_chunk_doc_manifest(
                chunkType=ChunkType.NEAR_CHUNK,
                boundsType=ChunkManifestBoundsType.SPECIAL)

        # Close chunk
        self.__save_chunk_doc_manifest(
                chunkType=ChunkType.CLOSE_CHUNK,
                boundsType=ChunkManifestBoundsType.NORMAL)
        self.__save_chunk_doc_manifest(
                chunkType=ChunkType.CLOSE_CHUNK,
                boundsType=ChunkManifestBoundsType.SPECIAL)

    def __save_chunk_doc_manifest(self, chunkType: ChunkType, 
            boundsType: ChunkManifestBoundsType):
        if chunkType == ChunkType.FAR_CHUNK:
            if boundsType == ChunkManifestBoundsType.NORMAL:
                result = _save_chunk_manifest_to_firestore(
                        transaction=self.transaction,
                        chunkManifestDoc=self.__farChunksManifest,
                        chunksDocRoot=self.chunksRef)
            else:
                result = _save_chunk_manifest_to_firestore(
                        transaction=self.transaction,
                        chunkManifestDoc=self.__specialFarChunksManifest,
                        chunksDocRoot=self.chunksRef)
        elif chunkType == ChunkType.STANDARD_CHUNK:
            if boundsType == ChunkManifestBoundsType.NORMAL:
                result = _save_chunk_manifest_to_firestore(
                        transaction=self.transaction,
                        chunkManifestDoc=self.__standardChunksManifest,
                        chunksDocRoot=self.chunksRef)
            else:
                result = _save_chunk_manifest_to_firestore(
                        transaction=self.transaction,
                        chunkManifestDoc=self.__specialStandardChunksManifest,
                        chunksDocRoot=self.chunksRef)
        elif chunkType == ChunkType.NEAR_CHUNK:
            if boundsType == ChunkManifestBoundsType.NORMAL:
                result = _save_chunk_manifest_to_firestore(
                        transaction=self.transaction,
                        chunkManifestDoc=self.__nearChunksManifest,
                        chunksDocRoot=self.chunksRef)
            else:
                result = _save_chunk_manifest_to_firestore(
                        transaction=self.transaction,
                        chunkManifestDoc=self.__specialNearChunksManifest,
                        chunksDocRoot=self.chunksRef)
        else:
            if boundsType == ChunkManifestBoundsType.NORMAL:
                result = _save_chunk_manifest_to_firestore(
                        transaction=self.transaction,
                        chunkManifestDoc=self.__closeChunksManifest,
                        chunksDocRoot=self.chunksRef)
            else:
                result = _save_chunk_manifest_to_firestore(
                        transaction=self.transaction,
                        chunkManifestDoc=self.__specialCloseChunksManifest,
                        chunksDocRoot=self.chunksRef)
        
        if result != GoogleMapsTrafficHelperTransactionStatus.\
                TRANSACTION_SUCCESS:
            raise Exception("Error trying to save manifest to firestore")

    def __handle_child_ready_to_join_request(self, message: QueueMessage):
        """ Handles the terminating of a child processes from the registrar. """
        processToJoin = message.get_message_from()
        
        # Join the process
        self.__childProcessesDict[processToJoin].get_process_handle().join()
        
        # Remove the process from the child processes dict
        result = self.__childProcessesDict.pop(processToJoin, None)

        if result != None:
            if len(self.__childProcessesDict) != 0:
                # More processes are waiting to be shut down
                return True
            else:
                # No more processes left, shut down the main process
                return False
        else:
            raise Exception("Child process {} does not exist!".format(
                    processToJoin))
    
    def __handle_child_request_chunk_id(self, message: QueueMessage):
        """ Handles the request for a chunk id from a child process. 
        
        NOTE
        - requests are ordered from most common to least common to improve 
                efficiency.
        """
        if message.get_message_type() == QueueMessageType\
                .CHILD_REQUEST_CLOSE_CHUNK_ID:
            result = self.__closeChunkIds.pop()
        elif message.get_message_type() == QueueMessageType\
                .CHILD_REQUEST_NEAR_CHUNK_ID:
            result = self.__nearChunkIds.pop()
        elif message.get_message_type() == QueueMessageType\
                .CHILD_REQUEST_STANDARD_CHUNK_ID:
            result = self.__standardChunkIds.pop()
        elif message.get_message_type() == QueueMessageType\
                .CHILD_REQUEST_FAR_CHUNK_ID:
            result = self.__farChunkIds.pop()
        else:
            raise Exception("Unknown message type for chunk id handler: {}"\
                    .format(message.get_message_type().name))

        if not isinstance(result, str):
            raise Exception("Error trying to generate new chunk id: {}"\
                    .format(result))

        # Send an acknowledgement back to the child (along with the new id)
        self.__childProcessesDict[message.get_message_from()]\
                .get_to_child_queue().put(
                        QueueMessage(msgType=QueueMessageType.ACK,
                                msgAction=QueueMessageRequiredAction\
                                        .NO_RESPONSE_REQUIRED,
                                msgData=result,
                                msgFrom=self.__ppid))

        self.__bar()
        return True

    def __handle_child_request_chunk_doc_id(self, message: QueueMessage):
        """ Handles the request for a chunk doc id from a child process. 
        
        NOTE
        - requests are ordered from most common to least common to improve 
                efficiency.
        """
        if message.get_message_type() == QueueMessageType\
                .CHILD_REQUEST_CLOSE_CHUNK_DOC_ID:
            result = self.__generate_new_chunk_doc_id(
                    chunkType=ChunkType.CLOSE_CHUNK)
        elif message.get_message_type() == QueueMessageType\
                .CHILD_REQUEST_NEAR_CHUNK_DOC_ID:
            result = self.__generate_new_chunk_doc_id(
                    chunkType=ChunkType.NEAR_CHUNK)
        elif message.get_message_type() == QueueMessageType\
                .CHILD_REQUEST_STANDARD_CHUNK_DOC_ID:
            result = self.__generate_new_chunk_doc_id(
                    chunkType=ChunkType.STANDARD_CHUNK)
        elif message.get_message_type() == QueueMessageType\
                .CHILD_REQUEST_FAR_CHUNK_DOC_ID:
            result = self.__generate_new_chunk_doc_id(
                    chunkType=ChunkType.FAR_CHUNK)
        else:
            raise Exception("Unknown message type for chunk doc id handler: {}"\
                    .format(message.get_message_type().name))

        if not isinstance(result, str):
            raise Exception("Error trying to generate new chunk doc id: {}"\
                    .format(result))

        # Send an acknowledgement back to the child (along with the new id)
        self.__childProcessesDict[message.get_message_from()]\
                .get_to_child_queue().put(
                        QueueMessage(msgType=QueueMessageType.ACK,
                                msgAction=QueueMessageRequiredAction\
                                        .NO_RESPONSE_REQUIRED,
                                msgData=result,
                                msgFrom=self.__ppid))

        return True

    def __handle_child_send_chunk_doc(self, message: QueueMessage):
        """ Handles the child process sending a completed chunk doc to parent. 
        
        I.e. the child has sent a chunk doc to the parent, and thus the parent
            should take the appropriate variables and use them to generate a
            chunk manifest doc.
        """
        msgData = message.get_message_data()
        if not isinstance(msgData, ChunkDoc):
            raise Exception("Expected Chunk doc, recieved: {}".format(type(msgData)))

        newChunkManifest = ChunkManifestIndex(
            chunkGroupBoundingBox=msgData.get_bounds(),
            currentCapacity=msgData.get_current_capacity(),
            docId=msgData.get_owner_id(),
            numChunks=msgData.get_num_chunks())

        chunkType = msgData.get_chunk_type()

        if chunkType == ChunkType.CLOSE_CHUNK:
            if msgData.get_bounds_type() == ChunkManifestBoundsType.NORMAL:
                self.__closeChunksManifest.add_chunk_manifest(
                        newChunkManifest)
            else:
                self.__specialCloseChunksManifest.add_chunk_manifest(
                        newChunkManifest)
        elif chunkType == ChunkType.NEAR_CHUNK:
            if msgData.get_bounds_type() == ChunkManifestBoundsType.NORMAL:
                self.__nearChunksManifest.add_chunk_manifest(
                        newChunkManifest)
            else:
                self.__specialNearChunksManifest.add_chunk_manifest(
                        newChunkManifest)
        elif chunkType == ChunkType.STANDARD_CHUNK:
            if msgData.get_bounds_type() == ChunkManifestBoundsType.NORMAL:
                self.__standardChunksManifest.add_chunk_manifest(
                        newChunkManifest)
            else:
                self.__specialStandardChunksManifest.add_chunk_manifest(
                        newChunkManifest)
        else:
            if msgData.get_bounds_type() == ChunkManifestBoundsType.NORMAL:
                self.__farChunksManifest.add_chunk_manifest(
                        newChunkManifest)
            else:
                self.__specialFarChunksManifest.add_chunk_manifest(
                        newChunkManifest)

        # Acknowledge to the child that we received the chunk doc
        self.__childProcessesDict[message.get_message_from()]\
                .get_to_child_queue().put(
                        QueueMessage(msgType=QueueMessageType.ACK,
                                msgAction=QueueMessageRequiredAction\
                                        .NO_RESPONSE_REQUIRED,
                                msgFrom=self.__ppid))

        return True

    def __handle_child_waiting_for_orders(self, message: QueueMessage):
        """ Handles the child process asking for more work.
        """
        # Determine if any more triangles need processing.
        if self.__icoCounter < self.__icoCounterMax:
            self.__childProcessesDict[message.get_message_from()]\
                .get_to_child_queue().put(
                        QueueMessage(msgType=QueueMessageType.PARENT_ORDER_CHILD_PROCESS_TRIANGLE,
                                msgAction=QueueMessageRequiredAction\
                                        .REQUIRES_ACKNOWLEDGE,
                                msgData=self.__icoTriangles[self.__icoCounter],
                                msgFrom=self.__ppid))

            # Increment the counter
            self.__icoCounter += 1
        else:
            # Tell the waiting process to terminate
            self.__childProcessesDict[message.get_message_from()]\
                .get_to_child_queue().put(
                        QueueMessage(msgType=QueueMessageType.PARENT_ORDER_CHILD_TERMINATE,
                                msgAction=QueueMessageRequiredAction\
                                        .NO_RESPONSE_REQUIRED,
                                msgFrom=self.__ppid))
            
            # Child process will begin to send the remaining unsaved firestore
            # documents to parent. Once finished, the child will send a 
            #   `JOIN_REQUEST`
        return True
    
    def __generate_new_chunk_id(self, chunkType: ChunkType):
        """ Generates a UNIQUE chunk id. """

         # Specify which chunk we're trying to create
        if chunkType == ChunkType.FAR_CHUNK:
            chunkId = 'fch{}'.format(self.__farChunkIdCounter)
            self.__farChunkIdCounter += 1
            
        elif chunkType == ChunkType.STANDARD_CHUNK:
            chunkId = 'sch{}'.format(self.__standardChunkIdCounter)
            self.__standardChunkIdCounter += 1
        elif chunkType == ChunkType.NEAR_CHUNK:
            chunkId = 'nch{}'.format(self.__nearChunkIdCounter)
            self.__nearChunkIdCounter += 1
        else:
            chunkId = 'cch{}'.format(self.__closeChunkIdCounter)
            self.__closeChunkIdCounter += 1
        
        return chunkId

    def __generate_new_chunk_doc_id(self, chunkType: ChunkType):
        """ Generates a UNIQUE chunk doc id. """

        counter = 0
        fig = FirestoreIdGenerator()

         # Specify which chunk we're trying to create
        if chunkType == ChunkType.CLOSE_CHUNK:
            generator = fig.generate_close_chunk_doc_id
            chunkIdList = self.__closeChunksIdList
        elif chunkType == ChunkType.NEAR_CHUNK:
            generator = fig.generate_near_chunk_doc_id
            chunkIdList = self.__nearChunksIdList
        elif chunkType == ChunkType.STANDARD_CHUNK:
            generator = fig.generate_standard_chunk_doc_id
            chunkIdList = self.__standardChunksIdList
        else:
            generator = fig.generate_far_chunk_doc_id
            chunkIdList = self.__farChunksIdList
            

        try:
            while True:
                chunkId = generator()
                # Check if id exists
                if chunkId not in chunkIdList:
                    chunkIdList.append(chunkId)
                    return chunkId
                counter += 1
                if counter >= 100:
                    raise Exception("Id generator exhausted")
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.class_error_handler(fileName=__filename__,
                    className=self.__classname__,
                    methodName='__generate_new_chunk_doc_id', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            return False

    def __init_icosahedron_data(self):
        """ Initialises the icosahedron with 81920 faces (FAR) """
        # Generate a standard, 20 face icosahedron
        ico = Icosahedron()

        # Tessellate four times to get the 5120 far chunks
        ico.tessellate_triangles()
        ico.tessellate_triangles()
        ico.tessellate_triangles()
        ico.tessellate_triangles()
        

        # Get the triangles that are over australia.
        ico.project_triangles_onto_sphere()

        tempTriangles = ico.get_triangles()

        ausViewport = ChunkBoundingBox(
                topRightLat=-26.8077,
                topRightLng=153.8716,
                bottomLeftLat=-28.2689,
                bottomLeftLng=151.6743)
        bbh = BoundingBoxHelper()
        secondStageTrianglesList: List[Triangle] = []
        for triangle in tempTriangles:
            cartCoords = triangle.get_cartographic_coords()
            newChunk = Chunk(
                lat1=cartCoords[0][0],
                lat2=cartCoords[1][0],
                lat3=cartCoords[2][0],
                lng1=cartCoords[0][1],
                lng2=cartCoords[1][1],
                lng3=cartCoords[2][1],
                intersectionsIdsList=[],
                chunkId="")

            if bbh.check_bounding_box_in_view(viewportBounds=ausViewport, 
                    chunkBoundingBox=newChunk.get_bounding_box()):
                secondStageTrianglesList.append(triangle)

        # Tessellate each of the triangles two more times
        outputTrianglesList: List[Triangle] = []
        for triangle in secondStageTrianglesList:
            newTriangles = triangle.tessellate("","","")
            for tri in newTriangles:
                temp = tri.tessellate("","","")
                for t in temp:
                    cartCoords = t.get_cartographic_coords()
                    newChunk = Chunk(
                        lat1=cartCoords[0][0],
                        lat2=cartCoords[1][0],
                        lat3=cartCoords[2][0],
                        lng1=cartCoords[0][1],
                        lng2=cartCoords[1][1],
                        lng3=cartCoords[2][1],
                        intersectionsIdsList=[],
                        chunkId="")
                    if bbh.check_bounding_box_in_view(
                            viewportBounds=ausViewport, 
                            chunkBoundingBox=newChunk.get_bounding_box()):
                        outputTrianglesList.append(t)
        self.__icoTriangles = outputTrianglesList
        
        # Initialise the counters
        self.__icoCounterMax = len(self.__icoTriangles)
        self.__icoCounter = 0

        return True
        

    def __init_parallel_computing_data(self):
        """ Initialises the variables required to begin parallel computing. """
        try:
            # Get the number of CPUs we have
            self.__cpuCount = os.cpu_count()
            
            if self.__cpuCount == None:
                raise Exception("No CPU detected (oh no)")

            self.__childProcessesDict: Dict[int, ChildProcessDataTracker] = {}

            # Initialise the parent queue (All processes will push to the parent
            #   queue, leaving the parent to wait for requests from its children)
            self.__fromChildToParentQueue: Queue = Queue()

            # Create cpuCount number of child processes
            for i in range(0, self.__cpuCount):
                fromParentToChildQueue = Queue()

                # Order reversed for the parent (makes more sense from the childs
                #   perspective)
                newChildQueueContainer = QueueContainer(
                        fromProcessQueue=fromParentToChildQueue,
                        toProcessQueue=self.__fromChildToParentQueue)
                
                p = Process(target=child_process, args=(newChildQueueContainer,))
                
                # Start the child process.
                p.start()
                
                temp = ChildProcessDataTracker(
                        toChildQueue=fromParentToChildQueue,
                        process=p)

                # Wait for each child process to send over their pid
                message: QueueMessage = self.__fromChildToParentQueue.\
                            get(block=True)

                if message.get_message_type() != QueueMessageType.CHILD_SEND_PID:
                    raise Exception("Got unexpected response {}".format(
                            message.get_message_type()))

                self.__childProcessesDict[message.get_message_from()] = temp

            # Awesome, we got all child process pids, lets acknowledge all of them
            for child in self.__childProcessesDict:
                self.__childProcessesDict[child].get_to_child_queue().put(
                        QueueMessage(
                                msgType=QueueMessageType.ACK,
                                msgAction=QueueMessageRequiredAction.\
                                        NO_RESPONSE_REQUIRED,
                                msgFrom=self.__ppid))

            return True
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.class_error_handler(fileName=__filename__,
                    className=self.__classname__,
                    methodName='__init_current_chunk_docs', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            return False

        
    def __init_current_chunk_docs(self, currentTime: int,
            farChunksIdsList: List[str],
            specialFarChunksIdsList: List[str],
            standardChunksIdsList: List[str],
            specialStandardChunksIdsList: List[str],
            nearChunksIdsList: List[str],
            specialNearChunksIdsList: List[str],
            closeChunksIdsList: List[str],
            specialCloseChunksIdsList: List[str]):
        try:
            # Current Far chunks
            self.__currentFarChunkDoc = ChunkDoc(
                chunkType=ChunkType.FAR_CHUNK,
                chunks=[],
                createTime=currentTime,
                docType=BaseDocType.FAR_CHUNK_DOC,
                lastEditted=currentTime,
                boundsType=ChunkManifestBoundsType.NORMAL,
                major=0,
                minor=3,
                patch=0,
                ownerId=farChunksIdsList[0],
                bounds=ChunkBoundingBox(0,0,0,0))

            self.__currentSpecialFarChunkDoc = ChunkDoc(
                chunkType=ChunkType.FAR_CHUNK,
                chunks=[],
                createTime=currentTime,
                docType=BaseDocType.FAR_CHUNK_DOC,
                lastEditted=currentTime,
                boundsType=ChunkManifestBoundsType.SPECIAL,
                major=0,
                minor=3,
                patch=0,
                ownerId=specialFarChunksIdsList[0],
                bounds=ChunkBoundingBox(0,0,0,0))

            # Current Standard chunks
            self.__currentStandardChunkDoc = ChunkDoc(
                chunkType=ChunkType.STANDARD_CHUNK,
                chunks=[],
                createTime=currentTime,
                docType=BaseDocType.STANDARD_CHUNK_DOC,
                lastEditted=currentTime,
                boundsType=ChunkManifestBoundsType.NORMAL,
                major=0,
                minor=3,
                patch=0,
                ownerId=standardChunksIdsList[0],
                bounds=ChunkBoundingBox(0,0,0,0))

            self.__currentSpecialStandardChunkDoc = ChunkDoc(
                chunkType=ChunkType.STANDARD_CHUNK,
                chunks=[],
                createTime=currentTime,
                docType=BaseDocType.STANDARD_CHUNK_DOC,
                lastEditted=currentTime,
                boundsType=ChunkManifestBoundsType.SPECIAL,
                major=0,
                minor=3,
                patch=0,
                ownerId=specialStandardChunksIdsList[0],
                bounds=ChunkBoundingBox(0,0,0,0))

            # Current Near Chunks
            self.__currentNearChunkDoc = ChunkDoc(
                chunkType=ChunkType.NEAR_CHUNK,
                chunks=[],
                createTime=currentTime,
                docType=BaseDocType.NEAR_CHUNK_DOC,
                lastEditted=currentTime,
                boundsType=ChunkManifestBoundsType.NORMAL,
                major=0,
                minor=3,
                patch=0,
                ownerId=nearChunksIdsList[0],
                bounds=ChunkBoundingBox(0,0,0,0))

            self.__currentSpecialNearChunkDoc = ChunkDoc(
                chunkType=ChunkType.NEAR_CHUNK,
                chunks=[],
                createTime=currentTime,
                docType=BaseDocType.NEAR_CHUNK_DOC,
                lastEditted=currentTime,
                boundsType=ChunkManifestBoundsType.SPECIAL,
                major=0,
                minor=3,
                patch=0,
                ownerId=specialNearChunksIdsList[0],
                bounds=ChunkBoundingBox(0,0,0,0))

            # Current Close Chunks
            self.__currentCloseChunkDoc = ChunkDoc(
                chunkType=ChunkType.CLOSE_CHUNK,
                chunks=[],
                createTime=currentTime,
                docType=BaseDocType.CLOSE_CHUNK_DOC,
                lastEditted=currentTime,
                boundsType=ChunkManifestBoundsType.NORMAL,
                major=0,
                minor=3,
                patch=0,
                ownerId=closeChunksIdsList[0],
                bounds=ChunkBoundingBox(0,0,0,0))

            self.__currentSpecialCloseChunkDoc = ChunkDoc(
                chunkType=ChunkType.CLOSE_CHUNK,
                chunks=[],
                createTime=currentTime,
                docType=BaseDocType.CLOSE_CHUNK_DOC,
                lastEditted=currentTime,
                boundsType=ChunkManifestBoundsType.SPECIAL,
                major=0,
                minor=3,
                patch=0,
                ownerId=specialCloseChunksIdsList[0],
                bounds=ChunkBoundingBox(0,0,0,0))
            return True
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.class_error_handler(fileName=__filename__,
                    className=self.__classname__,
                    methodName='__init_current_chunk_docs', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            return False

    def __init_chunk_buffers(self):
        """ Initialises the various buffers used by the main process. """
        fig = FirestoreIdGenerator()
        
        try:
            self.__farChunksIdList: List[str] = [
                    fig.generate_far_chunk_doc_id()]
            self.__specialFarChunksIdList: List[str] = [
                    fig.generate_far_chunk_id()]
            self.__farChunkIds: List[str] = [] 

            self.__standardChunksIdList: List[str] = [
                    fig.generate_standard_chunk_doc_id()]
            self.__specialStandardChunksIdList: List[str] = [
                    fig.generate_standard_chunk_id()]
            self.__standardChunkIds: List[str] = []
            
            self.__nearChunksIdList: List[str] = [
                    fig.generate_near_chunk_doc_id()]
            self.__specialNearChunksIdList: List[str] = [
                    fig.generate_near_chunk_id()]
            self.__nearChunkIds: List[str] = []


            self.__closeChunksIdList: List[str] = [
                    fig.generate_close_chunk_doc_id()]
            self.__specialCloseChunksIdList: List[str] = [
                    fig.generate_close_chunk_id()]
            self.__closeChunkIds: List[str] = []
            return True
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.class_error_handler(fileName=__filename__,
                    className=self.__classname__,
                    methodName='__init_chunk_buffers', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            return False

    def __init_chunk_manifests(self, currentTime: int):
        """ Initialises the various chunk manifests. 
        
        Chunk manifests keep track of the various chunk docs and their bounds.
        """
        
        
        try:
            # Check if the database already exists
            querySnapshot = self.chunksRef.limit(1).get()
            if len(querySnapshot) != 0: # type: ignore
                # A database already exists.
                raise Exception("Database already exists")

            self.__farChunksManifest = ChunkManifestDoc(
                chunkManifests=[],
                createTime=currentTime,
                docType=BaseDocType.FAR_CHUNK_MANIFEST,
                lastEditted=currentTime,
                major=0,
                minor=3,
                patch=0,
                ownerId='far_chunk_manifest'
            )

            self.__specialFarChunksManifest = ChunkManifestDoc(
                chunkManifests=[],
                createTime=currentTime,
                docType=BaseDocType.FAR_CHUNK_MANIFEST,
                lastEditted=currentTime,
                major=0,
                minor=3,
                patch=0,
                ownerId='special_far_chunk_manifest'
            )

            self.__standardChunksManifest = ChunkManifestDoc(
                chunkManifests=[],
                createTime=currentTime,
                docType=BaseDocType.STANDARD_CHUNK_MANIFEST,
                lastEditted=currentTime,
                major=0,
                minor=3,
                patch=0,
                ownerId='standard_chunk_manifest'
            )

            self.__specialStandardChunksManifest = ChunkManifestDoc(
                chunkManifests=[],
                createTime=currentTime,
                docType=BaseDocType.STANDARD_CHUNK_MANIFEST,
                lastEditted=currentTime,
                major=0,
                minor=3,
                patch=0,
                ownerId='special_standard_chunk_manifest'
            )

            self.__nearChunksManifest = ChunkManifestDoc(
                chunkManifests=[],
                createTime=currentTime,
                docType=BaseDocType.NEAR_CHUNK_MANIFEST,
                lastEditted=currentTime,
                major=0,
                minor=3,
                patch=0,
                ownerId='near_chunk_manifest'
            )

            self.__specialNearChunksManifest = ChunkManifestDoc(
                chunkManifests=[],
                createTime=currentTime,
                docType=BaseDocType.NEAR_CHUNK_MANIFEST,
                lastEditted=currentTime,
                major=0,
                minor=3,
                patch=0,
                ownerId='special_near_chunk_manifest'
            )

            self.__closeChunksManifest = ChunkManifestDoc(
                chunkManifests=[],
                createTime=currentTime,
                docType=BaseDocType.CLOSE_CHUNK_MANIFEST,
                lastEditted=currentTime,
                major=0,
                minor=3,
                patch=0,
                ownerId='close_chunk_manifest'
            )

            self.__specialCloseChunksManifest = ChunkManifestDoc(
                chunkManifests=[],
                createTime=currentTime,
                docType=BaseDocType.CLOSE_CHUNK_MANIFEST,
                lastEditted=currentTime,
                major=0,
                minor=3,
                patch=0,
                ownerId='special_close_chunk_manifest')
            return True
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.class_error_handler(fileName=__filename__,
                    className=self.__classname__,
                    methodName='__init_chunk_manifests', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            return False
    

def main():
    # Lets go baby
    
    mainProc = MainProcess()
    

# * TRANSACTIONS ---------------------------------------------------------------

@firestore.transactional
def _save_chunk_to_firestore(
        transaction: firestore.Transaction,
        chunksDocRoot: firestore.CollectionReference,
        chunkManifestDocId: str,
        chunkDoc: ChunkDoc):
    """  """
    try:

        chunkDocRef = chunksDocRoot.document(chunkManifestDocId)\
                .collection('chunk_data').document(chunkDoc.get_owner_id())

        transaction.set(chunkDocRef, chunkDoc.to_dict())

        return GoogleMapsTrafficHelperTransactionStatus.TRANSACTION_SUCCESS
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        feh = FirestoreErrorHelper()
        feh.transaction_error_handler(fileName=__filename__, 
                methodName='_save_chunk_to_firestore', 
                exceptionMsg=e,
                lineNum=exc_tb.tb_lineno)   # type: ignore
        return GoogleMapsTrafficHelperTransactionStatus.TRANSACTION_ERROR

@firestore.transactional
def _save_chunk_manifest_to_firestore(
        transaction: firestore.Transaction,
        chunksDocRoot: firestore.CollectionReference,
        chunkManifestDoc: ChunkManifestDoc):
    """  """
    try:
        chunkManifestRef = chunksDocRoot.document(
                chunkManifestDoc.get_owner_id())
        
        transaction.set(chunkManifestRef, chunkManifestDoc.to_dict())
        
        return GoogleMapsTrafficHelperTransactionStatus.TRANSACTION_SUCCESS
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        feh = FirestoreErrorHelper()
        feh.transaction_error_handler(fileName=__filename__, 
                methodName='_save_chunk_manifest_to_firestore', 
                exceptionMsg=e,
                lineNum=exc_tb.tb_lineno)   # type: ignore
        return GoogleMapsTrafficHelperTransactionStatus.TRANSACTION_ERROR

if __name__ == '__main__':
    main()