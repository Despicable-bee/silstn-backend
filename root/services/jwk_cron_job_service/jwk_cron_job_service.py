"""
	FILENAME:		jwk_cron_job_service.py

	AUTHOR(s):		

	DESCRIPTION:		

	VERSION:		0.0.1

	DESTINATION:		

	LICENSE:		MIT
"""
# Standard Library
from concurrent import futures
import logging
import os
from typing import Text
import json
import sys

# Third Party Library
import grpc
from jwcrypto import jwk

# Local Library
from root.services.jwk_cron_job_service.generated import jwk_cron_job_service_pb2_grpc as pb2_grpc
from root.services.jwk_cron_job_service.generated import jwk_cron_job_service_pb2 as pb2

from root.helpers.jwt_helper.jwt_helper import JwkStorageHelper

from root.helpers.common_helper.common_firestore_helper import \
		FirestoreErrorHelper

_PORT = os.environ["PORT"]

# * DEBUG ----------------------------------------------------------------------

__filename__ = 'jwk_cron_job_service.py'

# * MAIN CLASS -----------------------------------------------------------------

class JwkCronJobService(pb2_grpc.JwkCronJobServiceServicer):
	
	def generate_key_pair(self,
			request: pb2.JwkCronJobService_Request,
			context: grpc.ServicerContext) -> pb2.JwkCronJobService_Response:
		""" Generates a new RSA keypair and saves it to google cloud storage.

		Keys need to be rotated regularly to ensure the platforms security can 
		keep up with malicious efforts to crack (guess) our secret key.

		ARGS:
		- request.public_exponent: A (relatively) prime number for all primes p
				which divides the modulus
		- request.size: The number of bytes of the key.
		
		"""
		try:
			key = jwk.JWK.generate(kty='RSA',
					public_exponent=request.public_exponent,	# type: ignore
					size=request.size)							# type: ignore
			# Generate the kid (note this will NOT work in 2100)
			kid = 'leander-mediterranean'
			alg = 'RS256'

			publicKey = json.loads(key.export_public())	# type: ignore

			# Add the relevant information to the the 
			publicKey['alg'] = alg
			publicKey['kid'] = kid
			publicKey = str({'keys': [publicKey]}).replace("'", '"')

			privateKey = json.loads(key.export_private()) # type: ignore
			# Add the relevant information to the
			privateKey['alg'] = alg
			privateKey['kid'] = kid
			privateKey = str(privateKey).replace("'",'"')

			# Package the keys into their appropriate JSON form
			jwksh = JwkStorageHelper()
			result = jwksh.upload_jwks(publicKey, privateKey)
			
			if not result:
				raise Exception("Something went wrong uploading jwks")
			
			return pb2.JwkCronJobService_Response(
					status=int(pb2.SUCCESSFUL))
		except Exception as e:
			exc_type, exc_obj, exc_tb = sys.exc_info()
			feh = FirestoreErrorHelper()
			feh.api_error_handler(fileName=__filename__, 
					methodName='generate_key_pair', 
					exceptionMsg=e,
					lineNum=exc_tb.tb_lineno)   # type: ignore
			return pb2.JwkCronJobService_Response(
					status=int(pb2.ERROR))
	

def _serve(port: Text):
	bind_address = f"[::]:{port}"
	server = grpc.server(futures.ThreadPoolExecutor())
	pb2_grpc.add_JwkCronJobServiceServicer_to_server(JwkCronJobService(), server)
	server.add_insecure_port(bind_address)
	server.start()
	server.wait_for_termination()

if __name__ == "__main__":
	logging.basicConfig(level=logging.INFO)
	_serve(_PORT)