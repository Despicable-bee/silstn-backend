/**
 * @fileoverview gRPC-Web generated client stub for endpoints.silstn.jwk_cron_job_service
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


/* eslint-disable */
// @ts-nocheck



const grpc = {};
grpc.web = require('grpc-web');


var google_api_annotations_pb = require('./google/api/annotations_pb.js')
const proto = {};
proto.endpoints = {};
proto.endpoints.silstn = {};
proto.endpoints.silstn.jwk_cron_job_service = require('./jwk_cron_job_service_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.endpoints.silstn.jwk_cron_job_service.JwkCronJobServiceClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.endpoints.silstn.jwk_cron_job_service.JwkCronJobServicePromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.endpoints.silstn.jwk_cron_job_service.JwkCronJobService_Request,
 *   !proto.endpoints.silstn.jwk_cron_job_service.JwkCronJobService_Response>}
 */
const methodDescriptor_JwkCronJobService_generate_key_pair = new grpc.web.MethodDescriptor(
  '/endpoints.silstn.jwk_cron_job_service.JwkCronJobService/generate_key_pair',
  grpc.web.MethodType.UNARY,
  proto.endpoints.silstn.jwk_cron_job_service.JwkCronJobService_Request,
  proto.endpoints.silstn.jwk_cron_job_service.JwkCronJobService_Response,
  /**
   * @param {!proto.endpoints.silstn.jwk_cron_job_service.JwkCronJobService_Request} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.endpoints.silstn.jwk_cron_job_service.JwkCronJobService_Response.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.endpoints.silstn.jwk_cron_job_service.JwkCronJobService_Request,
 *   !proto.endpoints.silstn.jwk_cron_job_service.JwkCronJobService_Response>}
 */
const methodInfo_JwkCronJobService_generate_key_pair = new grpc.web.AbstractClientBase.MethodInfo(
  proto.endpoints.silstn.jwk_cron_job_service.JwkCronJobService_Response,
  /**
   * @param {!proto.endpoints.silstn.jwk_cron_job_service.JwkCronJobService_Request} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.endpoints.silstn.jwk_cron_job_service.JwkCronJobService_Response.deserializeBinary
);


/**
 * @param {!proto.endpoints.silstn.jwk_cron_job_service.JwkCronJobService_Request} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.endpoints.silstn.jwk_cron_job_service.JwkCronJobService_Response)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.endpoints.silstn.jwk_cron_job_service.JwkCronJobService_Response>|undefined}
 *     The XHR Node Readable Stream
 */
proto.endpoints.silstn.jwk_cron_job_service.JwkCronJobServiceClient.prototype.generate_key_pair =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/endpoints.silstn.jwk_cron_job_service.JwkCronJobService/generate_key_pair',
      request,
      metadata || {},
      methodDescriptor_JwkCronJobService_generate_key_pair,
      callback);
};


/**
 * @param {!proto.endpoints.silstn.jwk_cron_job_service.JwkCronJobService_Request} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.endpoints.silstn.jwk_cron_job_service.JwkCronJobService_Response>}
 *     Promise that resolves to the response
 */
proto.endpoints.silstn.jwk_cron_job_service.JwkCronJobServicePromiseClient.prototype.generate_key_pair =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/endpoints.silstn.jwk_cron_job_service.JwkCronJobService/generate_key_pair',
      request,
      metadata || {},
      methodDescriptor_JwkCronJobService_generate_key_pair);
};


module.exports = proto.endpoints.silstn.jwk_cron_job_service;

