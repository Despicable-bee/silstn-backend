"""
	FILENAME:		test_traffic_scraper.py

	AUTHOR(s):		

	DESCRIPTION:		

	VERSION:		0.0.1

	DESTINATION:		

	LICENSE:		Property of Starfighter Australia Pty Ltd
"""
# Standard Library
from concurrent import futures
import logging
import os
from typing import Text

# Third Party Library
import grpc

from root.base_classes.chunk_doc import Coordinates
from root.helpers.google_maps_traffic_helper.google_maps_traffic_helper import \
		GoogleMapsTrafficHelper

# Local Library
from root.services.test_traffic_scraper.generated import test_traffic_scraper_pb2_grpc as pb2_grpc
from root.services.test_traffic_scraper.generated import test_traffic_scraper_pb2 as pb2

_PORT = os.environ["PORT"]

class TestTrafficScraper(pb2_grpc.TestTrafficScraperServicer):
	
	def scrape_test(self,
				request: pb2.TestTrafficScraper_Request,
				context: grpc.ServicerContext) -> pb2.TestTrafficScraper_Response:
		""" Tests the scraping of a specific road """
		try:
			gmth = GoogleMapsTrafficHelper()
			origin = Coordinates(request.originLat, request.originLng)
			destination = Coordinates(request.destLat, request.destLng)

			timeInTrafficSec = gmth.get_traffic_data(origin=origin, 
					destination=destination)
			
			# Write this data to google cloud firestore

		except Exception as e:
			pass
		
	
	def init_database(self,
			request: pb2.InitDatabase_Request,
			context: grpc.ServicerContext) -> pb2.InitDatabase_Response:
		""" Initialises the database """
		try:
			pass
		except Exception as e:
			pass
		

def _serve(port: Text):
	bind_address = f"[::]:{port}"
	server = grpc.server(futures.ThreadPoolExecutor())
	pb2_grpc.add_TestTrafficScraperServicer_to_server(TestTrafficScraper(), server)
	server.add_insecure_port(bind_address)
	server.start()
	server.wait_for_termination()

if __name__ == "__main__":
	logging.basicConfig(level=logging.INFO)
	_serve(_PORT)