/**
 * @fileoverview gRPC-Web generated client stub for endpoints.silstn.data_collector
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


/* eslint-disable */
// @ts-nocheck



const grpc = {};
grpc.web = require('grpc-web');


var google_api_annotations_pb = require('./google/api/annotations_pb.js')
const proto = {};
proto.endpoints = {};
proto.endpoints.silstn = {};
proto.endpoints.silstn.data_collector = require('./data_collector_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.endpoints.silstn.data_collector.DataCollectorClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.endpoints.silstn.data_collector.DataCollectorPromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.endpoints.silstn.data_collector.DataCollector_Request,
 *   !proto.endpoints.silstn.data_collector.DataCollector_Response>}
 */
const methodDescriptor_DataCollector_run_data_collector = new grpc.web.MethodDescriptor(
  '/endpoints.silstn.data_collector.DataCollector/run_data_collector',
  grpc.web.MethodType.UNARY,
  proto.endpoints.silstn.data_collector.DataCollector_Request,
  proto.endpoints.silstn.data_collector.DataCollector_Response,
  /**
   * @param {!proto.endpoints.silstn.data_collector.DataCollector_Request} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.endpoints.silstn.data_collector.DataCollector_Response.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.endpoints.silstn.data_collector.DataCollector_Request,
 *   !proto.endpoints.silstn.data_collector.DataCollector_Response>}
 */
const methodInfo_DataCollector_run_data_collector = new grpc.web.AbstractClientBase.MethodInfo(
  proto.endpoints.silstn.data_collector.DataCollector_Response,
  /**
   * @param {!proto.endpoints.silstn.data_collector.DataCollector_Request} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.endpoints.silstn.data_collector.DataCollector_Response.deserializeBinary
);


/**
 * @param {!proto.endpoints.silstn.data_collector.DataCollector_Request} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.endpoints.silstn.data_collector.DataCollector_Response)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.endpoints.silstn.data_collector.DataCollector_Response>|undefined}
 *     The XHR Node Readable Stream
 */
proto.endpoints.silstn.data_collector.DataCollectorClient.prototype.run_data_collector =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/endpoints.silstn.data_collector.DataCollector/run_data_collector',
      request,
      metadata || {},
      methodDescriptor_DataCollector_run_data_collector,
      callback);
};


/**
 * @param {!proto.endpoints.silstn.data_collector.DataCollector_Request} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.endpoints.silstn.data_collector.DataCollector_Response>}
 *     Promise that resolves to the response
 */
proto.endpoints.silstn.data_collector.DataCollectorPromiseClient.prototype.run_data_collector =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/endpoints.silstn.data_collector.DataCollector/run_data_collector',
      request,
      metadata || {},
      methodDescriptor_DataCollector_run_data_collector);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.endpoints.silstn.data_collector.InvokeTraining_Request,
 *   !proto.endpoints.silstn.data_collector.InvokeTraining_Response>}
 */
const methodDescriptor_DataCollector_run_training_algorithm = new grpc.web.MethodDescriptor(
  '/endpoints.silstn.data_collector.DataCollector/run_training_algorithm',
  grpc.web.MethodType.UNARY,
  proto.endpoints.silstn.data_collector.InvokeTraining_Request,
  proto.endpoints.silstn.data_collector.InvokeTraining_Response,
  /**
   * @param {!proto.endpoints.silstn.data_collector.InvokeTraining_Request} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.endpoints.silstn.data_collector.InvokeTraining_Response.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.endpoints.silstn.data_collector.InvokeTraining_Request,
 *   !proto.endpoints.silstn.data_collector.InvokeTraining_Response>}
 */
const methodInfo_DataCollector_run_training_algorithm = new grpc.web.AbstractClientBase.MethodInfo(
  proto.endpoints.silstn.data_collector.InvokeTraining_Response,
  /**
   * @param {!proto.endpoints.silstn.data_collector.InvokeTraining_Request} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.endpoints.silstn.data_collector.InvokeTraining_Response.deserializeBinary
);


/**
 * @param {!proto.endpoints.silstn.data_collector.InvokeTraining_Request} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.endpoints.silstn.data_collector.InvokeTraining_Response)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.endpoints.silstn.data_collector.InvokeTraining_Response>|undefined}
 *     The XHR Node Readable Stream
 */
proto.endpoints.silstn.data_collector.DataCollectorClient.prototype.run_training_algorithm =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/endpoints.silstn.data_collector.DataCollector/run_training_algorithm',
      request,
      metadata || {},
      methodDescriptor_DataCollector_run_training_algorithm,
      callback);
};


/**
 * @param {!proto.endpoints.silstn.data_collector.InvokeTraining_Request} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.endpoints.silstn.data_collector.InvokeTraining_Response>}
 *     Promise that resolves to the response
 */
proto.endpoints.silstn.data_collector.DataCollectorPromiseClient.prototype.run_training_algorithm =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/endpoints.silstn.data_collector.DataCollector/run_training_algorithm',
      request,
      metadata || {},
      methodDescriptor_DataCollector_run_training_algorithm);
};


module.exports = proto.endpoints.silstn.data_collector;

