"""
	FILENAME:		data_collector.py

	AUTHOR(s):		

	DESCRIPTION:		

	VERSION:		0.0.1

	DESTINATION:		

	LICENSE:		Property of 
"""
# Standard Library
from concurrent import futures
import logging
import os
from typing import Text

import sys
import time

# Third Party Library
import grpc

# Local Library
from root.services.data_collector.generated import data_collector_pb2_grpc as pb2_grpc
from root.services.data_collector.generated import data_collector_pb2 as pb2

from root.helpers.data_collector.data_collector_firestore_helper import \
		DataCollectorHelper

from root.helpers.common_helper.common_firestore_helper import \
		FirestoreErrorHelper

from root.helpers.prediction_api.prediction_api_helper import \
		PredictionApiHelper

_PORT = os.environ["PORT"]

# * DEBUG ----------------------------------------------------------------------

__filename__ = 'data_collector.py'

class DataCollector(pb2_grpc.DataCollectorServicer):
	def run_data_collector(self,
			request: pb2.DataCollector_Request,
			context: grpc.ServicerContext) -> pb2.DataCollector_Response:
		""" Service for routinely collecting data from the brisbane government
			website. 
		"""
		dc = DataCollectorHelper()
		try:
			# Sleep for 30 seconds (allow the brisbane city database to update)
			time.sleep(10)

			# Get the intersection volume data
			dc.get_and_process_intersection_data_sample()
			return pb2.DataCollector_Response(
					status=int(pb2.SUCCESSFUL))

		except Exception as e:
			exc_type, exc_obj, exc_tb = sys.exc_info()
			feh = FirestoreErrorHelper()
			feh.api_error_handler(fileName=__filename__, 
					methodName='run_data_collector', 
					exceptionMsg=e,
					lineNum=exc_tb.tb_lineno)   # type: ignore
			return pb2.DataCollector_Response(
					status=int(pb2.ERROR))

	def run_training_algorithm(self,
			request: pb2.InvokeTraining_Request,
			context: grpc.ServicerContext) -> pb2.InvokeTraining_Response:
		""" Invokes the training algorith :v """
		pah = PredictionApiHelper()
		try:
			# Sends a new request for training data.
			pah.request_training()

			return pb2.InvokeTraining_Response(
					status=int(pb2.SUCCESSFUL))
		except Exception as e:
			exc_type, exc_obj, exc_tb = sys.exc_info()
			feh = FirestoreErrorHelper()
			feh.api_error_handler(fileName=__filename__, 
					methodName='run_data_collector', 
					exceptionMsg=e,
					lineNum=exc_tb.tb_lineno)   # type: ignore
			return pb2.InvokeTraining_Response(
					status=int(pb2.ERROR))

def _serve(port: Text):
	bind_address = f"[::]:{port}"
	server = grpc.server(futures.ThreadPoolExecutor())
	pb2_grpc.add_DataCollectorServicer_to_server(DataCollector(), server)
	server.add_insecure_port(bind_address)
	server.start()
	server.wait_for_termination()

if __name__ == "__main__":
	logging.basicConfig(level=logging.INFO)
	_serve(_PORT)