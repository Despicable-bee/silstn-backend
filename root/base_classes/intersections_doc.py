# Standard Libs
from typing import List
from typing import Optional

from enum import Enum
from enum import auto

import sys

import json

# Third Party Libs

# Local Libs
from root.helpers.common_helper.common_firestore_helper import \
        FirestoreDocSizeHelper
from root.helpers.common_helper.common_firestore_helper import \
        FirestoreErrorHelper

# Base Classes
from root.base_classes.base_doc import BaseDoc
from root.base_classes.base_doc import BaseDocType

from root.base_classes.common_classes import Coordinates

# * DEBUG ----------------------------------------------------------------------

__filename__ = 'intersections_doc.py'

# * ENUMS ----------------------------------------------------------------------

class TravelMode(Enum):
    DRIVING = auto()
    # TODO - Add more driving modes

# * SUPPORT CLASS DEFINITIONS --------------------------------------------------

class TrafficDataManifest(object):
    def __init__(self):
        pass

class Step(object):
    def __init__(self, 
            distanceValue: int,
            durationValue: int,
            coords: List[Coordinates]):
        
        # Distance of the step (in metres)
        self.__distanceValue = distanceValue
        # Duration of the step, including traffic (in seconds)
        self.__durationValue = durationValue

        self.__coords = coords

        self.__classname__ = 'Step'

    def get_distance_value(self):
        return self.__distanceValue

    def set_distance_value(self, newDistanceValue: int):
        self.__distanceValue = newDistanceValue

    def get_duration_value(self):
        return self.__durationValue

    def set_duration_value(self, newDurationValue: int):
        self.__durationValue = newDurationValue

    def get_coords(self):
        return self.__coords

    def to_dict(self):
        """ Returns the contents of the class as a dictionary. """
        try:
            coordsDictList = []
            for coord in self.__coords:
                coordsDictList.append(coord.to_dict())
            
            return {    
                'duration_value': self.__durationValue,
                'distance_value': self.__distanceValue,
                'coords': coordsDictList
            }
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.class_error_handler(fileName=__filename__,
                    className=self.__classname__,
                    methodName='to_dict', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            raise Exception(e)

class Route(object):
    def __init__(self, steps: List[Step]):
        self.__steps = steps
        self.__classname__ = 'Route'

    def get_steps(self):
        return self.__steps
    
    def set_steps(self, newSteps: List[Step]):
        self.__steps = newSteps

    def to_dict(self):
        try:
            listOfStepsDict: List[dict] = []
            for step in self.__steps:
                listOfStepsDict.append(step.to_dict())
            return {
                'steps': listOfStepsDict
            }
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.class_error_handler(fileName=__filename__,
                    className=self.__classname__,
                    methodName='to_dict', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            raise Exception(e)

class Arm(object):
    def __init__(self,
            nextTsc: int,
            route: Route,
            streetName: str):
        
        # Preconditions
        assert isinstance(nextTsc, int) and nextTsc >= 0, \
                "nextTsc must be a valid integer"
        assert isinstance(route, Route), "must be a valid Route object"
        assert isinstance(streetName, str), "streetname is a valid string"

        self.__nextTsc = nextTsc
        self.__route = route
        self.__streetName = streetName

        self.__classname__ = 'Arm'

    def get_next_tsc(self):
        return self.__nextTsc

    def set_next_tsc(self, newNextTsc: int):
        self.__nextTsc = newNextTsc

    def get_route(self):
        return self.__route

    def set_route(self, newRoute: Route):
        self.__route = newRoute

    def get_street_name(self):
        return self.__streetName

    def set_street_name(self, newStreetName: str):
        self.__streetName = newStreetName

    def to_dict(self):
        """ Returns the contents of the object as a dictionary. """
        try:
            return {
                'next_tsc': self.__nextTsc,
                'route': self.__route.to_dict(),
                'street_name': self.__streetName
            }
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.class_error_handler(fileName=__filename__,
                    className=self.__classname__,
                    methodName='to_dict', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            raise Exception(e)

class Intersection(object):
    def __init__(self,
            origin: Coordinates,
            tsc: int,
            arms: List[Arm]):
        # Preconditions
        assert isinstance(origin, Coordinates), \
                "must be a valid Coordinates object"
        assert isinstance(tsc, int) and tsc >= 0, "Must be a valid +ve int"
        assert isinstance(arms, List), "must be a valid list of arms"

        self.__origin = origin
        self.__tsc = tsc
        self.__arms = arms

        self.__classname__ = 'Intersection'

    def get_origin(self):
        return self.__origin
        
    def set_origin(self, newOrigin: Coordinates):
        self.__origin = newOrigin

    def get_tsc(self):
        return self.__tsc

    def set_tsc(self, newTsc: int):
        self.__tsc = newTsc

    def get_arms(self):
        return self.__arms

    def set_arms(self, newArms: List[Arm]):
        self.__arms = newArms

    def to_dict(self):
        """ Returns the contents of the object as a dictionary. """
        try:

            armsDictList = []
            for arm in self.__arms:
                armsDictList.append(arm.to_dict())

            return {
                'tsc': self.__tsc,
                'origin': self.__origin.to_dict(),
                'arms': armsDictList
            }
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.class_error_handler(fileName=__filename__,
                    className=self.__classname__,
                    methodName='to_dict', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            raise Exception(e)

class IntersectionDocManifestInstance(object):
    def __init__(self, listOfTscs: List[int], currentCapacity: int, 
            docId: str):
        self.__listOfTscs = listOfTscs
        self.__currentCapacity = currentCapacity
        self.__docId = docId

    def get_list_of_tscs(self):
        return self.__listOfTscs

    def set_list_of_tscs(self, newListOfTscs: List[int]):
        self.__listOfTscs = newListOfTscs

    def get_current_capacity(self):
        return self.__currentCapacity

    def set_current_capacity(self, newCapacity: int):
        self.__currentCapacity = newCapacity
    
    def get_doc_id(self):
        return self.__docId

    def set_doc_id(self, newDocId: str):
        self.__docId = newDocId

    def to_dict(self):
        return {
            'list_of_tscs': self.__listOfTscs,
            'current_capacity': self.__currentCapacity,
            'doc_id': self.__docId
        }

class IntersectionsDocManifest(BaseDoc):
    def __init__(self,
            instancesList: List[IntersectionDocManifestInstance],
            predictionTscs: str,
            # BaseDoc
            docType: BaseDocType, 
            createTime: int, 
            lastEditted: int, 
            ownerId: str, 
            # BaseDocVersion
            major: int, 
            minor: int, 
            patch: int):
        # Preconditions
        # TODO

        self.__instancesList = instancesList
        self.__predictionTscs = predictionTscs

        # Initialise base class
        BaseDoc.__init__(self, docType=docType, createTime=createTime, 
                lastEditted=lastEditted, ownerId=ownerId, major=major,
                minor=minor, patch=patch)

    # ? PUBLIC METHODS ---------------------------------------------------------

    def get_instances_list(self):
        return self.__instancesList
    
    def set_instances_list(self, 
            newInstancesList: List[IntersectionDocManifestInstance]):
        self.__instancesList = newInstancesList

    def get_prediction_tscs_as_list(self):
        return json.loads(self.__predictionTscs)

    def set_prediction_tscs_as_string(self, listOfTscs: List[int]):
        self.__predictionTscs = json.dumps(listOfTscs)

    def to_dict(self):
        instancesDictsList = []
        for instance in self.__instancesList:
            instancesDictsList.append(instance.to_dict())

        return {
            'instances': instancesDictsList,
            'prediction_tscs': self.__predictionTscs
        } | BaseDoc.to_dict(self)

# * MAIN CLASS DEFINITIONS -----------------------------------------------------

class IntersectionsDoc(BaseDoc):
    def __init__(self,
            # Class specific
            intersections: List[Intersection],
            # BaseDoc
            docType: BaseDocType, 
            createTime: int, 
            lastEditted: int, 
            ownerId: str, 
            # BaseDocVersion
            major: int, 
            minor: int, 
            patch: int,
            currentCapacity: Optional[int]=None):
        # Preconditions
        # TODO

        self.__intersections = intersections
        self.__classname__ = 'IntersectionsDoc'

        # Initialise base class
        BaseDoc.__init__(self, docType=docType, createTime=createTime, 
                lastEditted=lastEditted, ownerId=ownerId, major=major,
                minor=minor, patch=patch)

        try:
            if isinstance(currentCapacity, int):
                self.__currentCapacity = currentCapacity
            elif currentCapacity == None:
                fdsh = FirestoreDocSizeHelper()
                self.__currentCapacity = 0
                docAbsPath = self.__get_doc_abs_path()

                self.__currentCapacity = fdsh.firestore_doc_size_calc(
                        docAbsPath=docAbsPath, 
                        docDict=self.to_dict(),
                        docName=BaseDoc.get_owner_id(self))
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.class_error_handler(fileName=__filename__,
                    className=self.__classname__,
                    methodName='__init__', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            raise Exception(e)

    # ? PUBLIC METHODS ---------------------------------------------------------

    def add_intersection(self, intersection: Intersection):
        """ Attempts to add an intersection to the list.

        ARGS:
        - intersection: the intersection we want to add to the list.

        RETURNS:
        If the document can fit the intersection in, then the method will return
            `True`.
        If the document can NOT fit the intersection in, then the method will 
            return `False`
        
        """
        fdsh = FirestoreDocSizeHelper()
        docAbsPath = self.__get_doc_abs_path()
        
        capacityToAdd = fdsh.firestore_doc_size_calc(docAbsPath=docAbsPath,
            docDict=intersection.to_dict(),
            docName=BaseDoc.get_owner_id(self))

        if (capacityToAdd + self.__currentCapacity) > \
                fdsh.get_firestore_doc_max_size() * 0.75:
            return False

        # Add the chunk to the list
        self.__currentCapacity += capacityToAdd
        self.__intersections.append(intersection)

        return True

    def get_current_capacity(self):
        return self.__currentCapacity

    def set_current_capacity(self, newCurrentCapacity: int):
        self.__currentCapacity = newCurrentCapacity

    def get_intersections(self):
        return self.__intersections

    def set_intersections(self, newIntersections: List[Intersection]):
        self.__intersections = newIntersections
    
    def to_dict(self):
        """ Returns the contents of the object as a dictionary. """
        intersectionsDictList = []
        for intersection in self.__intersections:
            intersectionsDictList.append(intersection.to_dict())

        return {
            'intersections': intersectionsDictList,
            'current_capacity': self.__currentCapacity
        } | BaseDoc.to_dict(self)

    # ? PRIVATE METHODS --------------------------------------------------------

    def __get_doc_abs_path(self):
        return "intersections"