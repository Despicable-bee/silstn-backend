# Standard Libs
from typing import List
import sys

# Third Party Libs

# Local Libs

from root.helpers.common_helper.common_firestore_helper import \
        FirestoreErrorHelper

# Base Classes

from root.base_classes.base_doc import BaseDoc
from root.base_classes.base_doc import BaseDocType

# * DEBUG ----------------------------------------------------------------------

__filename__ = 'intersec_data_doc.py'

# * ENUM -----------------------------------------------------------------------


# * AUXILIARY HELPERS ----------------------------------------------------------

class IntersectionDataManifestDocObjectFactory(object):
    def __init__(self):
        self.__classname__ = 'IntersectionDataManifestDocObjectFactory'

    def to_obj_v_0_3_0(self, inputDict: dict):
        try:
            docVersion = inputDict['doc_version'].split('.')

            # Get all the manifests
            manifestsList = self.__manifest_instance_to_obj(
                    inputDict=inputDict['manifests'])

            return IntersectionDataManifestDoc(
                    docType=BaseDocType(inputDict['doc_type']),
                    createTime=inputDict['create_time'],
                    lastEditted=inputDict['last_editted'],
                    ownerId=inputDict['owner_id'],
                    currentDocName=inputDict['current_doc'],
                    currentEndEpoch=inputDict['end_epoch'],
                    currentStartEpoch=inputDict['start_epoch'],
                    major=int(docVersion[0]),
                    minor=int(docVersion[1]),
                    patch=int(docVersion[2]),
                    manifestsList=manifestsList,
                    currentCapacity=inputDict['current_capacity'])

        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.class_error_handler(fileName=__filename__,
                    className=self.__classname__, 
                    methodName='to_obj_v_0_3_0', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            return None


    def __manifest_instance_to_obj(self, inputDict: dict):
        """ NOTE
        
        - This method expects the `inputDict` to be everything including and 
            below the `manifests` item (not the original inputDict).

        - Check the appropriate `ManifestInstance.to_dict()` method for more
            details
        """
        try: 
            manifestsList: List[ManifestInstance] = []
            for instance in inputDict:
                currentInstance = inputDict[instance]
                manifestsList.append(ManifestInstance(
                    docName=instance,
                    currentCapacity=currentInstance['current_capacity'],
                    startDate=currentInstance['start_date'],
                    endDate=currentInstance['end_date']))

            return manifestsList
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.class_error_handler(fileName=__filename__,
                    className=self.__classname__, 
                    methodName='__manifest_instance_to_obj', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            raise Exception("Error trying to initialise manifests instance")

    
class IntersectionDataDocObjectFactory(object):
    def __init__(self):
        self.__classname__ = 'IntersectionDataDocObjectFactory'

    def to_obj_v_0_3_0(self, inputDict: dict):
        try:
            docVersion = inputDict['doc_version'].split('.')

            dataList = self.__data_instance_to_obj(inputDict=inputDict['data'])

            return IntersectionDataDoc(
                docType=BaseDocType(inputDict['doc_type']),
                createTime=inputDict['create_time'],
                lastEditted=inputDict['last_editted'],
                ownerId=inputDict['owner_id'],
                major=int(docVersion[0]),
                minor=int(docVersion[1]),
                patch=int(docVersion[2]),
                data=dataList,
                currentCapacity=inputDict['current_capacity'])
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.class_error_handler(fileName=__filename__,
                    className=self.__classname__, 
                    methodName='to_obj_v_0_3_0', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            return None
        
    def __data_instance_to_obj(self, inputDict: dict):
        """ NOTE
        
        - This method expects the `inputDict` to be everything including and 
            below the `manifests` item (not the original inputDict).

        - Many of these access 
        """
        intersectionTscDataList: List[IntersectionsTscData] = []
        try:
            for tsc in inputDict:
                currentDataPairsList = inputDict[tsc]
                intersectionDataPairsList: List[IntersectionTscDataPair] = []
                for dataPair in currentDataPairsList:
                    intersectionDataPairsList.append(
                        IntersectionTscDataPair(
                            timestamp=dataPair['timestamp'],
                            cmf=dataPair['cmf']
                        ))
                intersectionTscDataList.append(IntersectionsTscData(
                    tsc=int(tsc), dataPairs=intersectionDataPairsList))

            return intersectionTscDataList
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.class_error_handler(fileName=__filename__,
                    className=self.__classname__, 
                    methodName='__manifest_instance_to_obj', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            raise Exception("Error trying to initialise data instance")
        

# * MANIFEST CLASS -------------------------------------------------------------

class ManifestInstance(object):
    def __init__(self, docName: str, 
            currentCapacity: int, 
            startDate: int, 
            endDate: int):
        """ Object used to specify the manifest of a target document. 
        
        ARGS:
        - docName: Name of the document
        - currentCapacity: Capacity of the target document (in bytes)
        - startDate: Date of the earliest record
        - endDate: Date of the latest record
        """
        # Preconditions
        self.__doc_name_precondition(docName=docName)
        self.__current_capacity_precondition(currentCapacity=currentCapacity)
        self.__start_date_precondition(startDate=startDate)
        self.__end_date_precondition(endDate=endDate)

        self.__docName = docName
        self.__currentCapacity = currentCapacity
        self.__startDate = startDate
        self.__endDate = endDate

    # ? PUBLIC METHODS ---------------------------------------------------------

    def get_doc_name(self):
        return self.__docName

    def set_doc_name(self, newDocName: str):
        # Preconditions
        self.__doc_name_precondition(docName=newDocName)
        self.__docName = newDocName

    def get_current_capacity(self):
        return self.__currentCapacity

    def set_current_capacity(self, newCurrentCapacity: int):
        # Preconditons
        self.__current_capacity_precondition(currentCapacity=newCurrentCapacity)
        self.__currentCapacity = newCurrentCapacity

    def get_start_date(self):
        return self.__startDate

    def set_start_date(self, newStartDate: int):
        # Preconditions
        self.__start_date_precondition(startDate=newStartDate)
        self.__startDate = newStartDate

    def get_end_date(self):
        return self.__endDate

    def set_end_date(self, newEndDate: int):
        # Preconditions
        self.__end_date_precondition(endDate=newEndDate)
        self.__endDate = newEndDate

    def to_dict(self):
        """ Returns the contents of the object as a dictionary. 
        
        RETURN FORM:
        - The method will return a dictionary of the following form:
        
        ```
        {
            docName1: {
                current_capacity: ...
                start_date: ...
                end_date: ...
            }
        }
        ```
        """
        
        return {
            self.__docName: {
                'current_capacity': self.__currentCapacity,
                'start_date': self.__startDate,
                'end_date': self.__endDate
            }
        }

    # ? STATIC METHODS ---------------------------------------------------------

    @staticmethod
    def __doc_name_precondition(docName: str):
        assert isinstance(docName, str), "docName must be a valid string"

    @staticmethod
    def __current_capacity_precondition(currentCapacity: int):
        assert currentCapacity >= 0, "capacity must be a positive integer"

    @staticmethod
    def __start_date_precondition(startDate: int):
        assert startDate >= 0, "startDate must be greater than or equal to zero"

    @staticmethod
    def __end_date_precondition(endDate: int):
        assert endDate >= 0, "endDate must be greater than or equal to zero"

class IntersectionDataManifestDoc(BaseDoc):
    def __init__(self,
            # Class specific
            manifestsList: List[ManifestInstance],
            currentDocName: str,
            currentStartEpoch: int,
            currentEndEpoch: int,
            currentCapacity: int,
            # BaseDoc
            docType: BaseDocType, 
            createTime: int, 
            lastEditted: int, 
            ownerId: str, 
            # BaseDocVersion
            major: int, 
            minor: int, 
            patch: int):
        # Preconditions
        self.__manifests_list_precondition(manifestsList=manifestsList)
        self.__current_doc_name_precondition(currentDocName=currentDocName)
        self.__current_epoch_precondition(
                currentEndEpoch=currentEndEpoch,
                currentStartEpoch=currentStartEpoch)
        self.__current_capacity_precondition(currentCapacity=currentCapacity)

        self.__manifestsList = manifestsList
        self.__currentDocName = currentDocName
        self.__currentStartEpoch = currentStartEpoch
        self.__currentEndEpoch = currentEndEpoch
        self.__currentCapacity = currentCapacity

        # Initialise base class
        BaseDoc.__init__(self, docType=docType, createTime=createTime, 
                lastEditted=lastEditted, ownerId=ownerId, major=major,
                minor=minor, patch=patch)
    
    # ? PUBLIC METHODS ---------------------------------------------------------

    def get_manifests_list(self):
        return self.__manifestsList

    def set_manifests_list(self, newManifestsList: List[ManifestInstance]):
        # Preconditions
        self.__manifests_list_precondition(manifestsList=newManifestsList)
        self.__manifestsList = newManifestsList

    def get_current_doc_name(self):
        return self.__currentDocName
    
    def set_current_doc_name(self, newCurrentDocName: str):
        # Preconditions
        self.__current_doc_name_precondition(currentDocName=newCurrentDocName)
        self.__currentDocName = newCurrentDocName

    def get_current_start_epoch(self):
        return self.__currentStartEpoch

    def set_current_start_epoch(self, newStartingEpoch: int):
        """ NOTE: 
            - The preconditions for this method checks whether the 
            `endEpoch` > `startEpoch`. Thus it is advised that you call the 
            `set_current_end_epoch` BEFORE you call this method."""
        # Preconditions
        self.__current_epoch_precondition(currentStartEpoch=newStartingEpoch, 
                currentEndEpoch=self.__currentEndEpoch)
        self.__currentStartEpoch = newStartingEpoch

    def get_current_end_epoch(self):
        return self.__currentEndEpoch
    
    def set_current_end_epoch(self, newEndingEpoch: int):
        # Preconditions
        self.__current_epoch_precondition(
                currentStartEpoch=self.__currentStartEpoch,
                currentEndEpoch=newEndingEpoch)
        
        self.__currentEndEpoch = newEndingEpoch

    def get_current_capacity(self):
        return self.__currentCapacity

    def set_current_capacity(self, newCurrentCapacity: int):
        self.__current_capacity_precondition(
                currentCapacity=newCurrentCapacity)
        self.__currentCapacity = newCurrentCapacity

    def to_dict(self):
        """ Returns the contents of the object as a dictionary. """
        manifestsDict = {}
        for manif in self.__manifestsList:
            manifestsDict = manifestsDict | manif.to_dict()
        
        return {
            'manifests': manifestsDict,
            'current_doc': self.__currentDocName,
            'start_epoch': self.__currentStartEpoch,
            'end_epoch': self.__currentEndEpoch,
            'current_capacity': self.__currentCapacity
        } | BaseDoc.to_dict(self)

    # ? STATIC METHODS ---------------------------------------------------------

    @staticmethod
    def __current_epoch_precondition(currentEndEpoch: int, 
            currentStartEpoch: int):
        assert currentEndEpoch >= 0, "ending epoch must be >= 0"
        assert currentStartEpoch >= 0, "starting epoch must be >= 0"
        assert currentEndEpoch >= currentStartEpoch, \
                "Ending epoch must be >= starting epoch"

    @staticmethod
    def __current_doc_name_precondition(currentDocName: str):
        assert isinstance(currentDocName, str), \
                "currentDocName must be a valid string"

    @staticmethod
    def __manifests_list_precondition(manifestsList: List[ManifestInstance]):
        assert isinstance(manifestsList, list), \
                "manifestsList must be a valid list"
        
        for manif in manifestsList:
            assert isinstance(manif, ManifestInstance), \
                    "All instances of the list must be a valid ManifestInstance"

    @staticmethod
    def __current_capacity_precondition(currentCapacity: int):
        assert currentCapacity >= 0, \
                "Current capacity must be a positive integer"

# * MAIN CLASS -----------------------------------------------------------------


class IntersectionTscDataPair(object):
    def __init__(self, timestamp: int, cmf: int):
        """ The pair of data points to be displayed on a graph. 
        
        ARGS:
        - timestamp: The UNIX timestamp since last epoch (ms)
        - cmf: The combined measured flow.
        """
        # Preconditions
        self.__timestamp_precondition(timestamp=timestamp)
        self.__cmf_precondition(cmf=cmf)

        self.__timestamp = timestamp
        self.__cmf = cmf

    # ? PUBLIC METHODS ---------------------------------------------------------

    def get_timestamp(self):
        """ Returns the timestamp in milliseconds """
        return self.__timestamp

    def set_timestamp(self, newTimestamp: int):
        # Preconditions
        self.__timestamp_precondition(timestamp=newTimestamp)
        self.__timestamp = newTimestamp

    def get_cmf(self):
        return self.__cmf
    
    def set_cmf(self, newCmf: int):
        # Preconditions
        self.__cmf_precondition(cmf=newCmf)
        self.__cmf = newCmf

    def to_dict(self):
        """ Returns the contents of the object as a dictionary. """
        return {
            'timestamp': self.__timestamp,
            'cmf': self.__cmf
        }

    # ? STATIC METHODS ---------------------------------------------------------
    @staticmethod
    def __timestamp_precondition(timestamp: int):
        assert timestamp >= 0, "timestamp must be greater than 0"

    @staticmethod
    def __cmf_precondition(cmf: int):
        assert cmf >= 0, "No such thing as negative traffic"

class IntersectionsTscData(object):
    def __init__(self, tsc: int, dataPairs: List[IntersectionTscDataPair]):
        # Preconditions
        self.__tsc_precondition(tsc=tsc)
        self.__data_pairs_precondition(dataPairs=dataPairs)

        self.__tsc = tsc
        self.__dataPairs = dataPairs

    # ? PUBLIC METHODS ---------------------------------------------------------

    def get_tsc(self):
        return self.__tsc

    def set_tsc(self, newTsc: int):
        # Preconditions
        self.__tsc_precondition(tsc=newTsc)
        self.__tsc = newTsc

    def get_data_pairs(self):
        return self.__dataPairs

    def set_data_pairs(self, newDataPairs: List[IntersectionTscDataPair]):
        # Preconditions
        self.__data_pairs_precondition(dataPairs=newDataPairs)
        self.__dataPairs = newDataPairs

    def to_dict(self):
        """ Returns the contents of the object as a dictionary. """
        dataPairsDictList = []

        for pair in self.__dataPairs:
            dataPairsDictList.append(pair.to_dict())

        return {
            str(self.__tsc): dataPairsDictList
        }

    # ? STATIC METHODS ---------------------------------------------------------

    @staticmethod
    def __tsc_precondition(tsc: int):
        assert tsc > 0, "Negative tsc's don't exist"

    @staticmethod
    def __data_pairs_precondition(dataPairs: List[IntersectionTscDataPair]):
        assert isinstance(dataPairs, list), "dataPairs must be a valid list"
        for pair in dataPairs:
            assert isinstance(pair, IntersectionTscDataPair), \
                    "All list items must be an instance of " + \
                    "IntersectionTscDataPair"

class IntersectionDataDoc(BaseDoc):
    def __init__(self,
            data: List[IntersectionsTscData],
            currentCapacity: int,
            # BaseDoc
            docType: BaseDocType, 
            createTime: int, 
            lastEditted: int, 
            ownerId: str, 
            # BaseDocVersion
            major: int, 
            minor: int, 
            patch: int):

        # Preconditions 
        self.__data_precondition(data=data)
        self.__current_capacity_precondition(currentCapacity=currentCapacity)

        self.__data = data
        self.__currentCapacity = currentCapacity

        # Initialise base class
        BaseDoc.__init__(self, docType=docType, createTime=createTime, 
                lastEditted=lastEditted, ownerId=ownerId, major=major,
                minor=minor, patch=patch)

    # ? PUBLIC METHODS ---------------------------------------------------------

    def get_data(self):
        return self.__data

    def set_data(self, newData: List[IntersectionsTscData]):
        # Preconditions
        self.__data_precondition(data=newData)
        self.__data = newData

    def get_current_capacity(self):
        return self.__currentCapacity

    def set_current_capacity(self, newCurrentCapacity: int):
        self.__current_capacity_precondition(
                currentCapacity=newCurrentCapacity)
        self.__currentCapacity = newCurrentCapacity

    def to_dict(self):
        """ Returns the contents of the object as a dictionary. """
        dataDict = {}
        for d in self.__data:
            dataDict = dataDict | d.to_dict()

        return {
            'data': dataDict,
            'current_capacity': self.__currentCapacity
        } | BaseDoc.to_dict(self)

    # ? STATIC METHODS ---------------------------------------------------------

    @staticmethod
    def __data_precondition(data: List[IntersectionsTscData]):
        assert isinstance(data, list), "data must be a valid list"
        for d in data:
            assert isinstance(d, IntersectionsTscData), \
                    "each item in data must be a valid IntersectionDataDoc"

    @staticmethod
    def __current_capacity_precondition(currentCapacity: int):
        assert isinstance(currentCapacity, int), \
                'currentCapacity must be a valid integer'
        assert currentCapacity >= 0, 'currentCapacity must be >= 0'
