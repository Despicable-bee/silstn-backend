# Standard Libs
from enum import Enum
from enum import auto
from typing import Dict
from typing import Union
from typing import List
from typing import Optional
import sys


# Third Party Libs

# Local libs
from root.base_classes.base_doc import BaseDoc
from root.base_classes.base_doc import BaseDocType

from root.helpers.common_helper.common_firestore_helper import \
        FirestoreDocSizeHelper
from root.helpers.common_helper.common_firestore_helper import \
        FirestoreErrorHelper


# * DEBUG ----------------------------------------------------------------------

__filename__ = 'sub_doc.py'

# * ENUMS ----------------------------------------------------------------------

class SubDocStatus(Enum):
    def _generate_next_value_(name, start, count, last_values): # type: ignore
        """ Automatic numbering function. Use this to specify what 
        <Enum_instance>.value will return.
        """
        return count + 1
    READ_AND_WRITE = auto()
    READ_ONLY = auto()

class SubDocModes(Enum):
    def _generate_next_value_(name, start, count, last_values): # type: ignore
        """ Automatic numbering function. Use this to specify what 
        <Enum_instance>.value will return.
        """
        return count + 1
    # Chunk docs
    TRAFFIC_DATA = auto()

class SubDocInstanceModes(Enum):
    def _generate_next_value_(name, start, count, last_values): # type: ignore
        """ Automatic numbering function. Use this to specify what 
        <Enum_instance>.value will return.
        """
        return count + 1

    # Chunk docs
    TRAFFIC_DATA_INSTANCE = auto()


# * AUXILIARY HELPERS ----------------------------------------------------------


class SubDocManifestObjectFactory:
    def __init__(self):
        self.__classname__ = 'SubDocManifestObjectFactory'

    def to_obj_v_0_3_0(self, inputDict: dict):
        try:
            return SubDocManifest(
                createTime=inputDict['create_time'],
                currentCapacity=inputDict['current_capacity'],
                lastEditted=inputDict['last_editted'],
                status=SubDocStatus(inputDict['status']),
                subDocId=inputDict['sub_doc_id'])
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.class_error_handler(fileName=__filename__,
                    className=self.__classname__, 
                    methodName='to_obj_v_0_3_0', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            return None

# * SUB DOC INSTANCES ----------------------------------------------------------

class SubDocActual_Base_Instance(object):
    def __init__(self, mode: SubDocInstanceModes):
        self.__mode = mode

    def get_mode(self):
        return self.__mode

    def set_mode(self, newMode: SubDocInstanceModes):
        self.__mode = newMode

    def to_dict(self):
        return {
            'mode': self.__mode.value
        }

class SubDocActual_TrafficData_Instance(SubDocActual_Base_Instance):
    def __init__(self,
            durationValue: int,
            timestamp: int,
            # BaseInstance
            mode: SubDocInstanceModes):
        
        self.__durationValue = durationValue
        self.__timestamp = timestamp

        SubDocActual_Base_Instance.__init__(self, mode)

    def get_duration_value(self):
        return self.__durationValue

    def set_duration_value(self, newDurationValue: int):
        self.__durationValue = newDurationValue

    def get_timestamp(self):
        return self.__timestamp
    
    def set_timestamp(self, newTimestamp: int):
        self.__timestamp = newTimestamp

    def to_dict(self):
        """ Returns the contents of the class as a dictionary. """
        return {
            'duration_value': self.__durationValue,
            'timestamp': self.__timestamp
        } | SubDocActual_Base_Instance.to_dict(self)


# * CUSTOM TYPES ---------------------------------------------------------------

# Add more instances here if need be
SubDocActualInstanceTypes = Union[SubDocActual_TrafficData_Instance]

# * SUPPORT CLASS DEFINITIONS --------------------------------------------------

class SubDocActual(BaseDoc):
    def __init__(self,
            # BaseDoc
            docType: BaseDocType, 
            createTime: int, 
            lastEditted: int, 
            ownerId: str, 
            # BaseDocVersion
            major: int, 
            minor: int, 
            patch: int,
            # Class specific
            currentCapacity: int,
            groupName: str,
            groupInstances: List[SubDocActualInstanceTypes]):
        # Preconditions
        # TODO

        # Initialise base class
        BaseDoc.__init__(self, docType=docType, createTime=createTime, 
                lastEditted=lastEditted, ownerId=ownerId, major=major,
                minor=minor, patch=patch)

        self.__currentCapacity = currentCapacity
        self.__groupName = groupName
        self.__groupInstances = groupInstances

        # Switch statement dictionary
        self.__replaceGroupInstanceSwitch = {
            docType.TRAFFIC_DATA_SUB_DOC: \
                    self.__replace_traffic_data_group_instance
        }

    # PUBLIC METHODS -----------------------------------------------------------

    def get_group_instances(self):
        return self.__groupInstances

    def set_group_instances(self, 
            newGroupInstances: List[SubDocActualInstanceTypes]):
        # Preconditions
        # TODO
        self.__groupInstances = newGroupInstances

    def replace_group_instance(self, 
            newGroupInstance: SubDocActualInstanceTypes):
        """ Attempts to replace an existing instance with a new one. """
        return self.__replaceGroupInstanceSwitch[self.get_doc_type()](
                newGroupInstance)

    def add_to_group_instance(self, 
            newGroupInstance: SubDocActualInstanceTypes):
        """ Attempts to add a new instance to the `groupInstances` list. 
        
        Sub docs needs to be aware of when they are full, and refuse changes
            if they become full, or adding to the list would put the document
            over-capacity.

        ARGS:
        - newGroupInstance: The instance we want to add.

        RETURNS:
        - On success, the `newGroupInstance` is appended to the groupInstances
            list, and the method returns `True`. Otherwise, if the added
            instance exceeds the maximum firestore doc size, the method returns
            `False`. 
        """
        fdsh = FirestoreDocSizeHelper()
        capacityToAdd = fdsh.firestore_handle_dict_size_calc(
                self.__groupName, newGroupInstance.to_dict())

        if (self.__currentCapacity + capacityToAdd) >= \
                fdsh.get_firestore_doc_max_size():
            # Document is full, return False
            return False
        self.__groupInstances.append(newGroupInstance) # type: ignore
        return True

    def get_current_capacity(self):
        return self.__currentCapacity

    def set_current_capacity(self, newCurrentCapacity: int):
        # Preconditions
        # TODO
        self.__currentCapacity = newCurrentCapacity

    def to_dict(self):
        """ Returns the contents of the class as a dict. """
        groupInstancesDict = {}
        for instance in self.__groupInstances:
            print("Instance in the dict:")
            print(instance)
            groupInstancesDict = groupInstancesDict | instance.to_dict()

        returnDict = {
            "current_capacity": self.__currentCapacity,
            self.__groupName: groupInstancesDict,
        } | BaseDoc.to_dict(self)

        return returnDict


    # PRIVATE METHODS ----------------------------------------------------------

    def __replace_traffic_data_group_instance(self, 
            newGroupInstance: SubDocActualInstanceTypes):
        """ Attempts to replace an existing traffic data instance. """
        if not isinstance(newGroupInstance, SubDocActual_TrafficData_Instance):
            return False
        

class SubDocManifest(object):
    def __init__(self,
            subDocId: str,
            createTime: int,
            currentCapacity: int,
            lastEditted: int,
            status: SubDocStatus):
        """ Sub-document manifest class.

        Used to keep track of the status of specific sub documents.

        NOTE:
        This method has trouble with sparsification when users retract their
                comments, votes, etc.

        TODOS:
        Create a method to go through and refactor the manifests such that
        they're more tightly packed.

        ARGS:
        - subDocId: The id of the document in the subcollection under the 
                group name.
        - createTime: When the sub-document was first created.
        - currentCapacity: The current amount of data (in bytes) in the 
                sub-document.
        - lastEditted: When the sub-document was last editted.
        - status: The status of the sub-document. When it is full, the manifest
                status is set to 'READ_ONLY', when it is not, and is the current
                sub-document being written to, it is set to 'READ_AND_WRITE'.
        """
        # Preconditions
        # TODO

        self.__subDocId = subDocId
        self.__createTime = createTime
        self.__currentCapacity = currentCapacity
        self.__lastEditted = lastEditted
        self.__status = status

    def get_sub_doc_id(self):
        return self.__subDocId

    def set_sub_doc_id(self, newSubDocId: str):
        self.__subDocId = newSubDocId

    def get_create_time(self):
        return self.__createTime

    def set_create_time(self, newCreateTime: int):
        self.__createTime = newCreateTime

    def get_last_editted(self):
        return self.__lastEditted

    def set_last_editted(self, newLastEditted: int):
        self.__lastEditted = newLastEditted

    def get_current_capacity(self):
        return self.__currentCapacity

    def set_current_capacity(self, newCurrentCapacity: int):
        self.__currentCapacity = newCurrentCapacity

    def get_status(self):
        return self.__status

    def set_status(self, newStatus: SubDocStatus):
        self.__status = newStatus

    def to_dict(self):
        """ Returns the contents of the class as a dict. """
        return {
            'create_time': self.__createTime,
            'current_capacity': self.__currentCapacity,
            'last_editted': self.__lastEditted,
            'status': self.__status.value,
            'sub_doc_id': self.__subDocId
        }



# * MAIN CLASS DEFINITIONS -----------------------------------------------------

class SubDoc(object):
    def __init__(self,
            currentSubDocId: str,
            groupName: str,
            netCount: int,
            subDocManifests: List[SubDocManifest]):
        """ Sub-document inheritable.

        Used to manage the structure of sub-documents used by the parent doc.
        This is primarily used for storing things like comments, votes, and
        user activity logs (basically things that are unbounded in terms
        of size.)

        ARGS:
        - currentSubDocId: The current sub document that new writes to the
                group name are written.
        - groupName: The name of the group (i.e. 'votes', 'comments', etc).
        - netCount: The total count of all the 'things' in the sub document(s).
        - subDocManifests: The list of manifests that detail the status of
                each sub-document.
        """
        # Preconditions
        # TODO

        self.__currentSubDocId = currentSubDocId
        self.__groupName = groupName
        self.__netCount = netCount    
        self.__manifests = subDocManifests

    def get_current_sub_doc_id(self):
        return self.__currentSubDocId

    def get_sub_doc_manifest(self, subDocId: str):
        """ Returns the manifest that has the same id as the one provided. """
        for manifest in self.__manifests:
            if manifest.get_sub_doc_id() == subDocId:
                return manifest

    def set_current_sub_doc_id(self, newCurrentSubDocId: str):
        self.__currentSubDocId = newCurrentSubDocId

    def get_group_name(self):
        return self.__groupName

    def set_group_name(self, newGroupName: str):
        self.__groupName = newGroupName

    def get_net_count(self):
        return self.__netCount

    def set_net_count(self, newNetCount: int):
        self.__netCount = newNetCount

    def get_manifests(self):
        return self.__manifests

    def set_manifests(self, newManifests: List[SubDocManifest]):
        self.__manifests = newManifests

    def to_dict(self):
        """ Returns the contents of the class as a dict. """
        manifestsDictList = []
        for manifest in self.__manifests:
            manifestsDictList.append(manifest.to_dict())
        
        returnDict = {
            self.__groupName: {
                'current_{}_doc_id'.format(self.__groupName): \
                        self.__currentSubDocId,
                'net_{}_count'.format(self.__groupName): self.__netCount,
                'doc_manifests': manifestsDictList
            }
        }
            
        return returnDict
