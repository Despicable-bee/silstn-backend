# Standard Libs
from enum import Enum
from enum import auto
from typing import Dict
from typing import List
from typing import Optional
from datetime import datetime
import re
import os
import hashlib
import logging
import sys

# Third Party Libs

# Local Libs
from root.helpers.common_helper.common_firestore_helper import \
        FirestoreDocSizeHelper

from root.helpers.common_helper.common_firestore_helper import \
        FirestoreErrorHelper

from root.base_classes.base_doc import BaseDoc
from root.base_classes.base_doc import BaseDocType

# * DEBUG ----------------------------------------------------------------------

__filename__ = 'chunk_doc.py'

# * ENUMS ----------------------------------------------------------------------

class ChunkMode(Enum):
    def _generate_next_value_(name, start, count, last_values): # type: ignore
        """ Automatic numbering function. Use this to specify what 
        <Enum_instance>.value will return.
        """
        return count + 1
    NORMAL = auto()
    ON_THE_LINE_LHS = auto()
    ON_THE_LINE_RHS = auto()
    SPLIT = auto()

class ChunkManifestBoundsType(Enum):
    def _generate_next_value_(name, start, count, last_values): # type: ignore
        """ Automatic numbering function. Use this to specify what 
        <Enum_instance>.value will return.
        """
        return count + 1
    NORMAL = auto()
    SPECIAL = auto()

class SpecialChunkManifestConfigs(Enum):
    def _generate_next_value_(name, start, count, last_values): # type: ignore
        """ Automatic numbering function. Use this to specify what 
        <Enum_instance>.value will return.
        """
        return count + 1
    LHS_ONLY = auto()
    LHS_AND_SPLIT = auto()
    SPLIT_ONLY = auto()
    SPLIT_AND_RHS = auto()
    RHS_ONLY = auto()
    LHS_AND_RHS = auto()
    ALL = auto()

class ChunkType(Enum):
    def _generate_next_value_(name, start, count, last_values): # type: ignore
        """ Automatic numbering function. Use this to specify what 
        <Enum_instance>.value will return.
        """
        return count + 1
    FAR_CHUNK = auto()
    STANDARD_CHUNK = auto()
    NEAR_CHUNK = auto()
    CLOSE_CHUNK = auto()


# * SUPPORT CLASS DEFINITIONS --------------------------------------------------

class LatLngVertex(object):
    def __init__(self, lat: float, lng: float):
        self.__lat = lat
        self.__lng = lng
        pass

    def get_lat(self):
        return self.__lat

    def set_lat(self, newLat: float):
        self.__lat = newLat

    def get_lng(self): 
        return self.__lng

    def set_lng(self, newLng: float):
        self.__lng = newLng

    def to_dict(self):
        return {
            'lat': self.__lat,
            'lng': self.__lng
        }

class ChunkBoundingBox(object):
    def __init__(self, 
            topRightLat: float, 
            topRightLng: float, 
            bottomLeftLat: float, 
            bottomLeftLng: float):
        # Preconditions
        # TODO

        self.__topRightVert = LatLngVertex(lat=topRightLat, lng=topRightLng)
        self.__bottomLeftVert = LatLngVertex(lat=bottomLeftLat, 
                lng=bottomLeftLng)

    def get_top_right_vert(self):
        return self.__topRightVert

    def set_top_right_vert(self, newTopRightVert: LatLngVertex):
        self.__topRightVert = newTopRightVert

    def get_bottom_left_vert(self):
        return self.__bottomLeftVert
    
    def set_bottom_left_vert(self, newbottomLeftVert: LatLngVertex):
        self.__bottomLeftVert = newbottomLeftVert

    def to_dict(self):
        return {
            'top_right': self.__topRightVert.to_dict(),
            'bottom_left': self.__bottomLeftVert.to_dict()
        }

class DocManifest(object):
    def __init__(self, docId: str, 
            currentCapacity: int):
        
        self.__docId = docId
        self.__currentCapacity = currentCapacity

    def get_doc_id(self):
        return self.__docId

    def set_doc_id(self, newDocId: str):
        self.__docId = newDocId

    def get_current_capacity(self):
        return self.__currentCapacity

    def set_current_capacity(self, newCurrentCapacity: int):
        self.__currentCapacity = newCurrentCapacity

    def to_dict(self):
        return {
            self.__docId: {
                'current_capacity': self.__currentCapacity
            }            
        }

class ChunkManifestIndex(object):
    def __init__(self, 
            chunkGroupBoundingBox: ChunkBoundingBox,
            numChunks: int,
            docId: str,
            currentCapacity: int):
        """ Contains bounding information for an arbitrary number of chunks. 
        
        Each `ChunkDoc` will contain multiple chunks. 
        Groups of chunks will be tracked via their combined bounding box.
        This way we can read a single document, check to see which document
            contains which collection of chunks, and then read in the chunk
            documents we're intereseted in.  
        """
        self.__chunkBoundingBox = chunkGroupBoundingBox
        self.__numChunks = numChunks
        self.__docId = docId
        self.__currentCapacity = currentCapacity
        self.__boundsMode: ChunkMode

        self.__determine_bounds_mode()

    def get_bounds_mode(self):
        """ Returns the bounds mode of the chunk index. """
        return self.__boundsMode

    def set_bounds_mode(self, newBoundsMode: ChunkMode):
        self.__boundsMode = newBoundsMode

    def get_doc_id(self):
        return self.__docId

    def get_bounding_box(self):
        return self.__chunkBoundingBox

    def to_dict(self):
        """ Returns the contents of the object as a dictionary. """
        return {
            'group_bounds': self.__chunkBoundingBox.to_dict(),
            'num_chunks': self.__numChunks,
            'doc_id': self.__docId,
            'current_capacity': self.__currentCapacity
        }

    def __determine_bounds_mode(self):
        v1 = self.__chunkBoundingBox.get_top_right_vert()
        v2 = self.__chunkBoundingBox.get_bottom_left_vert()

        if v1.get_lng() == 180.0:
            # On the line LHS
            self.__boundsMode = ChunkMode.ON_THE_LINE_LHS
        elif v2.get_lng() == -180.0:
            # On the line RHS
            self.__boundsMode = ChunkMode.ON_THE_LINE_RHS
        elif v1.get_lng() < v2.get_lng():
            # Split
            self.__boundsMode = ChunkMode.SPLIT
        else:
            # Normal
            self.__boundsMode = ChunkMode.NORMAL

class ChunkManifestDoc(BaseDoc):
    def __init__(self,
            chunkManifests: List[ChunkManifestIndex],
            # BaseDoc 
            docType: BaseDocType, 
            createTime: int, 
            lastEditted: int, 
            ownerId: str, 
            # BaseDocVersion
            major: int, 
            minor: int, 
            patch: int):
        
        self.__chunkManifests = chunkManifests

        # Initialise base class
        BaseDoc.__init__(self, docType=docType, createTime=createTime, 
                lastEditted=lastEditted, ownerId=ownerId, major=major,
                minor=minor, patch=patch)
        
    def get_chunk_manifests(self):
        return self.__chunkManifests
    
    def add_chunk_manifest(self, manifestToAdd: ChunkManifestIndex):
        self.__chunkManifests.append(manifestToAdd)

    def set_chunk_manifests(self, newChunkManifests: List[ChunkManifestIndex]):
        self.__chunkManifests = newChunkManifests

    def to_dict(self):
        manifestsDictList = []
        for manif in self.__chunkManifests:
            manifestsDictList.append(manif.to_dict())

        return {
            'manifests': manifestsDictList
        } | BaseDoc.to_dict(self)

class Chunk(object):
    def __init__(self,
            lat1: float,
            lat2: float,
            lat3: float,
            lng1: float,
            lng2: float,
            lng3: float,
            intersectionsIdsList: List[str],
            chunkId: str):
        """ Contains data for a specific chunk """
        # Preconditions
        # TODO

        self.__vert1 = LatLngVertex(lat=lat1, lng=lng1)
        self.__vert2 = LatLngVertex(lat=lat2, lng=lng2)
        self.__vert3 = LatLngVertex(lat=lat3, lng=lng3)

        self.__chunkMode: Optional[ChunkMode] = None

        self.__determine_bounding_box()

        self.__intersectionsIdsList = intersectionsIdsList

        self.__chunkId = chunkId
    
    def get_chunk_id(self):
        return self.__chunkId

    def set_chunk_id(self, newChunkId: str):
        self.__chunkId = newChunkId

    def get_chunk_mode(self):
        return self.__chunkMode

    def set_chunk_mode(self, newChunkMode: ChunkMode):
        self.__chunkMode = newChunkMode

    def get_vertex_points(self):
        return (self.__vert1, self.__vert2, self.__vert3)

    def set_vertex_points(self, vert1: LatLngVertex, vert2: LatLngVertex, 
            vert3: LatLngVertex):
        self.__vert1 = vert1
        self.__vert2 = vert2
        self.__vert3 = vert3
    
    def get_bounding_box(self):
        return self.__boundingBox

    def set_bounding_box(self, newBoundingBox: ChunkBoundingBox):
        self.__boundingBox = newBoundingBox

    def get_intersections_ids_list(self):
        return self.__intersectionsIdsList

    def set_intersection_ids_list(self, newIntersectionIdsList: List[str]):
        self.__intersectionsIdsList = newIntersectionIdsList

    def to_dict(self):
        return {
            'vert_1': self.__vert1.to_dict(),
            'vert_2': self.__vert2.to_dict(),
            'vert_3': self.__vert3.to_dict(),
            'bounds': self.__boundingBox.to_dict(),
            'intersection_ids': self.__intersectionsIdsList,
            'chunk_id': self.__chunkId
        }

    def to_dict_verts_only(self):
        return {
            'vert_1': self.__vert1.to_dict(),
            'vert_2': self.__vert2.to_dict(),
            'vert_3': self.__vert3.to_dict()
        }

    def __determine_bounding_box(self):
        """ Determines the bounding box of the chunk. """
        if self.__vert1.get_lng() == 180.0 or \
                self.__vert2.get_lng() == 180.0 or \
                self.__vert3.get_lng() == 180.0:
            # It is either split or on the line
            lngs: List[float] = []
            counter = 0
            for vert in self.get_vertex_points():
                if vert.get_lng() == 180.0:
                    counter += 1
                lngs.append(vert.get_lng())
            
            if counter == 1:
                # Could be split or on-the-line
                # Remove the longitude on the line
                lngs.remove(180.0)
                if (lngs[0] >= 0 and lngs[1] >= 0):
                    # LHS
                    self.__compute_bounds(mode=ChunkMode.ON_THE_LINE_LHS)
                elif (lngs[0] < 0 and lngs[1] < 0):
                    # RHS
                    self.__compute_bounds(mode=ChunkMode.ON_THE_LINE_RHS)
                else:
                    # Split
                    self.__compute_bounds(mode=ChunkMode.SPLIT)
            else:
                # Must be on-the-line-LHS/RHS
                for vert in self.get_vertex_points():
                    if vert.get_lng() != 180:
                        if vert.get_lng() >= 0:
                            # LHS
                            self.__compute_bounds(
                                    mode=ChunkMode.ON_THE_LINE_LHS)
                        else:
                            # RHS
                            self.__compute_bounds(
                                    mode=ChunkMode.ON_THE_LINE_RHS)
        else:
            # It is a standard chunk, go ahead and compute bounds as per normal
            self.__compute_bounds(mode=ChunkMode.NORMAL)

    def __compute_bounds(self, mode: ChunkMode):
        """ Computes the bounds given a specific mode """
        lats: List[float] = []
        lngs: List[float] = []
        for vert in self.get_vertex_points():
            lats.append(vert.get_lat())
            lngs.append(vert.get_lng())

        maxLat = max(lats)
        maxLng = max(lngs)
        minLat = min(lats)
        minLng = min(lngs)
        if mode == ChunkMode.NORMAL or mode == ChunkMode.ON_THE_LINE_LHS:
            self.__boundingBox = ChunkBoundingBox(
                    topRightLat=maxLat,
                    topRightLng=maxLng,
                    bottomLeftLat=minLat,
                    bottomLeftLng=minLng)
        elif mode == ChunkMode.ON_THE_LINE_RHS:
            lngs.remove(180.0)
            self.__boundingBox = ChunkBoundingBox(
                    topRightLat=maxLat,
                    topRightLng=max(lngs),
                    bottomLeftLat=minLat,
                    bottomLeftLng=-180.0)
        elif mode == ChunkMode.SPLIT:
            lngs.remove(180.0)
            self.__boundingBox = ChunkBoundingBox(
                    topRightLat=maxLat,
                    topRightLng=min(lngs),
                    bottomLeftLat=minLat,
                    bottomLeftLng=max(lngs))
        else:
            raise Exception("Unknown chunk mode: {}".format(mode))

        self.__chunkMode = mode

# * MAIN CLASS DEFINITION ------------------------------------------------------

class ChunkDoc(BaseDoc):
    def __init__(self,
            # class specific
            chunkType: ChunkType,
            chunks: List[Chunk],
            bounds: ChunkBoundingBox,
            boundsType: ChunkManifestBoundsType,
            # BaseDoc 
            docType: BaseDocType, 
            createTime: int, 
            lastEditted: int, 
            ownerId: str, 
            # BaseDocVersion
            major: int, 
            minor: int, 
            patch: int,
            currentCapacity: Optional[int]=None):
        self.__chunkType = chunkType
        self.__chunks = chunks
        self.__bounds = bounds
        self.__classname__ = 'ChunkDoc'
        self.__boundsType = boundsType
        
        
        # Initialise base class
        BaseDoc.__init__(self, docType=docType, createTime=createTime, 
                lastEditted=lastEditted, ownerId=ownerId, major=major,
                minor=minor, patch=patch)

        try:
            if isinstance(currentCapacity, int):
                self.__currentCapacity = currentCapacity
            elif currentCapacity == None:
                fdsh = FirestoreDocSizeHelper()
                self.__currentCapacity = 0
                docAbsPath = self.__get_doc_abs_path()

                self.__currentCapacity = fdsh.firestore_doc_size_calc(
                        docAbsPath=docAbsPath, 
                        docDict=self.to_dict(),
                        docName=BaseDoc.get_owner_id(self))
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            feh = FirestoreErrorHelper()
            feh.class_error_handler(fileName=__filename__,
                    className=self.__classname__,
                    methodName='init_database', 
                    exceptionMsg=e,
                    lineNum=exc_tb.tb_lineno)   # type: ignore
            raise Exception(e)

        

    def get_num_chunks(self):
        return len(self.__chunks)

    def get_chunk_type(self):
        return self.__chunkType

    def set_chunk_type(self, newChunkType: ChunkType):
        self.__chunkType = newChunkType

    def get_chunks(self):
        return self.__chunks

    def add_chunk(self, chunkToAdd: Chunk):
        """ Attempts to add a chunk to the list. 
        
        If the document can fit the chunk in, then the method will return `True`
        If the document can no longer accomodate additional chunks (due to the
            1 MiB size limit on firestore docs), the the method will return 
            `False`
        """
        fdsh = FirestoreDocSizeHelper()
        docAbsPath = self.__get_doc_abs_path()

        capacityToAdd = fdsh.firestore_doc_size_calc(docAbsPath=docAbsPath,
            docDict=chunkToAdd.to_dict(),
            docName=BaseDoc.get_owner_id(self))

        # Check if capacity can be afforded (60% of max size as to leave room 
        #       for intersections)
        if (capacityToAdd + self.__currentCapacity) > \
                fdsh.get_firestore_doc_max_size() * 0.75:
            return False
        
        # Add the chunk to the list
        self.__currentCapacity += capacityToAdd
        self.__chunks.append(chunkToAdd)

        # Compute the new bounds
        if self.__boundsType == ChunkManifestBoundsType.NORMAL:
            self.__compute_bounds_normal()
        else:
            self.__compute_bounds_special()
        
        return True

    def get_bounds_type(self):
        return self.__boundsType

    def set_chunks(self, newChunks: List[Chunk]):
        self.__chunks = newChunks

    def get_bounds(self):
        return self.__bounds

    def set_bounds(self, newBounds: ChunkBoundingBox):
        self.__bounds = newBounds

    def get_current_capacity(self):
        return self.__currentCapacity

    def set_current_capacity(self, newCurrentCapacity: int):
        self.__currentCapacity = newCurrentCapacity

    def to_dict(self):
        chunksDictList = []
        for chunk in self.__chunks:
            chunksDictList.append(chunk.to_dict())

        return {
            'chunk_type': self.__chunkType.value,
            'chunks': chunksDictList,
            'bounds': self.__bounds.to_dict(),
            'current_capacity': self.__currentCapacity,
            'bounds_type': self.__boundsType.value
        } | BaseDoc.to_dict(self)
    
    def __get_doc_abs_path(self):
        if self.__chunkType == ChunkType.FAR_CHUNK:
            return "chunks/far_chunks"
        elif self.__chunkType == ChunkType.STANDARD_CHUNK:
            return "chunks/standard_chunks"
        elif self.__chunkType == ChunkType.NEAR_CHUNK:
            return "chunks/near_chunks"
        else:
            return "chunks/close_chunks"

    def __compute_bounds_normal(self):
        """ Computes the bounds for the group of chunks. 
        
        NOTE:
        This method is only to be called if the `chunkType` is set to `NORMAL`
        """
        topRightLats: List[float] = []
        topRightLngs: List[float] = []
        bottomLeftLat: List[float] = []
        bottomLeftlngs: List[float] = []
        for chunk in self.__chunks:
            currentTopRight = chunk.get_bounding_box().get_top_right_vert()
            currentBottomLeft = chunk.get_bounding_box().get_bottom_left_vert()
            topRightLats.append(currentTopRight.get_lat())
            topRightLngs.append(currentTopRight.get_lng())
            bottomLeftLat.append(currentBottomLeft.get_lat())
            bottomLeftlngs.append(currentBottomLeft.get_lng())

        self.set_bounds(ChunkBoundingBox(
            topRightLat=max(topRightLats),
            topRightLng=max(topRightLngs),
            bottomLeftLat=min(bottomLeftLat),
            bottomLeftLng=min(bottomLeftlngs)))

    def __compute_bounds_special(self):
        """ Computes the bounds for the group of chunks in the special zone. 
        
        This manifest will act as a collection of all chunks that are:
        - on-the-line
        - split

        NOTE:
        This method is only to be called if the `chunkType` is set to `SPECIAL`
        """
        hasOnTheLineLHS: bool = False
        hasSplit: bool = False
        hasOnTheLineRHS: bool = False

        lhsTopRightLats: List[float] = []
        lhsTopRightLngs: List[float] = []
        lhsBottomLeftLats: List[float] = []
        lhsBottomLeftLngs: List[float] = []

        rhsTopRightLats: List[float] = []
        rhsTopRightLngs: List[float] = []
        rhsBottomLeftLats: List[float] = []

        splitTopRightLats: List[float] = []
        splitTopRightLngs: List[float] = []
        splitBottomLeftLats: List[float] = []
        splitBottomLeftLngs: List[float] = []


        for chunk in self.__chunks:
            currentTopRight = chunk.get_bounding_box().get_top_right_vert()
            currentBottomLeft = chunk.get_bounding_box().get_bottom_left_vert()
            if chunk.get_chunk_mode() == \
                    ChunkMode.ON_THE_LINE_LHS:
                # if currentBottomLeft.get_lng() == 0:
                #     print("Current Bottom Left LHS: {}".format(chunk.get_chunk_id()))
                #     print("{}".format(chunk.to_dict_verts_only()))
                #     print("")
                lhsTopRightLats.append(currentTopRight.get_lat())
                lhsTopRightLngs.append(currentTopRight.get_lng())
                lhsBottomLeftLats.append(currentBottomLeft.get_lat())
                lhsBottomLeftLngs.append(currentBottomLeft.get_lng())
            elif chunk.get_chunk_mode() == \
                    ChunkMode.ON_THE_LINE_RHS:
                # if currentTopRight.get_lng() == 180:
                #     print("Current Top Right RHS: {}".format(chunk.get_chunk_id()))
                #     print("{}".format(chunk.to_dict_verts_only()))
                #     print("")
                rhsTopRightLats.append(currentTopRight.get_lat())
                rhsTopRightLngs.append(currentTopRight.get_lng())
                rhsBottomLeftLats.append(currentBottomLeft.get_lat())
                
            elif chunk.get_chunk_mode() == ChunkMode.SPLIT:
                # if currentTopRight.get_lng() == 180:
                #     print("Current Top Right Split: {}".format(chunk.get_chunk_id()))
                #     print("{}".format(chunk.to_dict_verts_only()))
                #     print("")
                # if currentBottomLeft.get_lng() == 0:
                #     print("Current Bottom Left Split: {}".format(chunk.get_chunk_id()))
                #     print("{}".format(chunk.to_dict_verts_only()))
                #     print("")
                splitTopRightLats.append(currentTopRight.get_lat())
                splitTopRightLngs.append(currentTopRight.get_lng())
                splitBottomLeftLats.append(currentBottomLeft.get_lat())
                splitBottomLeftLngs.append(currentBottomLeft.get_lng())
        
        # If any of the four lists are filled with something, then it means we
        #   found a case where the chunk type is true.
        if len(lhsTopRightLats) > 0:
            hasOnTheLineLHS = True

        if len(rhsTopRightLats) > 0:
            hasOnTheLineRHS = True
        
        if len(splitTopRightLats) > 0:
            hasSplit = True

        # LHS only
        if hasOnTheLineLHS and not hasSplit and not hasOnTheLineRHS:
            self.set_bounds(ChunkBoundingBox(
                    topRightLat=max(lhsTopRightLats),
                    topRightLng=max(lhsTopRightLngs),
                    bottomLeftLat=min(lhsBottomLeftLats),
                    bottomLeftLng=min(lhsBottomLeftLngs)))

        # RHS only
        if not hasOnTheLineLHS and not hasSplit and hasOnTheLineRHS:
            self.set_bounds(ChunkBoundingBox(
                    topRightLat=max(rhsTopRightLats),
                    topRightLng=max(rhsTopRightLngs),
                    bottomLeftLat=min(rhsBottomLeftLats),
                    bottomLeftLng=180.0))

        # LHS and split
        if hasOnTheLineLHS and hasSplit and not hasOnTheLineRHS:
            self.set_bounds(ChunkBoundingBox(
                    topRightLat=max([max(lhsTopRightLats), max(splitTopRightLats)]),
                    topRightLng=max(splitTopRightLngs),
                    bottomLeftLat=min([min(lhsBottomLeftLats),min(splitBottomLeftLats)]),
                    bottomLeftLng=min([min(lhsBottomLeftLngs), min(splitBottomLeftLngs)])))

        # Split only
        if not hasOnTheLineLHS and hasSplit and not hasOnTheLineRHS:
            self.set_bounds(ChunkBoundingBox(
                    topRightLat=max(splitTopRightLats),
                    topRightLng=max(splitTopRightLngs),
                    bottomLeftLat=min(splitBottomLeftLats),
                    bottomLeftLng=min(splitBottomLeftLngs)))

        # Split and RHS
        if not hasOnTheLineLHS and hasSplit and hasOnTheLineRHS:
            self.set_bounds(ChunkBoundingBox(
                    topRightLat=max([max(rhsTopRightLats), max(splitTopRightLats)]),
                    topRightLng=max([max(rhsTopRightLngs), max(splitTopRightLngs)]),
                    bottomLeftLat=min([min(splitBottomLeftLats), min(rhsBottomLeftLats)]),
                    bottomLeftLng=min(splitBottomLeftLngs)))

        # LHS and RHS
        if hasOnTheLineLHS and not hasSplit and hasOnTheLineRHS:
            self.set_bounds(ChunkBoundingBox(
                    topRightLat=max([max(rhsTopRightLats), max(lhsTopRightLats)]),
                    topRightLng=max(rhsTopRightLngs),
                    bottomLeftLat=min([min(lhsBottomLeftLats), min(rhsBottomLeftLats)]),
                    bottomLeftLng=min(lhsBottomLeftLngs)))

        # All
        if hasOnTheLineLHS and hasSplit and hasOnTheLineRHS:

            self.set_bounds(ChunkBoundingBox(
                    topRightLat=max([max(rhsTopRightLats), max(lhsTopRightLats), max(splitTopRightLats)]),
                    topRightLng=max([max(splitTopRightLngs), max(rhsTopRightLngs)]),
                    bottomLeftLat=min([min(lhsBottomLeftLats), min(rhsBottomLeftLats), min(splitBottomLeftLats)]),
                    bottomLeftLng=min([min(lhsBottomLeftLngs), min(splitBottomLeftLngs)])))

