# Standard libs
from enum import Enum
from enum import auto
from typing import Dict
from typing import Tuple

# * ENUMS ----------------------------------------------------------------------

class BaseDocType(Enum):
    def _generate_next_value_(name, start, count, last_values): # type: ignore
        """ Automatic numbering function. Use this to specify what 
        <Enum_instance>.value will return.
        """
        return count + 1
    USER_PROFILE = auto()

    FAR_CHUNK_MANIFEST = auto()
    STANDARD_CHUNK_MANIFEST = auto()
    NEAR_CHUNK_MANIFEST = auto()
    CLOSE_CHUNK_MANIFEST = auto()

    FAR_CHUNK_DOC = auto()
    STANDARD_CHUNK_DOC = auto()
    NEAR_CHUNK_DOC = auto()
    CLOSE_CHUNK_DOC = auto()
    
    INTERSECTIONS_DOC = auto()
    INTERSECTIONS_DOC_MANIFEST = auto()
    
    INTERSECTION_DATA_DOC = auto()
    INTERSECTION_DATA_DOC_MANIFEST = auto()



class BaseDocVersionAsserts(Enum):
    def _generate_next_value_(name, start, count, last_values): # type: ignore
        """ Automatic numbering function. Use this to specify what 
        <Enum_instance>.value will return.
        """
        return name
    MAJOR_VERSION_INVALID = auto()
    MINOR_VERSION_INVALID = auto()
    PATCH_VERSION_INVALID = auto()

class BaseDocAsserts(Enum):
    def _generate_next_value_(name, start, count, last_values): # type: ignore
        """ Automatic numbering function. Use this to specify what 
        <Enum_instance>.value will return.
        """
        return name
    DOCTYPE_INVALID = auto()
    CREATE_TIME_INVALID = auto()
    LAST_EDITTED_INVALID = auto()
    OWNER_INVALID = auto()

# * SUPPORT CLASS DEFINITIONS --------------------------------------------------

class BaseDocVersion(object):
    def __init__(self, major: int, minor: int, patch: int):
        """ Base document version class. 
        
        Utilised by the BaseDoc in order to facilitate versioning of 
        firestore documents.

        Note, defensive programming is a must, hence we include preconditions.
        """
        # Preconditions
        assert major >= 0, BaseDocVersionAsserts.MAJOR_VERSION_INVALID.value
        assert minor >= 0, BaseDocVersionAsserts.MINOR_VERSION_INVALID.value
        assert patch >= 0, BaseDocVersionAsserts.PATCH_VERSION_INVALID.value

        self.__major = major
        self.__minor = minor
        self.__patch = patch
    
    def get_doc_version(self) -> Tuple[int, int, int]:
        """ Returns a tuple containing the version info in a semantic format. 
        
        e.g.
        - (major).(minor).(patch)
        - 4.2.0
        """
        return (self.__major, self.__minor, self.__patch)

    def set_doc_version(self, major: int, minor: int, patch: int) -> None:
        """ Sets the version of the document. """
        # Preconditions
        assert major >= self.__major, """major version must be greater than, 
                or equal to its previous value"""

        if major >= self.__major:
            assert minor > 0, "minor version must be greater than zero"
        else:
            assert minor >= self.__minor, """minor version must be greater than, 
                    or equal to its previous value"""

        if minor > self.__minor or major > self.__major:
            assert patch > 0, "patch version must be greater than zero"
        else:
            assert patch > self.__patch, """patch version must be greater its 
                    previous value"""

        self.__major = major
        self.__minor = minor
        self.__patch = patch

    def to_dict(self) -> Dict[str, str]:
        """ Returns the contents of the class as a dictionary """
        return {
            'doc_version': "{}.{}.{}".format(self.__major, self.__minor, 
                    self.__patch)
        }

# * MAIN CLASS DEFINITION ------------------------------------------------------

class BaseDoc(BaseDocVersion):
    def __init__(self, 
            # Class specific
            docType: BaseDocType, 
            createTime: int, 
            lastEditted: int, 
            ownerId: str, 
            # BaseDocVersion
            major: int, 
            minor: int, 
            patch: int):
        """ Base document class. 
        
        Utilised in every firestore document (in a vulkan style).
        Primary purpose is:
        - version tracking
        - identification
        - timestamping

        ARGS:
        - docType: The 
        """
        # Preconditions
        assert isinstance(docType, BaseDocType), BaseDocAsserts.\
                DOCTYPE_INVALID.value

        assert createTime >= 0, BaseDocAsserts.CREATE_TIME_INVALID.value
        
        assert lastEditted >= 0, BaseDocAsserts.LAST_EDITTED_INVALID.value
        
        assert ownerId != None, BaseDocAsserts.OWNER_INVALID.value

        # Initialise the rest of the class
        self.__docType = docType
        self.__createTime = createTime
        self.__lastEditted = lastEditted
        self.__ownerId = ownerId

        # Initialise the base doc version
        BaseDocVersion.__init__(self, major, minor, patch)
    
    def get_create_time(self) -> int:
        """ Returns the create time. """
        return self.__createTime

    def get_last_editted(self) -> int:
        """ Returns the last editted time. """
        return self.__lastEditted

    def set_last_editted(self, newLastEditted: int) -> None:
        """ Sets the last editted time. """
        assert newLastEditted > self.__lastEditted, """Updated last editted
                time must be greater than the current last editted time"""
        self.__lastEditted = newLastEditted
    
    def get_doc_type(self) -> BaseDocType:
        """ Returns the document type. """
        return self.__docType
    
    def set_doc_type(self, newDocType: BaseDocType) -> None:
        """ Sets the doc type. """
        assert isinstance(newDocType, BaseDocType), """ newDocType must be of 
                type BaseDocType."""
        self.__docType = newDocType

    def get_owner_id(self) -> str:
        """ Returns the owner id. """
        return self.__ownerId

    def set_owner_id(self, newOwnerId: str) -> None:
        """ Sets the owner id for the document. """
        assert newOwnerId != None, """New owner id must be specified."""

    def to_dict(self) -> Dict:
        """ Returns a dictionary containing the contents of the class. """
        return {
            'doc_type': self.__docType.value,
            'create_time': self.__createTime, 
            'last_editted': self.__lastEditted,
            'owner_id': self.__ownerId
        } | BaseDocVersion.to_dict(self)