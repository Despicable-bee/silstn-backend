class Coordinates(object):
    def __init__(self, latitude: float, longitude: float):
        self.__latitude = latitude
        self.__longitude = longitude

    def get_latitude(self):
        return self.__latitude

    def set_latitude(self, newLatitude: float):
        self.__latitude = newLatitude

    def get_longitude(self):
        return self.__longitude
    
    def set_longitude(self, newLongitude: float):
        self.__longitude = newLongitude

    def to_dict(self):
        """ Returns the contents of the object as a dictionary. 
        
        ## OUTPUT

        ```
        { 'lat': float, 'lng': float }
        ```
        """
        return {
            'lat': self.__latitude,
            'lng': self.__longitude
        }
