# Standard Libs
from enum import Enum
from enum import auto
from typing import Dict
from typing import List
from typing import Optional
from datetime import datetime
import re
import os
import hashlib
import logging

# Third party libs
from email_validator import validate_email, EmailNotValidError

# local Libs
from root.base_classes.base_doc import BaseDoc
from root.base_classes.base_doc import BaseDocType

# * ENUMS ----------------------------------------------------------------------

class UserProfileDocAsserts(Enum):
    def _generate_next_value_(name, start, count, last_values): # type: ignore
        """ Automatic numbering function. Use this to specify what 
        <Enum_instance>.value will return.
        """
        return "UserProfileDocAssert: {}".format(name)
    EMAIL_FORMAT_INVALID = auto()
    DOB_INVALID = auto()
    FIRSTNAME_INVALID = auto()
    SURNAME_INVALID = auto()

class UserProfilePasswordAsserts(Enum):
    def _generate_next_value_(name, start, count, last_values): # type: ignore
        """ Automatic numbering function. Use this to specify what 
        <Enum_instance>.value will return.
        """
        return "UserProfilePasswordAssert: {}".format(name)
    PASSWORD_MUST_BE_DEFINED = auto()
    # Invalid password, password must contain a minimum of 8 
    # chars, at least one uppercase char, one lowercase char,
    # one number and one special char.
    INVALID_PASSWORD_FORMAT = auto()

# * AUXILIARY CLASS DEFINTIONS -------------------------------------------------

class DateTimeHelper(object):
    def __init__(self):
        self.__formatDateString = "%d/%m/%Y"
        pass
    
    def get_formatting_string(self) -> str:
        return self.__formatDateString

    def validate_datetime_basic(self, inputDateStr: str) -> bool:
        """ Checks if a given string is 'dd/mm/YYYY' formatting. 
        
        e.g.
        >>> __validate_datetime_basic('2017-01-01')
        True

        >>> __validate_datetime_basic('2008-08-30T01:45:36.123Z')
        False
        """
        
        try:
            datetime.strptime(inputDateStr, self.__formatDateString)
            return True
        except ValueError:
            return False

class UserProfilePassword(object):
    def __init__(self, 
            saltPasswordHash: Optional[bytes]=None, 
            passwordPlainText: Optional[str]=None):

        # Minimum 8 chars, at least one uppercase char, one lowercase char,
        #   one number and one special char.
        self.__regex = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&-_])[A-Za-z\d@$!%*?&-_]{8,}$"
        self.__pattern = re.compile(self.__regex)
        
        # Length of the salt bytes sequence
        self.__saltLength = 32

        if saltPasswordHash == None:
            assert passwordPlainText != None, UserProfilePasswordAsserts.\
                    PASSWORD_MUST_BE_DEFINED.value
            assert self.validate_plaintext_password(passwordPlainText), \
                    UserProfilePasswordAsserts.INVALID_PASSWORD_FORMAT.value
            self.__hash_plaintext_password(passwordPlainText)
        else:
            self.__salt = saltPasswordHash[:self.__saltLength]
            self.__key = saltPasswordHash[self.__saltLength:]

    # * PUBLIC METHODS ---------------------------------------------------------

    def set_new_password(self, newPlainTextPassword: str) -> None:
        """ Overwrites the password with a new one. """
        assert self.validate_plaintext_password(newPlainTextPassword), \
                UserProfilePasswordAsserts.INVALID_PASSWORD_FORMAT.value
        self.__hash_plaintext_password(newPlainTextPassword)

    def validate_plaintext_password(self, passwordToValidate: str) -> bool:
        """ Validates the plaintext password using the above regex conditions. 
        """
        mat = re.search(self.__pattern, passwordToValidate)
        if mat:
            return True
        else:
            return False

    def check_hashed_password_matches(self, passwordToCheck: str) -> bool:
        """ Checks if a given plain-text password matches the password hash. 
        
        This method takes the plain text password, hashes it using the salt
        data retrieved from firestore, and compares the byte keys.

        ARGS:
        - passwordToCheck: The plain text password provided by the user.
        
        RETURNS:
        - On success (i.e. the byte keys match), the method returns True.
                Otherwise returns False.
        """
        newKey = self.__generate_key(passwordToCheck, self.__salt)
        return newKey == self.__key

    def to_dict(self) -> Dict:
        """ Returns the contents of the class as a dictionary. """
        return {
            'password': self.__salt + self.__key
        }

    # * PRIVATE METHODS --------------------------------------------------------

    def __hash_plaintext_password(self, passwordPlainText: str) -> None:
        """ Hashes the plain text password using SHA512.

        NOTE
        This should only be done when first creating the users profile.

        ARGS:
        - passwordPlainText: The plain text password provided by the user.

        RETURNS:
        - salt + SHA512 hashed key. The first 32 bytes is the salt, the 
            remaining chars are the key.
        """
        # Generate a random 32 byte string
        self.__salt = os.urandom(self.__saltLength)
        self.__key = self.__generate_key(passwordPlainText, self.__salt)

    def __generate_key(self, passwordPlainText: str, salt: bytes) -> bytes:
        """ Generates the key (password) from a given salt bytes array. """
        return hashlib.pbkdf2_hmac(
            hash_name='sha512', # The hash digest algorithm for HMAC
            password=passwordPlainText.encode('utf-8'), # Convert the password to bytes
            salt=salt, # Provide the salt
            iterations=200000, # Recommended at least 100,000 iterations
            dklen=128 # Get a 128 byte key
        )

# * MAIN CLASS DEFINITION ------------------------------------------------------

class UserProfileDoc(BaseDoc, UserProfilePassword):
    def __init__(self,
            # class specific
            email: str,
            emailVerified: bool,
            dob: str,
            firstname: str,
            surname: str,
            refreshToken: str,
            # BaseDoc
            docType: BaseDocType, 
            createTime: int, 
            lastEditted: int, 
            ownerId: str, 
            # BaseDocVersion
            major: int, 
            minor: int, 
            patch: int,
            # Password
            saltPasswordHash: Optional[bytes]=None,
            passwordPlainText: Optional[str]=None):
        self.dth = DateTimeHelper()
        
        # Preconditions
        try:
            validEmail = validate_email(email)
        except EmailNotValidError as e:
            assert False, UserProfileDocAsserts.EMAIL_FORMAT_INVALID.value

        assert self.dth.validate_datetime_basic(dob), \
                UserProfileDocAsserts.DOB_INVALID.value
        
        assert firstname != "", UserProfileDocAsserts.FIRSTNAME_INVALID
        assert surname != "", UserProfileDocAsserts.SURNAME_INVALID

        # Initialise base class
        BaseDoc.__init__(self, docType=docType, createTime=createTime, 
                lastEditted=lastEditted, ownerId=ownerId, major=major,
                minor=minor, patch=patch)

        # Initialise the password
        UserProfilePassword.__init__(self, saltPasswordHash=saltPasswordHash, 
                passwordPlainText=passwordPlainText)

        self.__email = validEmail.email # normalised email
        self.__emailVerified = emailVerified
        self.__dob = dob
        self.__firstname = firstname
        self.__surname = surname
        self.__refreshToken = refreshToken

    def get_refresh_token(self):
        return self.__refreshToken

    def set_refresh_token(self, newRefreshToken: str):
        self.__refreshToken = newRefreshToken

    def get_email(self) -> str:
        return self.__email # type: ignore

    def set_email(self, newEmail: str) -> None:
        try:
            validEmail = validate_email(newEmail)
        except EmailNotValidError as e:
            assert False, "Email format is invalid"

    def get_firstname(self):
        return self.__firstname
    
    def set_firstname(self, newFirstname: str):
        self.__firstname = newFirstname

    def get_surname(self):
        return self.__surname

    def set_surname(self, newSurname: str):
        self.__surname = newSurname

    def to_dict(self):
        """ Returns the contents of the class as a dictionary. """
        return {
            'email': self.__email,
            'email_verified': self.__emailVerified,
            'dob': self.__dob,
            'firstname': self.__firstname,
            'surname': self.__surname,
            'refresh_token': self.__refreshToken
        } | BaseDoc.to_dict(self) | \
            UserProfilePassword.to_dict(self)

