"""
	FILENAME:		login.py

	AUTHOR(s):		

	DESCRIPTION:		

	VERSION:		0.0.1

	DESTINATION:		

	LICENSE:		Property of Starfighter Australia Pty Ltd
"""
# Standard Library
from concurrent import futures
import logging
import os
from typing import Text
import sys

# Third Party Library
import grpc

# Local Library
from root.api.login.generated import login_pb2_grpc as pb2_grpc
from root.api.login.generated import login_pb2 as pb2

from root.helpers.user_profile.user_profile_firestore_helper import \
		UserProfileFirestoreHelper

from root.helpers.jwt_helper.jwt_helper import JwtHelper
from root.helpers.jwt_helper.jwt_helper import JwtClaims
from root.helpers.jwt_helper.jwt_helper import JwtRoles
from root.helpers.jwt_helper.jwt_helper import JwkStorageHelper
from root.helpers.jwt_helper.jwt_helper import OldJwtCheckStatus

from root.helpers.common_helper.common_firestore_helper import \
		FirestoreTokenHelper

from root.helpers.common_helper.common_firestore_helper import \
		FirestoreErrorHelper

from root.base_classes.user_profile_doc import UserProfileDoc

_PORT = os.environ["PORT"]

# * DEBUG ----------------------------------------------------------------------

__filename__ = 'login.py'

# * MAIN CLASS -----------------------------------------------------------------

class Login(pb2_grpc.LoginServicer):
	def user_login(self,
			request: pb2.User_Login_Request,
			context: grpc.ServicerContext) -> pb2.User_Login_Response:
		""" Logs the user in, given the correct credentials

		ARGS:
		- request: Contains the parameters to log the user in either by
			jwt (JWT + refresh token) or form (email + password).

		RETURNS:
		- On success, the method returns a JWT and a refresh token. Otherwise
			returns an error status.
		"""
		jwth = JwtHelper()
		upfh = UserProfileFirestoreHelper()
		fth = FirestoreTokenHelper()
		jwksh = JwkStorageHelper() 
		feh = FirestoreErrorHelper()
		# uphc = UserProfileFirestoreHelperCOMMON()
		try:
			# * Login via form
			if request.loginType == pb2.LOGIN_W_FORM: # type: ignore
				# Check whether email and password are valid
				result = upfh.check_user_login_info(
					request.email,			# type: ignore
					request.password		# type: ignore
				)

				# Check the result
				if isinstance(result, bool) or result == None:
					# Something went wrong, return an error
					# ! NOTE
					# The cause of the error is purposely left vague to deter
					# malicious actors.
					return pb2.User_Login_Response(
						status=int(pb2.UL_FAILURE),
						errMsg="Email or password incorrect."
					)

				# Provided information is valid, generate the JWT + refresh
				jwt = jwth.generate_jwt(roles=[JwtRoles.USER],
						audience=['User'], identifier=result);
				
				refresh = fth.generate_refresh_token();
				
				# Check if refresh token was generated successfully.
				if refresh == None:
					return pb2.User_Login_Response(
						status=int(pb2.UL_ERROR),
						errMsg=feh.get_server_error_message()
					)

				# Save refresh to firestore
				userProfile = upfh.get_user_profile(result)	# type: ignore

				if isinstance(userProfile, bool):
					return pb2.User_Login_Response(
						status=int(pb2.UL_FAILURE),
						errMsg="User does not exist"
					)
				
				if userProfile == None:
					return pb2.User_Login_Response(
						status=int(pb2.UL_FAILURE),
						errMsg=feh.get_server_error_message()
					)

				if isinstance(userProfile, str):
					return pb2.User_Login_Response(
						status=int(pb2.UL_FAILURE),
						errMsg=userProfile)
				
				userProfile.set_refresh_token(refresh)

				upfh.update_user_profile(userProfile)

				# Check if the generation was successful
				if isinstance(refresh, str):
					return pb2.User_Login_Response(
						status=pb2.UL_SUCCESSFUL,
						jwt=jwt,
						refresh=refresh
					)
				
				else:
					# TODO: Error handling
					return pb2.User_Login_Response(
						status=int(pb2.UL_ERROR),
						errMsg=feh.get_server_error_message()
					)
				

			# * Login via JWT
			elif request.loginType == pb2.LOGIN_W_JWT: # type: ignore
				# Check the old jwt
				jwtResult = jwksh.check_old_jwt(
						request.jwtOld) # type: ignore
				
				# Check for errors
				if jwtResult == OldJwtCheckStatus.JWT_INVALID:
					return pb2.User_Login_Response(
						status=int(pb2.UL_ERROR),
						errMsg="Invalid JWT signature")

				if jwtResult == OldJwtCheckStatus.JWT_ERROR:
					return pb2.User_Login_Response(
						status=int(pb2.UL_ERROR),
						errMsg=feh.get_server_error_message())

				# Check if JWT still valid
				jwtStatus = jwtResult.get_status()	# type: ignore
				if jwtStatus == OldJwtCheckStatus.JWT_VALID:
					# JWT is still valid, so just return it
					return pb2.User_Login_Response(
						status=int(pb2.UL_SUCCESSFUL),
						jwt=request.jwtOld,		# type: ignore
						refresh=request.refreshOld # type: ignore
					)
					

				elif jwtStatus == OldJwtCheckStatus.JWT_EXPIRED:
					# Authenticate refresh token
					userProfile = upfh.get_user_profile(
							jwtResult.get_subject()) # type: ignore
					
					if not isinstance(userProfile, UserProfileDoc):
						return pb2.User_Login_Response(
								status=int(pb2.UL_ERROR),
								errMsg="User does not exist")
					
					if userProfile.get_refresh_token() != \
							request.refreshOld:	# type: ignore
						return pb2.User_Login_Response(
								status=int(pb2.UL_ERROR),
								errMsg="Refresh token does not match")
					
					# Refresh token matches, generate new JWT
					newJwt = jwth.generate_jwt(
							oldJwtClaims=jwtResult.to_dict()) # type: ignore

					# Return the JWT and old refresh to the user
					return pb2.User_Login_Response(
							status=int(pb2.UL_SUCCESSFUL),
							jwt=newJwt,
							refresh=request.refreshOld)	# type: ignore
				else:
					# Unknown status
					return pb2.User_Login_Response(
							status=int(pb2.UL_ERROR),
							errMsg="Unknown JWT status")
			else:
				return pb2.User_Login_Response(
						status=int(pb2.UL_ERROR),
						errMsg="Unknown Login Option")

		except Exception as e:
			exc_type, exc_obj, exc_tb = sys.exc_info()
			feh = FirestoreErrorHelper()
			feh.api_error_handler(fileName=__filename__, 
					methodName='user_login', 
					exceptionMsg=e,
					lineNum=exc_tb.tb_lineno)   # type: ignore
			return pb2.User_Login_Response(
						status=int(pb2.UL_ERROR),
						errMsg=feh.get_server_error_message())

	def user_create_account(self,
			request: pb2.User_Create_Account_Request,
			context: grpc.ServicerContext) -> pb2.User_Create_Account_Response:
		""" Creates a new user profile.

		ARGS:
		- request: Contains the form elements required for creating a new user
				profile (see login.proto)

		RETURNS:
		- On success, returns a JWT + refresh token, otherwise returns ERROR.
		"""

		jwth = JwtHelper()
		upfh = UserProfileFirestoreHelper()
		feh = FirestoreErrorHelper()

		try:
			profileResult = upfh.create_new_user_profile(
					email=request.email,			# type: ignore
					password=request.password,		# type: ignore
					firstname=request.firstname,	# type: ignore
					surname=request.surname,		# type: ignore
					dateOfBirth=request.dob			# type: ignore
					)
			# Check input is correct
			if isinstance(profileResult, bool):
				return pb2.User_Create_Account_Response(
					status=pb2.UCA_INVALID_INPUT,
					errMsg="Email or mobile number already exists.")

			if isinstance(profileResult, str):
				# An assert was fired when trying to initialise the 
				# 	UserProfileDoc
				return pb2.User_Create_Account_Response(
					status=pb2.UCA_INVALID_INPUT,
					errMsg=profileResult)

			if profileResult == None:
				return pb2.User_Create_Account_Response(
					status=pb2.UCA_ERROR,
					errMsg=feh.get_server_error_message())

			# Profile creation successful, generate JWT
			newJwt = jwth.generate_jwt(roles=[JwtRoles.USER],
					audience=['User'], identifier=profileResult[0])

			return pb2.User_Create_Account_Response(
					status=pb2.UCA_SUCCESSFUL,
					jwt=newJwt,
					refresh=profileResult[1])

		except Exception as e:
			exc_type, exc_obj, exc_tb = sys.exc_info()
			feh = FirestoreErrorHelper()
			feh.api_error_handler(fileName=__filename__,
					methodName='user_create_account', 
					exceptionMsg=e,
					lineNum=exc_tb.tb_lineno)   # type: ignore
			return pb2.User_Create_Account_Response(
					status=pb2.UCA_ERROR,
					errMsg=feh.get_server_error_message())

def _serve(port: Text):
	bind_address = f"[::]:{port}"
	server = grpc.server(futures.ThreadPoolExecutor())
	pb2_grpc.add_LoginServicer_to_server(Login(), server)
	server.add_insecure_port(bind_address)
	server.start()
	server.wait_for_termination()

if __name__ == "__main__":
	logging.basicConfig(level=logging.INFO)
	_serve(_PORT)