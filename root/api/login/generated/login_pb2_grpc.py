# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
"""Client and server classes corresponding to protobuf-defined services."""
import grpc

import login_pb2 as login__pb2


class LoginStub(object):
    """SERVICES ----------------------------------------------------------------- 

    """

    def __init__(self, channel):
        """Constructor.

        Args:
            channel: A grpc.Channel.
        """
        self.user_login = channel.unary_unary(
                '/endpoints.silstn.login.Login/user_login',
                request_serializer=login__pb2.User_Login_Request.SerializeToString,
                response_deserializer=login__pb2.User_Login_Response.FromString,
                )
        self.user_create_account = channel.unary_unary(
                '/endpoints.silstn.login.Login/user_create_account',
                request_serializer=login__pb2.User_Create_Account_Request.SerializeToString,
                response_deserializer=login__pb2.User_Create_Account_Response.FromString,
                )


class LoginServicer(object):
    """SERVICES ----------------------------------------------------------------- 

    """

    def user_login(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def user_create_account(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')


def add_LoginServicer_to_server(servicer, server):
    rpc_method_handlers = {
            'user_login': grpc.unary_unary_rpc_method_handler(
                    servicer.user_login,
                    request_deserializer=login__pb2.User_Login_Request.FromString,
                    response_serializer=login__pb2.User_Login_Response.SerializeToString,
            ),
            'user_create_account': grpc.unary_unary_rpc_method_handler(
                    servicer.user_create_account,
                    request_deserializer=login__pb2.User_Create_Account_Request.FromString,
                    response_serializer=login__pb2.User_Create_Account_Response.SerializeToString,
            ),
    }
    generic_handler = grpc.method_handlers_generic_handler(
            'endpoints.silstn.login.Login', rpc_method_handlers)
    server.add_generic_rpc_handlers((generic_handler,))


 # This class is part of an EXPERIMENTAL API.
class Login(object):
    """SERVICES ----------------------------------------------------------------- 

    """

    @staticmethod
    def user_login(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/endpoints.silstn.login.Login/user_login',
            login__pb2.User_Login_Request.SerializeToString,
            login__pb2.User_Login_Response.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def user_create_account(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/endpoints.silstn.login.Login/user_create_account',
            login__pb2.User_Create_Account_Request.SerializeToString,
            login__pb2.User_Create_Account_Response.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)
