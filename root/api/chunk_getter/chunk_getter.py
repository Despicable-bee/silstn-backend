"""
	FILENAME:		chunk_getter.py

	AUTHOR(s):		

	DESCRIPTION:		

	VERSION:		0.0.1

	DESTINATION:		

	LICENSE:		Property of Starfighter Australia Pty Ltd
"""
# Standard Library
from concurrent import futures
import logging
import os
from typing import Text
import json
import time

import sys

# Third Party Library
import grpc

# Local Library
from root.api.chunk_getter.generated import chunk_getter_pb2_grpc as pb2_grpc
from root.api.chunk_getter.generated import chunk_getter_pb2 as pb2
from root.base_classes.chunk_doc import ChunkBoundingBox, ChunkType

from root.helpers.google_maps_traffic_helper.google_maps_traffic_helper import \
		ViewportBoundsConfig
from root.helpers.google_maps_traffic_helper.google_maps_traffic_helper import \
		GoogleMapsTrafficHelper

from root.helpers.chunk_bounds_checker.chunk_bounds_checker import \
		ChunkBoundsChecker
from root.helpers.chunk_bounds_checker.chunk_bounds_checker import \
		BoundingBoxHelper

from root.helpers.common_helper.common_firestore_helper import \
		FirestoreMetadataHelper
from root.helpers.common_helper.common_firestore_helper import \
		FirestoreErrorHelper

from root.helpers.jwt_helper.jwt_helper import JwkStorageHelper
from root.helpers.jwt_helper.jwt_helper import JwtClaims


_PORT = os.environ["PORT"]

# * DEBUG ----------------------------------------------------------------------

__filename__ = 'chunk_getter'

class ChunkGetter(pb2_grpc.ChunkGetterServicer):
	def grab_chunks(self,
				request: pb2.ChunkGetter_Request,
				context: grpc.ServicerContext) -> pb2.ChunkGetter_Response:
		"""  """
		fmh = FirestoreMetadataHelper()
		feh = FirestoreErrorHelper()
		jwksh = JwkStorageHelper()

		try:
			metadataResult = fmh.get_context_metadata(
					context.invocation_metadata())
		
			# Check whether the jwt is valid
			if metadataResult == None or metadataResult.get_jwt() == '':
				return pb2.ChunkGetter_Response(
						status=pb2.ERROR,
						chunksJSON="",
						errMsg=feh.get_server_error_message(),
						forecastAvailable=2)
			
			# JWT is non-empty, check for validity
			claims = jwksh.check_old_jwt(metadataResult.get_jwt())

			if not isinstance(claims, JwtClaims):
				return pb2.ChunkGetter_Response(
						status=pb2.FAILURE,
						chunksJSON="",
						errMsg=feh.get_claims_error_message(),
						forecastAvailable=2)

			# TODO Check claims are legit

			userId = claims.get_subject()

			# TODO Check the user is not spamming the 'grab_chunks' api
			# for i in range(0, 10):
			# 	yield pb2.ChunkGetter_Response(
			# 			status=pb2.STREAM_OK,
			# 			chunksJSON="",
			# 			errMsg="")
			# 	time.sleep(0.25)

			cbc = ChunkBoundsChecker()
			bbh = BoundingBoxHelper()

			# Initialise the viewport 
			viewportBounds = ChunkBoundingBox(
					topRightLat=request.maxLat,				# type: ignore
					topRightLng=request.maxLng,				# type: ignore
					bottomLeftLat=request.minLat,			# type: ignore
					bottomLeftLng=request.minLng)			# type: ignore

			# perform a split check
			viewportConfig = bbh.check_viewport_bounds_config(
					viewportBounds=viewportBounds)

			# Get the chunks that are in view
			for i in cbc.get_chunks_in_view_stream(
					viewMode=ChunkType(request.chunkType),	# type: ignore
					viewportBounds=viewportBounds,
					viewportBoundsConfig=viewportConfig):
				yield i

			return pb2.ChunkGetter_Response(
					status=pb2.SUCCESSFUL,
					chunksJSON="",
					errMsg="",
					forecastAvailable=2)

		except Exception as e:
			exc_type, exc_obj, exc_tb = sys.exc_info()
			feh = FirestoreErrorHelper()
			feh.api_error_handler(fileName=__filename__,
					methodName='create_squadron', 
					exceptionMsg=e,
					lineNum=exc_tb.tb_lineno)   # type: ignore
			return pb2.ChunkGetter_Response(
					status=pb2.ERROR,
					chunksJSON="",
					errMsg=feh.get_server_error_message(),
					forecastAvailable=False)
	

def _serve(port: Text):
	bind_address = f"[::]:{port}"
	server = grpc.server(futures.ThreadPoolExecutor())
	pb2_grpc.add_ChunkGetterServicer_to_server(ChunkGetter(), server)
	server.add_insecure_port(bind_address)
	server.start()
	server.wait_for_termination()

if __name__ == "__main__":
	logging.basicConfig(level=logging.INFO)
	_serve(_PORT)