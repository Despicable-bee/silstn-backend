# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
"""Client and server classes corresponding to protobuf-defined services."""
import grpc

import chunk_getter_pb2 as chunk__getter__pb2


class ChunkGetterStub(object):
    """Missing associated documentation comment in .proto file."""

    def __init__(self, channel):
        """Constructor.

        Args:
            channel: A grpc.Channel.
        """
        self.grab_chunks = channel.unary_stream(
                '/endpoints.silstn.chunk_getter.ChunkGetter/grab_chunks',
                request_serializer=chunk__getter__pb2.ChunkGetter_Request.SerializeToString,
                response_deserializer=chunk__getter__pb2.ChunkGetter_Response.FromString,
                )


class ChunkGetterServicer(object):
    """Missing associated documentation comment in .proto file."""

    def grab_chunks(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')


def add_ChunkGetterServicer_to_server(servicer, server):
    rpc_method_handlers = {
            'grab_chunks': grpc.unary_stream_rpc_method_handler(
                    servicer.grab_chunks,
                    request_deserializer=chunk__getter__pb2.ChunkGetter_Request.FromString,
                    response_serializer=chunk__getter__pb2.ChunkGetter_Response.SerializeToString,
            ),
    }
    generic_handler = grpc.method_handlers_generic_handler(
            'endpoints.silstn.chunk_getter.ChunkGetter', rpc_method_handlers)
    server.add_generic_rpc_handlers((generic_handler,))


 # This class is part of an EXPERIMENTAL API.
class ChunkGetter(object):
    """Missing associated documentation comment in .proto file."""

    @staticmethod
    def grab_chunks(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_stream(request, target, '/endpoints.silstn.chunk_getter.ChunkGetter/grab_chunks',
            chunk__getter__pb2.ChunkGetter_Request.SerializeToString,
            chunk__getter__pb2.ChunkGetter_Response.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)
