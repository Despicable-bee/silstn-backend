"""
	FILENAME:		intersection_data_getter.py

	AUTHOR(s):		

	DESCRIPTION:		

	VERSION:		0.0.1

	DESTINATION:		

	LICENSE:		Property of 
"""
# Standard Library
from concurrent import futures
import logging
import os
from typing import Text
from typing import List
from typing import Tuple

import sys
import json

# Third Party Library
import grpc

import numpy as np

# Local Library
from root.api.intersection_data_getter.generated import \
		intersection_data_getter_pb2_grpc as pb2_grpc
from root.api.intersection_data_getter.generated import \
		intersection_data_getter_pb2 as pb2

from root.helpers.data_collector.data_collector_firestore_helper import \
		IntersectionTimeRange
from root.helpers.data_collector.data_collector_firestore_helper import \
		DataCollectorHelper

from root.helpers.common_helper.common_firestore_helper import \
		FirestoreMetadataHelper
from root.helpers.common_helper.common_firestore_helper import \
		FirestoreErrorHelper

from root.helpers.jwt_helper.jwt_helper import JwkStorageHelper
from root.helpers.jwt_helper.jwt_helper import JwtClaims

from root.helpers.prediction_api.prediction_api_helper import \
		PredictionApiHelper

from root.helpers.chunk_bounds_checker.intersection_doc_getters import \
        IntersectionDocGetter

# Base docs
from root.base_classes.intersections_doc import IntersectionsDocManifest

_PORT = os.environ["PORT"]

# * DEBUG ----------------------------------------------------------------------

__filename__ = 'intersection_data_getter.py'

class IntersectionDataGetter(pb2_grpc.IntersectionDataGetterServicer):
	
	def get_intersection_data(self,
			request: pb2.IntersectionDataGetter_Request,
			context: grpc.ServicerContext) -> pb2.IntersectionDataGetter_Response:
		""" Gets the intersection data for a given tsc. """
		fmh = FirestoreMetadataHelper()
		feh = FirestoreErrorHelper()
		jwksh = JwkStorageHelper()
		idg = IntersectionDocGetter()

		try:
			metadataResult = fmh.get_context_metadata(
					context.invocation_metadata())
		
			# Check whether the jwt is valid
			if metadataResult == None or metadataResult.get_jwt() == '':
				yield pb2.IntersectionDataGetter_Response(
						status=pb2.ERROR,
						chunksJSON="",
						errMsg=feh.get_server_error_message())
				return 

			# JWT is non-empty, check for validity
			claims = jwksh.check_old_jwt(metadataResult.get_jwt())

			if not isinstance(claims, JwtClaims):
				yield pb2.IntersectionDataGetter_Response(
						status=pb2.FAILURE,
						dataJSON="",
						errMsg=feh.get_claims_error_message())
				return

			# TODO Check claims are legit

			userId = claims.get_subject()

			# TODO Check the user is not spamming the 'get_intersection_data' api

			# Check request variables are legitimate
			tsc = request.intersectionNum # type: ignore
			timeRangeNum = request.timeRangeOption # type: ignore

			if not isinstance(tsc, int) or \
					tsc < 0 or \
					not isinstance(timeRangeNum, int) or \
					timeRangeNum > IntersectionTimeRange.ONE_YEAR.value or \
					timeRangeNum < IntersectionTimeRange.ONE_HOUR.value:
				yield pb2.IntersectionDataGetter_Response(
						status=pb2.FAILURE,
						dataJSON="",
						errMsg=feh.get_invalid_inputs_error())
				return

			# Get the intersections manifest doc
			intersecManifestDoc = idg.get_intersections_manifest_doc()

			if not isinstance(intersecManifestDoc, IntersectionsDocManifest):
				raise Exception(
						"Error trying to get intersections manifest doc")

			# if tsc not in intersecManifestDoc.get_prediction_tscs_as_list():
			# 	raise Exception("Specified tsc not available for forecast")

			# Stream the response stuff
			dch = DataCollectorHelper()

			for result in dch.get_intersection_data(tsc=tsc, 
					timeRange=IntersectionTimeRange(timeRangeNum)):
				# Yield a new batch of results each time
				yieldList: List[dict] = []
				
				# Reverse the results
				result[0].reverse()
				
				# convert each of the result objects to a dictionary form
				for item in result[0]:
					yieldList.append(item.to_dict())
				
				yield pb2.IntersectionDataGetter_Response(
					status=pb2.SUCCESSFUL,
					dataJSON=json.dumps(yieldList),
					totalFrames=result[1],
					errMsg="")
			
		except Exception as e:
			exc_type, exc_obj, exc_tb = sys.exc_info()
			feh = FirestoreErrorHelper()
			feh.api_error_handler(fileName=__filename__,
					methodName='get_intersection_data', 
					exceptionMsg=e,
					lineNum=exc_tb.tb_lineno)   # type: ignore
			yield pb2.IntersectionDataGetter_Response(
					status=pb2.ERROR,
					dataJSON="",
					errMsg=feh.get_server_error_message())

		# Completed - end the stream
		return

	def check_available_ranges(self,
			request: pb2.CheckAvailableRange_Request,
			context: grpc.ServicerContext) -> pb2.CheckAvailableRange_Response:
		""" Checks (broadly) what time ranges are available. """
		fmh = FirestoreMetadataHelper()
		feh = FirestoreErrorHelper()
		jwksh = JwkStorageHelper()
		try:
			metadataResult = fmh.get_context_metadata(
					context.invocation_metadata())
		
			# Check whether the jwt is valid
			if metadataResult == None or metadataResult.get_jwt() == '':
				return pb2.CheckAvailableRange_Response(
						status=pb2.ERROR,
						maxAvailableTimeRange=-1,
						errMsg=feh.get_server_error_message())

			# JWT is non-empty, check for validity
			claims = jwksh.check_old_jwt(metadataResult.get_jwt())

			if not isinstance(claims, JwtClaims):
				return pb2.CheckAvailableRange_Response(
						status=pb2.FAILURE,
						maxAvailableTimeRange=-1,
						errMsg=feh.get_claims_error_message())

			# TODO Check claims are legit

			userId = claims.get_subject()

			# TODO Check the user is not spamming the 'check_available_ranges' api

			# Get the available time ranges
			dch = DataCollectorHelper()
			
			result = dch.get_available_ranges()

			if not isinstance(result, IntersectionTimeRange):
				raise Exception("Couldn't get available time ranges")
			
			# Return the result
			return pb2.CheckAvailableRange_Response(
					status=pb2.SUCCESSFUL,
					maxAvailableTimeRange=result.value,
					errMsg="")

		except Exception as e:
			exc_type, exc_obj, exc_tb = sys.exc_info()
			feh = FirestoreErrorHelper()
			feh.api_error_handler(fileName=__filename__,
					methodName='get_intersection_data', 
					exceptionMsg=e,
					lineNum=exc_tb.tb_lineno)   # type: ignore
			return pb2.CheckAvailableRange_Response(
					status=pb2.ERROR,
					maxAvailableTimeRange=-1,
					errMsg=feh.get_server_error_message())
	

	def get_forecast(self, 
			request: pb2.IntersectionForecast_Request,
			context: grpc.ServicerContext) -> pb2.IntersectionForecast_Response:
		""" Returns the forecast for a given intersection number. """
		fmh = FirestoreMetadataHelper()
		feh = FirestoreErrorHelper()
		jwksh = JwkStorageHelper()
		try:
			metadataResult = fmh.get_context_metadata(
					context.invocation_metadata())
		
			# Check whether the jwt is valid
			if metadataResult == None or metadataResult.get_jwt() == '':
				yield pb2.IntersectionForecast_Response(
						status=pb2.ERROR,
						errMsg=feh.get_server_error_message())
				return

			# JWT is non-empty, check for validity
			claims = jwksh.check_old_jwt(metadataResult.get_jwt())

			if not isinstance(claims, JwtClaims):
				yield pb2.IntersectionForecast_Response(
						status=pb2.FAILURE,
						errMsg=feh.get_claims_error_message())
				return

			# TODO Check claims are legit

			userId = claims.get_subject()

			# TODO Check the user is not spamming the 'get_forecast' api

			pah = PredictionApiHelper()

			results = pah.request_forecast_2(
					tsc=request.intersectionNum) # type: ignore

			if not isinstance(results, Tuple):
				yield pb2.IntersectionForecast_Response(
						status=pb2.FAILURE,
						errMsg=feh.get_invalid_inputs_error())
				return

			if not isinstance(results[0], np.ndarray):
				# Oh no something went wrong :c
				yield pb2.IntersectionForecast_Response(
						status=pb2.FAILURE,
						errMsg=feh.get_invalid_inputs_error())
				return

			logging.info(results)
			

			# Yay the thing worked :D
			yield pb2.IntersectionForecast_Response(
					status=pb2.SUCCESSFUL,
					dataJSON=json.dumps([results[0].tolist(), results[1]]),
					errMsg="")
			return

		except Exception as e:
			exc_type, exc_obj, exc_tb = sys.exc_info()
			feh = FirestoreErrorHelper()
			feh.api_error_handler(fileName=__filename__,
					methodName='get_forecast', 
					exceptionMsg=e,
					lineNum=exc_tb.tb_lineno)   # type: ignore
			yield pb2.IntersectionForecast_Response(
					status=pb2.ERROR,
					errMsg=feh.get_server_error_message())

def _serve(port: Text):
	bind_address = f"[::]:{port}"
	server = grpc.server(futures.ThreadPoolExecutor())
	pb2_grpc.add_IntersectionDataGetterServicer_to_server(IntersectionDataGetter(), server)
	server.add_insecure_port(bind_address)
	server.start()
	server.wait_for_termination()

if __name__ == "__main__":
	logging.basicConfig(level=logging.INFO)
	_serve(_PORT)